@extends('base')
@section('title')The Pastor @parent @stop
@section('meta')
@parent
<meta name="description" content="The Redeemed Christian Church of God Chapel of Life - A place of worship where Jesus is enthroned, the undiluted Word of God is preached, the presence of God dwells. Join us for any of our services and experience the love and power of God in all areas of your life. Jesus loves you." />
<meta name="keywords" content="chapel of life, rccg, church, redeemed, christ, love, faith, peace, christian faith, religion, festac, lagos, amuwo odofin, jesus, mile two, nigeria" />
@stop
@section('styles')
@parent

<link rel="stylesheet" type="text/css" href="css/mediaelementplayer.min.css" />
<link rel="stylesheet" type="text/css" href="css/colors/blue.css" title="color1" />
@stop

@section('content')
<div class="page-top">
	<div class="parallax" style="background:url(images/parallax1.jpg);"></div>	
	<div class="container"> 
		<h1>The <span>Pastor</span></h1>
		<ul>
			<li><a href="./" title="">Home</a></li>
			<li><a href="#" title="">The Church</a></li>
			<li><a href="#" title="">Our Pastoral Team</a></li>
			<li><a href="#" title="">The Pastor</a></li>
		</ul>
	</div>
</div><!--- PAGE TOP -->

<section>
	<div class="block">
		<div class="container">
			<div class="row">
				<div class="col-md-8 column">
					<div class="team-single">
						<div class="member-img">
							<img src="images/resource/team-single.jpg" alt="" />
						</div>

						<div class="team-detail">
							<h3>JANE BIRKIN</h3>
							<ul class="team-list">
								<li><i class="fa fa-user"></i> PROJECT ADMINISTRATOR</li>
								<li><i class="fa fa-home"></i> WeStand Eaton Square 489, London</li>
								<li><i class="fa fa-phone"></i> (800) 123.456.7890 </li>
								<li><i class="fa fa-google"></i> http://www.janebirkin.com/</li>
								<li><i class="fa fa-envelope"></i> jane@partyname.com</li>
							</ul>
						</div>
						<ul class="social-media">
							<li><a title="" href="#"><i class="fa fa-linkedin"></i></a></li>
							<li><a title="" href="#"><i class="fa fa-google-plus"></i></a></li>
							<li><a title="" href="#"><i class="fa fa-twitter"></i></a></li>
							<li><a title="" href="#"><i class="fa fa-facebook"></i></a></li>
						</ul>
						
					</div><!-- TEAM SINGLE -->
					
					<p>Aenean leo vene quam. Pellntes ique ornare sem eius modte venenatis vestibum. Cras mattis itugir purus. Aenean le vene quam. Pellntes ique ornare seeim eiusmodte venenatis vestibum. Cras mattis citur exquisitely fari then far purus. Aenean leo vene quam. Pellntes ique ornare sem eiusmodte venen. Et tollit utamur nam, dcum ullumo etiam velit. Ne scripserit. Sea ex utamur phaedrum, nisl no, no reque sensibus duo. Meini coposae, paulo mediocrem etiam negleg enur. Vis ut argum entum lorem ipsum dolor sit amet, consectetur adipscing elit. Nulla convallis egestas rhoncus. Don eofacilisis fermentum sem, ac viverra ante lucus vel. Donec vel maurs quam. Lorem ipsum dolor sit amet, consect etur adpiscing elit. Nulla convallis egestas rhoncus. Donec facilisis ferme ntum sem, ac viverra ante luctus vel. Donec vel maus quam.Lorem ipsum dolor sit amet, consectetur dipiscing elit. Nulla convallis egestas rhoncus. </p>

					<blockquote>
					<div class="parallax" style="background:url(images/parallax2.jpg);"></div>
					<i class="fa fa-quote-left"></i>Voluptate illum dolore ita ipsum, quid deserunt singulis, labore admodum ita multos malis ea nam nam tamen fore amet.<i class="fa fa-quote-right"></i></blockquote>

					<p>Aenean leo vene quam. Pellntes ique ornare sem eius modte venenatis vestibum. Cras mattis itugir purus. Aenean le vene quam. Pellntes ique ornare seeim eiusmodte venenatis vestibum. Cras mattis citur exquisitely fari then far purus. Aenean leo vene quam. Pellntes ique ornare sem eiusmodte venen. Et tollit utamur nam, dcum ullumo etiam velit. Ne scripserit. Sea ex utamur phaedrum, nisl no, no reque sensibus duo. Meini coposae, paulo mediocrem etiam negleg enur. Vis ut argum entum lorem ipsum dolor sit amet, consectetur adipscing elit. Nulla convallis egestas rhoncus. Don eofacilisis fermentum sem, ac viverra ante lucus vel. Donec vel maurs quam. Lorem ipsum dolor sit amet, consect etur adpiscing elit. Nulla convallis egestas rhoncus. Donec facilisis ferme ntum sem, ac viverra ante luctus vel. Donec vel maus quam.Lorem ipsum dolor sit amet, consectetur dipiscing elit. Nulla convallis egestas rhoncus. </p>
					<div class="space"></div>
					<div class="space"></div>
					<div class="simple-text">
						<h3>QUALIFICATION:</h3>
						<p>Aenean leo vene quam. Pellntes ique ornare sem eius modte venenatis vestibum. Cras mattis citugir purus. Aenean levene quam. Pellntes ique ornare seeim eiusmodte venenatis vestibum. ras mattis citur exquisitely fari then far purus. Aenean leo vene quam. Pellntes ique ornare sem eiusmodte venen..</p>
					</div>
					<ul>
						<li><i class="fa fa-check-square"></i>Aenean leo vene quam ellntes ique ornare sem eius modte venenatis vestibum. </li>
						<li><i class="fa fa-check-square"></i>Cras mattis citugir purus. Aenean levene quam. </li>
						<li><i class="fa fa-check-square"></i>Pellntes ique ornare seeim eiusmodte venenatis vestibumras mattis citur.</li>
						<li><i class="fa fa-check-square"></i>Exquisitely fari then far purus. </li>
						<li><i class="fa fa-check-square"></i> Aenean leo vene quam. Pellntes ique ornare sem eiusmodte venen.</li>
					</ul>
				</div>

				<aside class="col-md-4 sidebar column">
					<div class="widget">
						<div class="widget-title"><h4>OUR GALLERY</h4></div>
						<div class="gallery-widget">
							<div class="col-md-3"><a href="images/resource/flickr1.jpg" data-rel="prettyPhoto"><img src="images/resource/flickr1.jpg" alt="" /></a></div>
							<div class="col-md-3"><a href="images/resource/flickr2.jpg" data-rel="prettyPhoto"><img src="images/resource/flickr2.jpg" alt="" /></a></div>
							<div class="col-md-3"><a href="images/resource/flickr3.jpg" data-rel="prettyPhoto"><img src="images/resource/flickr3.jpg" alt="" /></a></div>
							<div class="col-md-3"><a href="images/resource/flickr4.jpg" data-rel="prettyPhoto"><img src="images/resource/flickr4.jpg" alt="" /></a></div>
							<div class="col-md-3"><a href="images/resource/flickr5.jpg" data-rel="prettyPhoto"><img src="images/resource/flickr5.jpg" alt="" /></a></div>
							<div class="col-md-3"><a href="images/resource/flickr6.jpg" data-rel="prettyPhoto"><img src="images/resource/flickr6.jpg" alt="" /></a></div>
							<div class="col-md-3"><a href="images/resource/flickr1.jpg" data-rel="prettyPhoto"><img src="images/resource/flickr1.jpg" alt="" /></a></div>
							<div class="col-md-3"><a href="images/resource/flickr2.jpg" data-rel="prettyPhoto"><img src="images/resource/flickr2.jpg" alt="" /></a></div>
						</div>
					</div><!-- GALLERY -->


					<div class="widget">
						<div class="widget-title"><h4>CATEGORIES</h4></div>
						<ul>
							<li><a href="blog.html" title=""><i class="fa fa-hand-o-right"></i>Events</a></li>
							<li><a href="blog.html" title=""><i class="fa fa-hand-o-right"></i>News</a></li>
							<li><a href="blog.html" title=""><i class="fa fa-hand-o-right"></i>Blog</a></li>
						</ul>
					</div><!-- CATEGORIES -->

					<div class="widget">
						<div class="widget-title"><h4>POPULAR POSTS</h4></div>
						<div class="remove-ext">
							<div class="widget-blog">
								<div class="widget-blog-img"><img src="images/resource/widget-blog.jpg" alt="" /></div>
								<p><a href="blog-single.html" title="">Suspendisse velit anteg, aliquet vel adiping.</a></p>
								<span><i class="fa fa-calendar-o"></i> November 01, 2013</span>
							</div><!-- WIDGET BLOG -->

							<div class="widget-blog">
								<div class="widget-blog-img"><img src="images/resource/widget-blog2.jpg" alt="" /></div>
								<p><a href="blog-single.html" title="">Suspendisse velit anteg, aliquet vel adiping.</a></p>
								<span><i class="fa fa-calendar-o"></i> November 01, 2013</span>
							</div><!-- WIDGET BLOG -->

							<div class="widget-blog">
								<div class="widget-blog-img"><img src="images/resource/widget-blog3.jpg" alt="" /></div>
								<p><a href="blog-single.html" title="">Suspendisse velit anteg, aliquet vel adiping.</a></p>
								<span><i class="fa fa-calendar-o"></i> November 01, 2013</span>
							</div><!-- WIDGET BLOG -->
						</div>						
					</div><!-- POPULAR POSTS -->

					<div class="widget">
						<div class="widget-title"><h4>COMMENTS</h4></div>
						<div class="remove-ext">
							<div class="comment-widget">
								<h5><a href="blog-single.html" title="">Design can feel like something you have always been waiting for without knowing.</a></h5>
								<span><i class="fa fa-calendar-o"></i> NOVEMBER 01, 2013</span>
								<span><i class="fa fa-user"></i> NORMAN RONOGH</span>
							</div>
							<div class="comment-widget">
								<h5><a href="blog-single.html" title="">Design can feel like something you have always been waiting for without knowing.</a></h5>
								<span><i class="fa fa-calendar-o"></i> NOVEMBER 01, 2013</span>
								<span><i class="fa fa-user"></i> NORMAN RONOGH</span>
							</div>
						</div>
					</div><!-- COMMENTS -->
				</aside><!-- SIDEBAR -->
				
			</div>
		</div>
	</div>
</section>
@stop
@section('after_footer')
	<!-- SCRIPTS-->
	<script type="text/javascript" src="js/modernizr.custom.17475.js"></script>

	<script src="js/jquery.1.10.2.js" type="text/javascript"></script>
    <script src="js/bootstrap.min.js"></script>
	<script src="js/jquery.prettyPhoto.js" type="text/javascript"></script>
    <script src="js/script.js"></script>
    <script src="js/styleswitcher.js"></script>
	<script type="text/javascript" src="js/jquery.downCount.js"></script>
@stop