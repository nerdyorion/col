@extends('base')
@section('title')Events @parent @stop
@section('meta')
@parent
<meta name="description" content="The Redeemed Christian Church of God Chapel of Life - A place of worship where Jesus is enthroned, the undiluted Word of God is preached, the presence of God dwells. Join us for any of our services and experience the love and power of God in all areas of your life. Jesus loves you." />
<meta name="keywords" content="chapel of life, rccg, church, redeemed, christ, love, faith, peace, christian faith, religion, festac, lagos, amuwo odofin, jesus, mile two, nigeria" />
@stop
@section('styles')
@parent

<link rel="stylesheet" type="text/css" href="css/colors/blue.css" title="color1" />
@stop

@section('content')
<div class="page-top">
	<div class="parallax" style="background:url(images/parallax1.jpg);"></div>	
	<div class="container"> 
		<h1>EVENTS <span>LIST</span></h1>
		<ul>
			<li><a href="./" title="">Home</a></li>
			<li><a href="#">Events</a></li>
		</ul>
	</div>
</div><!--- PAGE TOP -->

<section>
	<div class="block">
		<div class="container">
			<div class="row">
				<div class="col-md-8 column">
					<div class="events-gridview remove-ext">  
						<div class="row">
							<div class="col-md-6">
								<div class="category-box">
									<div class="category-block">
										<div class="category-img">
									 		<img src="images/resource/event1.jpg" alt="" />
											<ul>
												<li class="date"><a href="event-single" title=""><i class="fa fa-calendar-o"></i> 28, April 2008</a></li>
												<li class="time"><a href="event-single" title=""><i class="fa fa-clock-o"></i> 8:00 PM</a></li>
											</ul>
										</div>
										<h3><a href="event-single" title="">Bethel School of Worship</a></h3>
										<span><i class="fa fa-map-marker"></i> 654 Kingsland Rd, Big House</span>
									</div>						
								</div><!-- EVENTS -->
							</div>

							<div class="col-md-6">
								<div class="category-box">
									<div class="category-block">
										<div class="category-img">
									 		<img src="images/resource/event2.jpg" alt="" />
											<ul>
												<li class="date"><a href="event-single" title=""><i class="fa fa-calendar-o"></i> 28, April 2008</a></li>
												<li class="time"><a href="event-single" title=""><i class="fa fa-clock-o"></i> 8:00 PM</a></li>
											</ul>
										</div>
										<h3><a href="event-single" title="">Firelife: School of Revival</a></h3>
										<span><i class="fa fa-map-marker"></i> 654 Kingsland Rd, Big House</span>
									</div>						
								</div><!-- EVENTS -->
							</div>

							<div class="col-md-6">
								<div class="category-box">
									<div class="category-block">
										<div class="category-img">
									 		<img src="images/resource/event3.jpg" alt="" />
											<ul>
												<li class="date"><a href="event-single" title=""><i class="fa fa-calendar-o"></i> 28, April 2008</a></li>
												<li class="time"><a href="event-single" title=""><i class="fa fa-clock-o"></i> 8:00 PM</a></li>
											</ul>
										</div>
										<h3><a href="event-single" title="">chool of the Prophets</a></h3>
										<span><i class="fa fa-map-marker"></i> 654 Kingsland Rd, Big House</span>
									</div>						
								</div><!-- EVENTS -->
							</div>

							<div class="col-md-6">
								<div class="category-box">
									<div class="category-block">
										<div class="category-img">
									 		<img src="images/resource/event4.jpg" alt="" />
											<ul>
												<li class="date"><a href="event-single" title=""><i class="fa fa-calendar-o"></i> 28, April 2008</a></li>
												<li class="time"><a href="event-single" title=""><i class="fa fa-clock-o"></i> 8:00 PM</a></li>
											</ul>
										</div>
										<h3><a href="event-single" title="">INSPIRE</a></h3>
										<span><i class="fa fa-map-marker"></i> 654 Kingsland Rd, Big House</span>
									</div>						
								</div><!-- EVENTS -->
							</div>
							<div class="col-md-6">
								<div class="category-box">
									<div class="category-block">
										<div class="category-img">
									 		<img src="images/resource/event1.jpg" alt="" />
											<ul>
												<li class="date"><a href="event-single" title=""><i class="fa fa-calendar-o"></i> 28, April 2008</a></li>
												<li class="time"><a href="event-single" title=""><i class="fa fa-clock-o"></i> 8:00 PM</a></li>
											</ul>
										</div>
										<h3><a href="event-single" title="">A Holisitic Approac Nurtu</a></h3>
										<span><i class="fa fa-map-marker"></i> 654 Kingsland Rd, Big House</span>
									</div>						
								</div><!-- EVENTS -->
							</div>

							<div class="col-md-6">
								<div class="category-box">
									<div class="category-block">
										<div class="category-img">
									 		<img src="images/resource/event2.jpg" alt="" />
											<ul>
												<li class="date"><a href="event-single" title=""><i class="fa fa-calendar-o"></i> 28, April 2008</a></li>
												<li class="time"><a href="event-single" title=""><i class="fa fa-clock-o"></i> 8:00 PM</a></li>
											</ul>
										</div>
										<h3><a href="event-single" title="">A Holisitic Approac Nurtu</a></h3>
										<span><i class="fa fa-map-marker"></i> 654 Kingsland Rd, Big House</span>
									</div>						
								</div><!-- EVENTS -->
							</div>
						</div>
					</div><!-- EVENTS GRID VIEW -->

					<div class="theme-pagination">
						<ul class="pagination">
							<li><a href="#"><i class="fa fa-angle-left"></i></a></li>
							<li><a href="#">1</a></li>
							<li><a href="#">2</a></li>
							<li><a href="#">3</a></li>
							<li><a href="#">4</a></li>
							<li><a href="#">5</a></li>
							<li><a href="#"><i class="fa fa-angle-right"></i></a></li>
						</ul>
					</div><!-- PAGINATION -->
					
				</div>

				<aside class="col-md-4 sidebar column">
					<div class="widget">
						<form class="search-form">
							<input type="text" placeholder="START SEARCHING" />
							<input type="submit" value="" />
						</form>
					</div><!-- SEARCH FORM -->

					<div class="widget">
						<div class="widget-title"><h4>TAG CLOUD</h4></div>
						<div class="tagclouds">
							<a href="blog.html" title="">Aenen</a>
							<a href="blog.html" title="">Suspendise</a>
							<a href="blog.html" title="">Citrous</a>
							<a href="blog.html" title="">Valitsantego</a>
							<a href="blog.html" title="">Pellntesious</a>
							<a href="blog.html" title="">Vestibu</a>
							<a href="blog.html" title="">Aenen</a>
							<a href="blog.html" title="">Suspendise</a>
							<a href="blog.html" title="">Citrous</a>
						</div>
					</div><!-- TAG CLOUD -->

					<div class="widget">
						<div class="widget-title"><h4>RECENT BLOG</h4></div>
						<div class="remove-ext">
							<div class="widget-blog">
								<div class="widget-blog-img"><img src="images/resource/widget-blog.jpg" alt="" /></div>
								<p><a href="blog-single.html" title="">Suspendisse velit anteg, aliquet vel adiping.</a></p>
								<span><i class="fa fa-calendar-o"></i> November 01, 2013</span>
							</div><!-- WIDGET BLOG -->

							<div class="widget-blog">
								<div class="widget-blog-img"><img src="images/resource/widget-blog2.jpg" alt="" /></div>
								<p><a href="blog-single.html" title="">Suspendisse velit anteg, aliquet vel adiping.</a></p>
								<span><i class="fa fa-calendar-o"></i> November 01, 2013</span>
							</div><!-- WIDGET BLOG -->
						</div>						
					</div><!-- RECENT BLOG -->

					<div class="widget">
						<div class="widget-title"><h4>VIDEO</h4></div>
						<div class="video">
							<div class="video-img">
								<img src="images/resource/video.jpg" alt="" />
								<a href="http://vimeo.com/44867610"  data-rel="prettyPhoto" title=""><i class="fa fa-play"></i></a>
							</div>
						</div>
					</div><!-- VIDEO -->


					<!--<div class="widget">
						<div class="widget-title"><h4>META</h4></div>
						<ul>
							<li><a href="blog.html" title=""><i class="fa fa-hand-o-right"></i>Log in</a></li>
							<li><a href="blog.html" title=""><i class="fa fa-hand-o-right"></i>Entries RSS</a></li>
							<li><a href="blog.html" title=""><i class="fa fa-hand-o-right"></i>Comments RSS</a></li>
							<li><a href="blog.html" title=""><i class="fa fa-hand-o-right"></i>Wordpress.org</a></li>
						</ul>
					</div> META -->
				</aside><!-- SIDEBAR -->
				
			</div>
		</div>
	</div>
</section>
@stop
@section('after_footer')

	<!-- SCRIPTS-->
	<script type="text/javascript" src="js/modernizr.custom.17475.js"></script>

	<script src="js/jquery.1.10.2.js" type="text/javascript"></script>
    <script src="js/bootstrap.min.js"></script>
	<script src="js/jquery.prettyPhoto.js" type="text/javascript"></script>
    <script src="js/script.js"></script>
	<script type="text/javascript" src="js/jquery.downCount.js"></script> 
@stop