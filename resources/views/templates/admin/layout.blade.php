<!DOCTYPE html>
<html lang="en">

<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<base href="{{ URL::to('/') }}/manage/">
<meta name="description" content="">
<meta name="author" content="">
<!-- <link rel="icon" type="image/png" sizes="16x16" href="assets/images/favicon.ico"> -->
<link href="{{ asset('images/favicon.png') }}" rel="shortcut icon" type="image/png" />
<title>{{ $page_title }} | {{ config('app.name') }} Administration</title>
<!-- Bootstrap Core CSS -->
<link href="{{ asset('admin/assets/bower_components/bootstrap/dist/css/bootstrap.min.css') }} " rel="stylesheet">
<!-- Menu CSS -->
<link href="{{ asset('admin/assets/bower_components/metisMenu/dist/metisMenu.min.css') }}" rel="stylesheet">
<!-- Menu CSS -->
<!--<link href="assets/bower_components/morrisjs/morris.css" rel="stylesheet">-->
<!-- Custom CSS -->
<link href="{{ asset('admin/assets/css/style.css') }}" rel="stylesheet">

<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
<!--[if lt IE 9]>
    <script src="assets/https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="assets/https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
<![endif]-->
</head>
<body>
<!-- Preloader -->
<div class="preloader">
    <div class="cssload-speeding-wheel"></div>
</div>
<div id="wrapper">
  <!-- Navigation -->
  <nav class="navbar navbar-default navbar-static-top" style="margin-bottom: 0">
    <div class="navbar-header"> <a class="navbar-toggle hidden-sm hidden-md hidden-lg " href="javascript:void(0)" data-toggle="collapse" data-target=".navbar-collapse"><i class="ti-menu"></i></a>
      <div class="top-left-part"><a class="logo" href="">
      <i class="fa fa-home" style="display: inline; color:#ffffff; "></i>&nbsp;<span class="hidden-xs">
      <!-- <img src="{{ asset('images/favicon.png') }}" class="img-responsive" style="display: inline; position: relative; width:60%; top: -4px; " /> -->
      <img src="{{ asset('images/logo.png') }}" class="img-responsive" style="display: inline; position: relative; width:80%; top: -4px; " />
      </span></a></div>
      <ul class="nav navbar-top-links navbar-left hidden-xs">
        <li><a href="javascript:void(0);" class="open-close hidden-xs waves-effect waves-light"><i class="icon-arrow-left-circle ti-menu"></i></a></li>
        <!-- /.dropdown -->
        
        <li class="dropdown"> <a class="dropdown-toggle waves-effect waves-light" href="{{ URL::to('/') }}" target="_blank"><i class="icon-login"></i> Go to Website</a>
        </li>
        
      </ul>
      <ul class="nav navbar-top-links navbar-right pull-right">
        <li class="dropdown"> <a class="dropdown-toggle profile-pic" data-toggle="dropdown" href="#"> 
        <img src="{{ asset('admin/assets/images/users/icon.png') }}" alt="User" width="36" class="img-circle">
        <!-- <i class="fa fa-user-circle"></i>  -->
        <b class="hidden-xs">{{ Session::has('name') ? Session::get('name') : "User" }}</b> </a>
          <ul class="dropdown-menu dropdown-user">
            <li><a href="profile"><i class="ti-user"></i> My Profile</a></li>
            <li role="separator" class="divider"></li>
            <li><a href="change-password"><i class="ti-lock"></i> Change Password</a></li>
            <li role="separator" class="divider"></li>
            <li><a href="logout"><i class="fa fa-power-off"></i> Logout</a></li>
          </ul>
          <!-- /.dropdown-user -->
        </li>
        <!-- /.dropdown -->
      </ul>
    </div>
    <!-- /.navbar-header -->
    <!-- /.navbar-top-links -->
    <!-- /.navbar-static-side -->
  </nav>
  <!-- <div id="success-msg" style="display:none" class="myadmin-alert myadmin-alert-icon myadmin-alert-click alert3 myadmin-alert-top"></div>
  <div id="error-msg" style="display:none" class="myadmin-alert myadmin-alert-icon myadmin-alert-click alert6 myadmin-alert-top"></div> -->
  @include('templates.admin.partials.alerts')
  

  <div class="navbar-default sidebar nicescroll" role="navigation">
    <div class="sidebar-nav navbar-collapse ">
      <ul class="nav" id="side-menu">
        <li class="nav-small-cap">Menu</li>
        <li class=""> <a href="" class="waves-effect"><i class="fa fa-dashboard"></i> Dashboard</a> </li>
        <li> <a href="#" class="waves-effect"><i class="fa fa-users"></i> Members<span class="fa arrow"></span></a>
            <ul class="nav nav-second-level collapse">
                <li> <a href="departments" class="waves-effect"><i class="fa fa-bookmark" aria-hidden="true"></i> Departments</a> </li>
                <li> <a href="members" class="waves-effect"><i class="fa fa-users" aria-hidden="true"></i> Members</a> </li>
            </ul>
            <!-- /.nav-second-level -->
        </li>
        <li> <a href="#" class="waves-effect"><i class="fa fa-paper-plane"></i> Send SMS<span class="fa arrow"></span></a>
            <ul class="nav nav-second-level collapse">
                <li> <a href="sms-templates" class="waves-effect"><i class="fa fa-list" aria-hidden="true"></i> SMS Templates</a> </li>
                <li> <a href="compose-sms" class="waves-effect"><i class="fa fa-pencil-square-o" aria-hidden="true"></i> Compose SMS</a> </li>
                <li> <a href="sent-sms" class="waves-effect"><i class="fa fa-map" aria-hidden="true"></i> Sent SMS</a> </li>
                <li> <a href="birthday-notification" class="waves-effect"><i class="fa fa-gift" aria-hidden="true"></i> Birthday Notification</a> </li>
                <li> <a href="buy-sms-units" class="waves-effect"><i class="fa fa-shopping-cart" aria-hidden="true"></i> Buy SMS Units</a> </li>
            </ul>
            <!-- /.nav-second-level -->
        </li>
        <li> <a href="#" class="waves-effect"><i class="fa fa-money"></i> Donations<span class="fa arrow"></span></a>
            <ul class="nav nav-second-level collapse">
                <li> <a href="donation-categories" class="waves-effect"><i class="fa fa-list" aria-hidden="true"></i> Categories</a> </li>
                <li> <a href="donations" class="waves-effect"><i class="fa fa-money" aria-hidden="true"></i> Donations</a> </li>
            </ul>
            <!-- /.nav-second-level -->
        </li>
        <li> <a href="#" class="waves-effect"><i class="fa fa-photo"></i> Media Center<span class="fa arrow"></span></a>
            <ul class="nav nav-second-level collapse">
                <li> <a href="photo-gallery" class="waves-effect"><i class="fa fa-photo" aria-hidden="true"></i> Photo Gallery</a> </li>
                <li> <a href="music" class="waves-effect"><i class="fa fa-music" aria-hidden="true"></i> Music</a> </li>
                <li> <a href="video" class="waves-effect"><i class="fa fa-play-circle-o" aria-hidden="true"></i> Video</a> </li>
                <li> <a href="sermons" class="waves-effect"><i class="fa fa-bullseye" aria-hidden="true"></i> Sermons</a> </li>
                <li> <a href="announcements" class="waves-effect"><i class="fa fa-bullhorn" aria-hidden="true"></i> Announcements</a> </li>
                <li> <a href="testimonies" class="waves-effect"><i class="fa fa-yelp" aria-hidden="true"></i> Testimonies</a> </li>
                <li> <a href="holy-ghost-service" class="waves-effect"><i class="fa fa-video-camera" aria-hidden="true"></i> Holy Ghost Service</a> </li>
                <li> <a href="random-scriptures" class="waves-effect"><i class="fa fa-random" aria-hidden="true"></i> Random Scriptures</a> </li>
            </ul>
            <!-- /.nav-second-level -->
        </li>
        <li><a href="events"><i class="fa fa-star"></i> Events</a></li>
        <li><a href="blog"><i class="fa fa-rss"></i> Blog</a></li>
        <li> <a href="#" class="waves-effect"><i class="fa fa-comments"></i> Feedback<span class="fa arrow"></span></a>
            <ul class="nav nav-second-level collapse">
                <li> <a href="feedback/contact-us" class="waves-effect"><i class="fa fa-comments" aria-hidden="true"></i> Contact Us</a> </li>
                <li> <a href="feedback/ask-the-pastor" class="waves-effect"><i class="fa fa-comments" aria-hidden="true"></i> Ask The Pastor</a> </li>
                <li> <a href="feedback/prayer-request" class="waves-effect"><i class="fa fa-comments" aria-hidden="true"></i> Join The Workforce</a> </li>
                <li> <a href="feedback/join-workforce" class="waves-effect"><i class="fa fa-comments" aria-hidden="true"></i> Prayer Request</a> </li>
            </ul>
            <!-- /.nav-second-level -->
        </li>
        <li><a href="newsletter"><i class="fa fa-newspaper-o"></i> Newsletter</a></li>
        <li> <a href="{{ route('users.index') }}" class="waves-effect"><i class="ti-user fa-user"></i> Users</a> </li>
        @if ( Session::has('user_role_id') and Session::has('user_role_name') )
          @if( (Session::get('user_role_id') == 3) and (strtoupper(Session::get('user_role_name')) == "REPORT ADMIN") )
            <li> <a href="Subscriptions" class="waves-effect"><i class="ti-bar-chart fa-bar-chart"></i> Subscriptions</a> </li>
          @else
            <li> <a href="Products" class="waves-effect"><i class="ti-upload fa fa-upload" aria-hidden="true"></i> Products</a> </li>
          @endif

          @if( (Session::get('user_role_id') == 1) and (strtoupper(Session::get('user_role_name')) == "SUPER ADMINISTRATOR") )
            <li> <a href="{{ route('users.index') }}" class="waves-effect"><i class="ti-user fa-user"></i> Users</a> </li>
          @endif
        @endif
      </ul>
    </div>
    <!-- /.sidebar-collapse -->
  </div>
  @yield('content')

    <footer class="footer text-center"> <?php echo date('Y'); ?> &copy; Powered By <a href="http://neegles.com" target="_blank">Neegles</a>. </footer>
</div>
<!-- /#wrapper -->
<!-- jQuery -->
<script src="{{ asset('admin/assets/bower_components/jquery/dist/jquery.min.js') }}"></script>
<!-- Bootstrap Core JavaScript -->
<script src="{{ asset('admin/assets/bower_components/bootstrap/dist/js/bootstrap.min.js') }}"></script>
<!-- Menu Plugin JavaScript -->
<script src="{{ asset('admin/assets/bower_components/metisMenu/dist/metisMenu.min.js') }}"></script>
<!--Nice scroll JavaScript -->
<script src="{{ asset('admin/assets/js/jquery.nicescroll.js') }}"></script>

<!--Morris JavaScript -->
<!--<script src="assets/bower_components/raphael/raphael-min.js"></script>
<script src="assets/bower_components/morrisjs/morris.js"></script>-->
<!--Wave Effects -->
<script src="{{ asset('admin/assets/js/waves.js') }}"></script>
<!-- Custom Theme JavaScript -->
<script src="{{ asset('admin/assets/js/myadmin.js') }}"></script>
<script src="{{ asset('admin/assets/js/jasny-bootstrap.js') }}"></script>
<!--<script src="assets/js/dashboard1.js"></script>-->
<!--<script src="assets/bower_components/jquery-sparkline/jquery.sparkline.min.js"></script>
<script src="assets/bower_components/jquery-sparkline/jquery.charts-sparkline.js"></script>-->
<script type="text/javascript">
  $(document).ready(function() {
      $('#error-msg').delay(7000).fadeOut(400);
      $('#success-msg').delay(7000).fadeOut(400);
    {{--
    @if ($errors->any())
      $("#error-msg").fadeToggle(350);
    @endif
    @if (Session::has('success') or Session::has('info') or Session::has('warning') or Session::has('error'))
      $("#success-msg").fadeToggle(350);
    @endif
    --}}
  });
</script>

</body>
</html>
