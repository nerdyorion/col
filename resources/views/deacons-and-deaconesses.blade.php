@extends('base')
@section('title')Deacons / Deaconesses @parent @stop
@section('meta')
@parent
<meta name="description" content="The Redeemed Christian Church of God Chapel of Life - A place of worship where Jesus is enthroned, the undiluted Word of God is preached, the presence of God dwells. Join us for any of our services and experience the love and power of God in all areas of your life. Jesus loves you." />
<meta name="keywords" content="chapel of life, rccg, church, redeemed, christ, love, faith, peace, christian faith, religion, festac, lagos, amuwo odofin, jesus, mile two, nigeria" />
@stop
@section('styles')
@parent

<link rel="stylesheet" type="text/css" href="css/colors/blue.css" title="color1" />
@stop

@section('content')
<div class="page-top">
	<div class="parallax" style="background:url(images/parallax1.jpg);"></div>	
	<div class="container"> 
		<h1>Deacons / Deaconesses</h1>
		<ul>
			<li><a href="./" title="">Home</a></li>
			<li><a href="#" title="">The Church</a></li>
			<li><a href="#" title="">Our Pastoral Team</a></li>
			<li><a href="#" title="">Deacons / Deaconesses</a></li>
		</ul>
	</div>
</div>

<section>
	<div class="block">
		<div class="container">
			<div class="row">
				<div class="col-md-12">
					<div class="remove-ext">
						<div class="team-page">
							<div class="row">
								<div class="col-md-3">
									<div class="member">
										<div class="team">
											<div class="team-img">
												<img src="images/resource/team1.jpg" alt="" />
											</div>
											<div class="member-detail">
												<h3><a href="team-single.html" title="">Jhon Smith</a></h3>
												<span>SENIOR PASTOR</span>
												<p>Lorem ipsum dolor sit a onsec tetiri adien tashn commodo leg tashn dol tashin tyons piscin.</p>
											</div>
										</div>
									</div><!-- MEMBER -->
								</div>

								<div class="col-md-3">
									<div class="member">
										<div class="team">
											<div class="team-img">
												<img src="images/resource/team2.jpg" alt="" />
											</div>
											<div class="member-detail">
												<h3><a href="team-single.html" title="">Anni</a></h3>
												<span>SENIOR PASTOR</span>
												<p>Lorem ipsum dolor sit a onsec tetiri adien tashn commodo leg tashn dol tashin tyons piscin.</p>
											</div>
										</div>
									</div><!-- MEMBER -->
								</div>

								<div class="col-md-3">
									<div class="member">
										<div class="team">
											<div class="team-img">
												<img src="images/resource/team3.jpg" alt="" />
											</div>
											<div class="member-detail">
												<h3><a href="team-single.html" title="">Hallen</a></h3>
												<span>SENIOR PASTOR</span>
												<p>Lorem ipsum dolor sit a onsec tetiri adien tashn commodo leg tashn dol tashin tyons piscin.</p>
											</div>
										</div>
									</div><!-- MEMBER -->
								</div>

								<div class="col-md-3">
									<div class="member">
										<div class="team">
											<div class="team-img">
												<img src="images/resource/team4.jpg" alt="" />
											</div>
											<div class="member-detail">
												<h3><a href="team-single.html" title="">H.A Davidson</a></h3>
												<span>SENIOR PASTOR</span>
												<p>Lorem ipsum dolor sit a onsec tetiri adien tashn commodo leg tashn dol tashin tyons piscin.</p>
											</div>
										</div>
									</div><!-- MEMBER -->
								</div>


								<div class="col-md-3">
									<div class="member">
										<div class="team">
											<div class="team-img">
												<img src="images/resource/team5.jpg" alt="" />
											</div>
											<div class="member-detail">
												<h3><a href="team-single.html" title="">Sophy</a></h3>
												<span>SENIOR PASTOR</span>
												<p>Lorem ipsum dolor sit a onsec tetiri adien tashn commodo leg tashn dol tashin tyons piscin.</p>
											</div>
										</div>

									</div><!-- MEMBER -->
								</div>

								<div class="col-md-3">
									<div class="member">
										<div class="team">
											<div class="team-img">
												<img src="images/resource/team6.jpg" alt="" />
											</div>
											<div class="member-detail">
												<h3><a href="team-single.html" title="">Bob Smith</a></h3>
												<span>SENIOR PASTOR</span>
												<p>Lorem ipsum dolor sit a onsec tetiri adien tashn commodo leg tashn dol tashin tyons piscin.</p>
											</div>
										</div>

									</div><!-- MEMBER -->
								</div>

								<div class="col-md-3">
									<div class="member">
										<div class="team">
											<div class="team-img">
												<img src="images/resource/team1.jpg" alt="" />
											</div>
											<div class="member-detail">
												<h3><a href="team-single.html" title="">Jhon Smith</a></h3>
												<span>SENIOR PASTOR</span>
												<p>Lorem ipsum dolor sit a onsec tetiri adien tashn commodo leg tashn dol tashin tyons piscin.</p>
											</div>
										</div>
									</div><!-- MEMBER -->
								</div>

								<div class="col-md-3">
									<div class="member">
										<div class="team">
											<div class="team-img">
												<img src="images/resource/team2.jpg" alt="" />
											</div>
											<div class="member-detail">
												<h3><a href="team-single.html" title="">Jhon Smith</a></h3>
												<span>SENIOR PASTOR</span>
												<p>Lorem ipsum dolor sit a onsec tetiri adien tashn commodo leg tashn dol tashin tyons piscin.</p>
											</div>
										</div>
									</div><!-- MEMBER -->
								</div>

							</div>
						</div>						
					</div>

					<div class="theme-pagination">
						<ul class="pagination">
							<li><a href="#"><i class="fa fa-angle-left"></i></a></li>
							<li><a href="#">1</a></li>
							<li><a href="#">2</a></li>
							<li><a href="#">3</a></li>
							<li><a href="#">4</a></li>
							<li><a href="#">5</a></li>
							<li><a href="#"><i class="fa fa-angle-right"></i></a></li>
						</ul>
					</div><!-- PAGINATION -->
					
				</div>
			</div>
		</div>
	</div>
</section>
@stop
@section('after_footer')
	<!-- SCRIPTS-->
	<script type="text/javascript" src="js/modernizr.custom.17475.js"></script>

	<script src="js/jquery.1.10.2.js" type="text/javascript"></script>
    <script src="js/bootstrap.min.js"></script>
	<script src="js/jquery.prettyPhoto.js" type="text/javascript"></script>
    <script src="js/script.js"></script>
    <script src="js/styleswitcher.js"></script>
	<script type="text/javascript" src="js/jquery.downCount.js"></script> 
@stop