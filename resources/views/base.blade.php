<!DOCTYPE html>

<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>@section('title') - Chapel of Life | The Redeemed Christian Church of God | Come and let's worship at the feet of Jesus @show</title>
@section('meta')
<meta name="viewport" content="width=device-width, initial-scale=1.0">
@show
<link rel="shortcut icon" type="image/x-icon" href="images/favicon.png">
@section('styles')
<!-- Google Fonts -->
<!-- 
<link href='http://fonts.googleapis.com/css?family=Noto+Serif:400,400italic,700,700italic' rel='stylesheet' type='text/css'>
<link href='http://fonts.googleapis.com/css?family=Noto+Sans:400,700,400italic,700italic' rel='stylesheet' type='text/css'> -->
<!-- Styles -->
<link href="font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css" />
<link href="css/bootstrap.min.css" rel="stylesheet" type="text/css" />
<link rel="stylesheet" href="css/owl-carousel.css" type="text/css" />
<link rel="stylesheet" href="css/style.css" type="text/css" />
<link href="css/responsive.css" rel="stylesheet" type="text/css" />

{{--
<!-- 
// Original DEMO: http://themes.webinane.com/deeds/
<link rel="stylesheet" type="text/css" href="css/colors/red.css" title="color1" />
<link rel="alternate stylesheet" type="text/css" href="css/colors/wedgewood.css" title="color2" />
<link rel="alternate stylesheet" type="text/css" href="css/colors/blue.css" title="color3" />
<link rel="alternate stylesheet" type="text/css" href="css/colors/green.css" title="color4" />
<link rel="alternate stylesheet" type="text/css" href="css/colors/darkgreen.css" title="color5" />
 -->
--}}
@show
</head>

<body>
<div class="theme-layout">
<header class="header2">
	<div class="topbar">
		<div class="container">
			<div class="header-timer">
				<p style="background-color: #f0283b;">Upcoming Event:</p>
				<ul class="headercounter">
					<li> <span class="days">00</span>
					<p class="days_ref">DAYS</p>
					</li>
					<li> <span class="hours">00</span>
					<p class="hours_ref">HOURS</p>
					</li>
					<li> <span class="minutes">00</span>
					<p class="minutes_ref">MINTS</p>
					</li>
					<li> <span class="seconds">00</span>
					<p class="seconds_ref">SECS</p>
					</li>
				</ul>
			</div>
			{{--
			<p><i class="fa fa-mobile"></i> Call us: <a href="tel:+1234567890">+123 456 7890</a></p><!--- CONTACT -->
			<p><i class="fa fa-envelope"></i>  <a href="mailto:info@rccgchapeloflife.org">info@rccgchapeloflife.org</a></p><!--- EMAIL -->
			<ul class="social-media">
				<li><a href="https://twitter.com/" title="Follow us on Twitter" target="_blank"><i class="fa fa-twitter"></i></a></li>
				<li><a href="https://facebook.com/" title="Connect with us on FaceBook" target="_blank"><i class="fa fa-facebook"></i></a></li>
			</ul>
			 --}}
			<div class="cart-dropdown">
				<p><a href="manage" style="color: #f0283b;"><i class="fa fa-lock"> Login</i></a></p>
			</div>
			<div class="cart-dropdown">
				<p><a href="announcements" style="color: #f0283b;"><i class="fa fa-bullhorn"> Announcements</i></a></p>
			</div>
		</div>
	</div><!--- TOP BAR -->
	<nav>
		<div class="container">
			<div class="logo">
				<a href="./" title=""><img src="images/logo.png" alt="" /></a>
			</div><!--- LOGO -->
			<span class="menu-btn">MENU</span>			
			<ul>
				<li><a href="./" title="">Home</a></li>
				<li class="menu-item-has-children"><a href="#" title="">Online Giving</a>
					<ul>
						<li class="menu-item-has-children"><a href="#" title="">Projects</a>
							<ul>
								<li><a href="church-projects" title="">Church Projects</a></li>
								<li class="menu-item-has-children"><a href="#" title="">RCCG HQ</a>
									<ul>
										<li><a href="http://rccg.org/partners-75/" title="" target="_blank">Partners 75</a></li>
										<li><a href="http://www.trccg.org/cp/" title="" target="_blank">Covenant Partners</a></li>
										<li><a href="https://www.trccg.org/holyGhost/trccg.org/offering.php?newauditorium=New%20Auditorium%20Building" title="" target="_blank">New Auditorium Building</a></li>
										<li><a href="https://www.trccg.org/holyGhost/trccg.org/offering.php?congress=Holyghost%20Congress%20Offering" title="" target="_blank">Holy Ghost Congress</a></li>
										
									</ul>
								</li>								
							</ul>
						</li>
						<li><a href="tithes-offering-first-fruit" title="">Tithes / Offering / First Fruit</a></li>						
					</ul>
				</li>
				<li class="menu-item-has-children"><a href="#" title="">Media Center</a>
					<ul>
						<li><a href="gallery" title="">Photo Gallery</a></li>
						<li><a href="music" title="">Music</a></li>
						<li><a href="videos" title="">Videos</a></li>
						<li><a href="sermons" title="">Sermons</a></li>
						<li><a href="announcements" title="">Announcements</a></li>
						<li><a href="testimonies" title="">Testimonies</a></li>
						<li class="menu-item-has-children"><a href="#" title="">Live Stream</a>
							<ul>
								<!-- <li><a href="https://livestream.com/rccgonline" title="" target="_blank">Holy Ghost Service</a></li> -->
								<li><a href="watch-holy-ghost-service" title="">Holy Ghost Service</a></li>
								<li><a href="#" title="">Church Service</a></li>
							</ul>
						</li>
					</ul>
				</li>
				<li><a href="events" title="">Events</a></li>
				<li class="menu-item-has-children"><a href="#" title="">Ministries</a>
					<ul>
						<li class="menu-item-has-children"><a href="#" title="">Helps</a>
							<ul>
								<li><a href="sanitation" title="">Sanitation</a></li>
								<li><a href="choir" title="">Choir</a></li>
							</ul>
						</li>
						<li><a href="good-women" title="">Good Women</a></li>
						<li><a href="excellent-men" title="">Excellent Men</a></li>
						<li><a href="children" title="">Children</a></li>
						<li><a href="prayer" title="">Prayer</a></li>	
						<li><a href="house-fellowship" title="">House Fellowship</a></li>						
					</ul>
				</li>
				<li class="menu-item-has-children"><a href="#" title="">The Church</a>
					<ul>
						<li><a href="about" title="">About Us</a></li>
						<li><a href="pastor-in-charge-of-province-32" title="">Pastor In Charge of Province 32</a></li>
						<li class="menu-item-has-children"><a href="#" title="">Our Pastoral Team</a>
							<ul>
								<li><a href="the-pastor" title="">The Pastor</a></li>
								<li><a href="deacons-and-deaconesses" title="">Deacons / Deaconesses</a></li>
								<li><a href="heads-of-departments" title="">Heads of Departments</a></li>
								
							</ul>
						</li>
						<li><a href="service-times" title="">Service Times</a></li>
						<li><a href="http://rccg.org/" title="" target="_blank">RCCG WorldWide</a></li>						
					</ul>
				</li>
				<li><a href="blog" title="">Blog</a></li>
				<li class="menu-item-has-children"><a href="#" title="">Get In Touch</a>
					<ul>
						<li><a href="contact" title="">Contact Us</a></li>
						<li><a href="ask-the-pastor" title="">Ask The Pastor</a></li>
						<li><a href="join-the-workforce" title="">Join The Workforce</a></li>
						<li><a href="prayer-request" title="">Prayer Request</a></li>						
					</ul>
				</li>
			</ul>
		</div>
	</nav>
</header><!--- HEADER -->
@yield('content')
<footer>
	<div class="block blackish">
	<div class="parallax" style="background:url(images/parallax5.jpg);"></div>
		<div class="container">
			<div class="row"> 
				<div class="col-md-4">
					<div class="widget">
						<div class="about">
							<img src="images/logo.png" alt="Chapel of Life" />
							<span>Worship with us</span>
							<p>Jesus Christ the same yesterday, and today, and for ever. <i>Hebrews 13:8</i></p>
							<div class="contact">
								<ul>
									<li><span><i class="fa fa-phone"></i>Phone :</span> ( +123 456 78 99 ) ( +123 456 78 99 )</li>
									<li><span><i class="fa fa-envelope"></i>Email:</span> info@rccgchapeloflife.org</li>
									<li><span><i class="fa fa-home"></i>Address:</span> Plot 7, Crystal Estate 3<sup>rd</sup> Gate, Durbar Road, Amuwo-Odofin, Lagos.</li>
								</ul>
							</div>							
							<ul class="social-media">
								<li><a href="https://twitter.com/" title="Follow us on Twitter" target="_blank"><i class="fa fa-twitter"></i></a></li>
								<li><a href="https://facebook.com/" title="Connect with us on FaceBook" target="_blank"><i class="fa fa-facebook"></i></a></li>
							</ul>															
						</div>
					</div>
				</div><!-- ABOUT WIDGET -->
				<div class="col-md-4">
					<div class="widget">
						<div class="widget-title"><h4>Newsletter Signup</h4></div>
						<form method="post" action="">
							<input type="email" name="newsletter_email" placeholder="Enter Your Email Address" required="required">
							<input type="text" name="newsletter_captcha" placeholder="10 + 7 =" required="required">
							{{ csrf_field() }}
							<input type="submit" value="SIGN UP NOW">
						</form>
						<div class="clearfix"></div>
						<h5 class="quick-links"><a href="about" title=""><i class="fa fa-hand-o-right"></i> About Us</a></h5>
						<h5 class="quick-links"><a href="service-times" title=""><i class="fa fa-hand-o-right"></i> Our Service Times</a></h5>
						<h5 class="quick-links"><a href="announcements" title=""><i class="fa fa-hand-o-right"></i> Church News</a></h5>
						<h5 class="quick-links"><a href="http://rccg.org/" title="" target="_blank"><i class="fa fa-hand-o-right"></i> RCCG WorldWide</a></h5>
					</div>
				</div><!-- QUICK MESSAGE WIDGET -->

				<div class="col-md-4">
					<div class="widget">
						<div class="widget-title"><h4>Recent Blog</h4></div>
						<div class="remove-ext">
							<div class="widget-blog">
								<div class="widget-blog-img"><img src="images/resource/widget-blog.jpg" alt="" /></div>
								<h6><a href="blog-single.html" title=""> Consectetur Adipisicing.</a></h6>
								<p>Homemade cream cheese mints These are amazing!Christmas!!- must try!</p>
								<span><i class="fa fa-calendar-o"></i> November 01, 2017</span>
							</div><!-- WIDGET BLOG -->

							<div class="widget-blog">
								<div class="widget-blog-img"><img src="images/resource/widget-blog2.jpg" alt="" /></div>
								<h6><a href="blog-single.html" title=""> Consectetur Adipisicing.</a></h6>
								<p>Homemade cream cheese mints These are amazing!Christmas!!- must try!</p>
								<span><i class="fa fa-calendar-o"></i> November 01, 2017</span>
							</div><!-- WIDGET BLOG -->
						</div>
						
					</div>
				</div><!-- RECENT BLOG -->				
			</div>
		</div>
	</div>
</footer><!-- FOOTER -->

<div class="bottom-footer">
	<div class="container">
		<p>&copy; {{ date("Y") }} <a title="">Chapel of Life</a>. All rights reserved. Site Credits: <a target="_blank" href="http://neegles.com" title="">Neegles</a></p>

	</div>
</div><!-- BOTTOM FOOTER STRIP -->

</div>
@section('after_footer')
@show

<script class="source" type="text/javascript">
	$('.headercounter').downCount({
		date: '08/25/2018 12:00:00',
		offset: +10
	});
</script> 

<!-- <script id="dsq-count-scr" src="//rccg-chapel-of-life.disqus.com/count.js" async></script> -->
</body>