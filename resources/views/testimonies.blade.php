@extends('base')
@section('title')Testimonies @parent @stop
@section('meta')
@parent
<meta name="description" content="Read life changing testimonies of everyday people like you - The Redeemed Christian Church of God Chapel of Life" />
<meta name="keywords" content="testimonies, experiences, life experiences, god's faithfulness, chapel of life, rccg, church, redeemed, christ, love, faith, peace, christian faith, religion, festac, lagos, amuwo odofin, jesus, mile two, nigeria" />
@stop
@section('styles')
@parent

<link rel="stylesheet" href="css/mediaelementplayer.min.css" />
<link rel="stylesheet" type="text/css" href="css/colors/blue.css" title="color1" />
@stop

@section('content')
<div class="page-top">
	<div class="parallax" style="background:url(images/parallax1.jpg);"></div>	
	<div class="container"> 
		<h1>TESTIMONIES</h1>
		<ul>
			<li><a href="./" title="">Home</a></li>
			<li><a href="#">Media Center</a></li>
			<li><a href="#" title="">Testimonies</a></li>
		</ul>
	</div>
</div><!--- PAGE TOP -->

<section>
	<div class="block">
		<div class="container">
			<div class="row">
				<div class="col-md-12 column">
					<div class="row">
						<div class="remove-ext">
							<div class="prayers-columns">
							<div class="col-md-6 column">
								<div class="prayer">
									<p>I thank God for granting my sister safe delivery of her twin. :)</p>
									<img src="images/resource/user-avatar.png" alt="" />
									<h4>Bro. James Cole</h4>
									<span>30 June 2018</span>
								</div>
							</div>
							<div class="col-md-6 column">
								<div class="prayer">
									<p>Aenean leo vene quam. Pellntes ique ornare semew modte venenatis vestibum. Cras mattis citugir purus. vene quam. Pellntes ique ornare seeim eiusm odteyivenenatis vestibumas mattis citur.</p>
									<img src="images/resource/user-avatar.png" alt="" />
									<h4>ALEXANDER SAMOKHIN</h4>
									<span>30 June 2015</span>
								</div>
							</div>
							<div class="col-md-6 column">
								<div class="prayer">
									<p>Aenean leo vene quam. Pellntes ique ornare semew modte venenatis vestibum. Cras mattis citugir purus. Aenean leo vene quam. Pellntes ique ornare semew modte venenatis vestibum. Cras mattis citugir purus. vene quam. vene quam. Pellntes ique ornare seeim eiusm odteyivenenatis vestibumas mattis citur.</p>
									<img src="images/resource/user-avatar.png" alt="" />
									<h4>ALEXANDER SAMOKHIN</h4>
									<span>30 June 2015</span>
								</div>
							</div>
							<div class="col-md-6 column">
								<div class="prayer">
									<p>Aenean leo vene quam. Pellntes ique ornare semew modte venenatis vestibum. Cras mattis citugir purus. vene quam. Pellntes ique ornare seeim eiusm odteyivenenatis vestibumas mattis citur.</p>
									<img src="images/resource/user-avatar.png" alt="" />
									<h4>ALEXANDER SAMOKHIN</h4>
									<span>30 June 2015</span>
								</div>
							</div>
							<div class="col-md-6 column">
								<div class="prayer">
									<p>Aenean leo vene quam. Pellntes ique ornare semew modte venenatis vestibum. Cras mattis citugir purus. vene quam. Pellntes ique ornare seeim eiusm odteyivenenatis vestibumas mattis citur.</p>
									<img src="images/resource/user-avatar.png" alt="" />
									<h4>ALEXANDER SAMOKHIN</h4>
									<span>30 June 2015</span>
								</div>
							</div>
							<div class="col-md-6 column">
								<div class="prayer">
									<p>Aenean leo vene quam. Pellntes ique ornare semew modte venenatis vestibum. Cras mattis citugir purus. vene quam. Pellntes ique ornare seeim eiusm odteyivenenatis vestibumas mattis citur.</p>
									<img src="images/resource/user-avatar.png" alt="" />
									<h4>ALEXANDER SAMOKHIN</h4>
									<span>30 June 2015</span>
								</div>
							</div>
							</div>
						</div>
					</div>


						<div class="theme-pagination">
							<ul class="pagination">
								<li><a href="#"><i class="fa fa-angle-left"></i></a></li>
								<li><a href="#">1</a></li>
								<li><a href="#">2</a></li>
								<li><a href="#">3</a></li>
								<li><a href="#">4</a></li>
								<li><a href="#">5</a></li>
								<li><a href="#"><i class="fa fa-angle-right"></i></a></li>
							</ul>
						</div><!-- PAGINATION -->
					
				</div>				
			</div>
		</div>
	</div>
</section>	
@stop
@section('after_footer')
	<!-- SCRIPTS-->
	<script type="text/javascript" src="js/modernizr.custom.17475.js"></script>

	<script src="js/jquery.1.10.2.js" type="text/javascript"></script>
    <script src="js/bootstrap.min.js"></script>
	<script src="js/jquery.prettyPhoto.js" type="text/javascript"></script>
    <script src="js/script.js"></script>
    <script src="js/styleswitcher.js"></script>
	<script type="text/javascript" src="js/jquery.downCount.js"></script> 

	<script src="js/jquery.isotope.min.js"></script>
	<script>
	$(window).load(function(){
		$(function(){
			var $portfolio = $('.prayers-columns');
			$portfolio.isotope({
			masonry: {
			  columnWidth: 1
			}
			});
		});
	});

	</script>
@stop