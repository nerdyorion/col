@extends('base')
@section('title')Hallelujah | August Holy Ghost Service 2017 @parent @stop
@section('meta')
@parent
<meta name="description" content="Watch Hallelujah August 2017 Holy Ghost Service Live @ The Redeemed Christian Church of God Chapel of Life - A place of worship where Jesus is enthroned, the undiluted Word of God is preached, the presence of God dwells. Join us for any of our services and experience the love and power of God in all areas of your life. Jesus loves you." />
<meta name="keywords" content="holy ghost service, watch live, chapel of life, rccg, church, redeemed, christ, love, faith, peace, christian faith, religion, festac, lagos, amuwo odofin, jesus, mile two, nigeria" />
@stop
@section('styles')
@parent

<link rel="stylesheet" href="css/mediaelementplayer.min.css" />
<link rel="stylesheet" type="text/css" href="css/colors/blue.css" title="color1" />
@stop

@section('content')
<div class="page-top">
	<div class="parallax" style="background:url(images/parallax1.jpg);"></div>	
	<div class="container"> 
		<h1>HALLELUJAH</h1>
		<ul>
			<li><a href="./" title="">Home</a></li>
			<li><a href="#">Media Center</a></li>
			<li><a href="#">Live Stream</a></li>
			<li><a href="watch-holy-ghost-service" title="">Holy Ghost Service</a></li>
			<li><a href="#" title="">Hallelujah (Aug 2017)</a></li>
		</ul>
	</div>
</div><!--- PAGE TOP -->

<section>
	<div class="block">
		<div class="container">
			<div class="row">
				<div class="col-md-12">
					<div class="remove-ext">
						<div class="row">
							<!-- 
								embed_id = 1503053823 
								// id="ls_embed_" + embed_id

								accountId = 204531
								eventId = 7648867
								width = 640; height = 360 // default for now ; no need to set

								// src="https://livestream.com/accounts/" + accountId + "/events/" + eventId + "/player?width=640&height=360&enableInfoAndActivity=true&defaultDrawer=&autoPlay=true&mute=false 
							-->
							<iframe id="ls_embed_1503053823" src="https://livestream.com/accounts/204531/events/7648867/player?width=640&height=360&enableInfoAndActivity=true&defaultDrawer=&autoPlay=true&mute=false" width="640" height="360" frameborder="0" scrolling="no" allowfullscreen> </iframe> <!-- 2017 Convention Holy Ghost Service -->
						</div>						
					</div>


					<div class="social-corner">
						<p>Think people should hear about this?</p>
						<ul class="social-media">
							<li><a href="#" title=""><i class="fa fa-twitter"></i></a></li>
							<li><a href="#" title=""><i class="fa fa-facebook"></i></a></li>
						</ul>
					</div>
					
				</div>
			</div>
		</div>
	</div>
</section>	
@stop
@section('after_footer')
	<!-- SCRIPTS-->
	<script type="text/javascript" src="js/modernizr.custom.17475.js"></script>

	<script src="js/jquery.1.10.2.js" type="text/javascript"></script>
    <script src="js/bootstrap.min.js"></script>
	<script src="js/jquery.prettyPhoto.js" type="text/javascript"></script>
    <script src="js/script.js"></script>
    <script src="js/styleswitcher.js"></script>
	<script type="text/javascript" src="js/jquery.downCount.js"></script> 

	<script src="js/jquery.isotope.min.js"></script>
	<script>
	$(window).load(function(){
		$(function(){
			var $portfolio = $('.mas-gallery');
			$portfolio.isotope({
			masonry: {
			  columnWidth: 1
			}
			});
		});
	});

	</script>
@stop