@extends('base')

@section('title')Welcome @parent @stop
@section('meta')
@parent
<meta name="description" content="The Redeemed Christian Church of God Chapel of Life - A place of worship where Jesus is enthroned, the undiluted Word of God is preached, the presence of God dwells. Join us for any of our services and experience the love and power of God in all areas of your life. Jesus loves you." />
<meta name="keywords" content="chapel of life, rccg, church, redeemed, christ, love, faith, peace, christian faith, religion, festac, lagos, amuwo odofin, jesus, mile two, nigeria" />
@stop
@section('styles')
@parent

<link rel="stylesheet" href="css/mediaelementplayer.min.css" />
<link href="css/revolution.css" rel="stylesheet" type="text/css" />
<link rel="stylesheet" type="text/css" href="css/colors/blue.css" title="color1" />
@stop

@section('content')
<div class="slider">
	<div class="tp-banner-container">
		<div class="tp-banner">
			<ul>	
				<li data-transition="fade" data-slotamount="10" data-masterspeed="1000" >
					<img src="images/resource/slide10.jpg"  alt="slidebg3"  data-bgfit="cover" data-bgposition="left top" data-bgrepeat="no-repeat">
					<div class="tp-caption sfl" data-x="800" data-y="100" data-speed="1000" data-start="1000" data-captionhidden="on" style="z-index:2;"><img src="images/resource/slide9-man.png" alt="" /></div>
					<div class="tp-caption slide10-title sfr" data-x="0" data-y="150" data-speed="500" data-start="1000"  data-captionhidden="on" style="z-index:2; font-size: 49px;">Pray For Yourself <span>Be Peaceful</span></div>
					<div class="tp-caption slide10-text sfr" data-x="0" data-y="210" data-speed="500" data-start="1300"  data-captionhidden="on" style="z-index:2; font-size: 18px;">1 Cor 15:10 "But by the grace of God I am what I am..."</div>
					<a href="#" title class="tp-caption slide10-button sfr" data-x="0" data-y="280" data-speed="500" data-start="1500"  data-captionhidden="on" style="z-index:2; font-size: 13px; padding:10px 30px; line-height:15px;font-size: 13px;">Read More</a>
				</li>

				<li data-transition="fade" data-slotamount="10" data-masterspeed="1000" >
					<img src="images/resource/slide11.jpg"  alt="slidebg3"  data-bgfit="cover" data-bgposition="left top" data-bgrepeat="no-repeat">
					<div class="tp-caption slide11-title customin" data-x="0" data-y="120" data-speed="800" data-start="1000" data-customin="x:0;y:0;z:0;rotationX:0;rotationY:-40deg;rotationZ:0;scaleX:1;scaleY:1;skewX:0;skewY:0;opacity:0;transformPerspective:200;transformOrigin:50% 0%;" data-captionhidden="on" style="z-index:2; font-size: 37px;">Save Our Future <span>HELP KIDS</span></div>
					<div class="tp-caption slide11-box coloured-box skewfromleft " data-x="0" data-y="180" data-speed="800" data-start="1300"  data-captionhidden="on" style="z-index:2; font-size: 18px; padding:5px 30px; line-height:25px;">These poor kids need your special attention.</div>
					<div class="tp-caption slide11-box skewfromleft " data-x="0" data-y="230" data-speed="800" data-start="1600"  data-captionhidden="on" style="z-index:2; font-size: 18px; padding:5px 30px; line-height:25px;">Step ahead to save this talent.</div>
					<a data-toggle="modal" data-target="#myModal" title="" class="tp-caption slide-blk-button skewfromleft " data-x="0" data-y="300" data-speed="800" data-start="1900"  data-captionhidden="on" style="z-index:2; font-size: 14px; padding:8px 30px; line-height:25px;"><i class="fa fa-shopping-cart"></i> Donate Now</a>
				</li>

				<li data-transition="fade" data-slotamount="10" data-masterspeed="1000" >
					<img src="images/resource/slide12.jpg"  alt="slidebg3"  data-bgfit="cover" data-bgposition="left top" data-bgrepeat="no-repeat">

					<div class="tp-caption slide12-box lft" data-x="500" data-y="80" data-speed="800" data-start="1000"  data-captionhidden="on" style="z-index:2; font-size: 36px; padding:10px 30px;">Get Everyone Forgiveness Today</div>

					<div class="tp-caption slide12-box2 lfr" data-x="500" data-y="140" data-speed="800" data-start="1300"  data-captionhidden="on" style="z-index:2; font-size: 26px; padding:5px 30px;">Powerfully Responsive</div>

					<div class="tp-caption slide12-box2 lfr" data-x="500" data-y="190" data-speed="800" data-start="1600"  data-captionhidden="on" style="z-index:2; font-size: 26px; padding:5px 30px;">Forgiveness is the most churchelly </div>

					<a data-toggle="modal" data-target="#myModal" title="" class="tp-caption slide-blk-button lfb" data-x="500" data-y="280" data-speed="800" data-start="2000"  data-captionhidden="on" style="z-index:2; font-size: 14px; padding:8px 30px; line-height:25px;"><i class="fa fa-shopping-cart"></i> Donate Now</a>
				</li> 
			</ul>
		</div>	
	</div><!-- REVOLUTION SLIDER -->	
</div>

<section>
	<div class="block whitish">
	<div class="parallax" style="background:url(images/parallax6.jpg);"></div>
		<div class="container">
			<div class="row">
				<div class="col-md-12 column">
					<div class="welcome">
						<h1>Welcome to <span>Chapel Of Life</span></h1>
						<p>The Redeemed Christian Church of God Chapel of Life - A place of worship where Jesus is enthroned, the undiluted Word of God is preached, and the presence of God dwells.</p>
						<a href="about" title="">READ MORE</a>

						<!-- 
						<iframe   id="ls_embed_{embed_id}"   src="//livestream.com/accounts/{accountId}/events/{eventId}/player?width={width}&height={height}&enableInfoAndActivity=true&autoPlay=true&mute=false"   width="{width}" height="{height}" frameborder="0" scrolling="no" allowfullscreen> </iframe>

						<iframe id="ls_embed_1503053823" src="https://livestream.com/accounts/204531/events/7648867/player?width=600&height=400&enableInfoAndActivity=true&autoPlay=true&mute=false" width="600" height="400" frameborder="0" scrolling="no" allowfullscreen></iframe> -->

						<!-- <script type="text/javascript" data-embed_id="ls_embed_{embedId}" src="//livestream.com/assets/plugins/referrer_tracking.js"> -->
						</script>

					</div>
				</div>
			</div>
		</div>
	</div>
</section>

<section>
	<div class="block">
		<div class="container">
			<div class="row">
				<div class="col-md-4 column">
					<div class="service-block">
						<div class="service-image">
							<img src="images/resource/partnerships.jpg" alt="" />
							<i class="fa fa-codepen"></i>
						</div>
						<h3>Our Services</h3>
						<p>We welcome you to worship with us in any of our services. Check our Service Times below.</p>
						<a href="service-times" title="">View</a>
					</div>
				</div>

				<div class="col-md-4 column">
					<div class="service-block">
						<div class="service-image">
							<img src="images/resource/prayers.jpg" alt="" />
							<i class="fa fa-vine"></i>
						</div>
						<h3>Our Sermons</h3>
						<p>Faith comes by hearing and hearing by the Word. Listen to the Word below.</p>
						<a href="sermons" title="">Explore</a>
					</div>
				</div>

				<div class="col-md-4 column">
					<div class="service-block">
						<div class="service-image">
							<img src="images/resource/worship.jpg" alt="" />
							<i class="fa fa-stumbleupon"></i>
						</div>
						<h3>Our Gallery</h3>
						<p>View our captured moments. Join us to make more fulfilling and life-impacting memories together.</p>
						<a href="gallery" title="">Explore</a>
					</div>
				</div>

			</div>
		</div>
	</div>
</section>

<section>
	<div class="block remove-gap">
		<div class="container">
			<div class="row">
				<div class="col-md-12">
					<div class="donation-box">
						<div class="needed-amount">
							<span><i><del>N</del></i> <i><del>N</del></i> <i><del>N</del></i> <i><del>N</del></i> <i><del>N</del></i> <i><del>N</del></i></span>
							<!-- <span><i><del>N</del></i> <i>6</i><i>5</i><i>2</i><i>4</i></span> -->
							<i>NEEDED DONATION</i>
						</div>	
						<div class="col-md-6">
							<h3>SOW A SEED</h3>
							<p>Be a part.</p>
						</div>
						<div class="col-md-6">
							<a data-toggle="modal" data-target="#myModal" title="">DONATE NOW</a>
						</div>	
						<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-hidden="true">
							<div class="donation-popup">
								<div class="popup-title">
									<h5>Make A Donation</h5>
									<p>The generous man will be prosperous, and he who waters will himself be watered</p>
									<div class="needed-amount">
										<span><i><del>N</del></i> <i><del>N</del></i> <i><del>N</del></i> <i><del>N</del></i> <i><del>N</del></i> <i><del>N</del></i></span>
										<!-- <span><i><del>N</del></i> <i>6</i><i>5</i><i>2</i><i>4</i></span> -->
										<i>NEEDED DONATION</i>
									</div>
								</div>
								<div class="popup-content">
									<div class="collected">
										<div class="percentage">
											<img src="images/logo.png" />
										</div>
										<div class="collected-amount">
											<span>Give and it shall be given unto you</span>
										</div>
									</div>

									<div class="amount-selection">
										<p><span style="color: #000;">I would like to make a donation in the amount of:</span></p>
										<form action="" method="post">
											<div class="row"> 
												<div class="col-sm-3"><label><input  onclick="document.getElementById('textfield').value='5000';" type="radio" value="5000" name="choice1" /><del>N</del> 5,000</label></div>
												<div class="col-sm-3"><label><input  onclick="document.getElementById('textfield').value='10000';" type="radio" value="10000" name="choice1" /><del>N</del> 10,000</label></div>
												<div class="col-sm-3"><label><input onclick="document.getElementById('textfield').value='20000';" type="radio" value="20000" name="choice1" /><del>N</del> 20,000</label></div>
												<div class="col-sm-3"><label><input onclick="document.getElementById('textfield').value='50000';" type="radio" value="50000" name="choice1" /><del>N</del> 50,000</label></div>

												<div class="selections">
													<div class="col-md-6">
														<input type="text" id="textfield" placeholder="ENTER YOUR AMOUNT PLEASE" style="color: #000;" required="required">
													</div>
													<div class="col-md-6">
														<select id="donate" name="donate">
															<option value="state1">Once</option>
															<option value="state2">Weekly</option>
															<option value="state3">Monthly</option>
															<option value="state4">Quaterly</option>
															<option value="state5">Half Yearly</option>
															<option value="state6">Yearly</option>
														</select>
														{{ csrf_field() }}
													</div>
												</div>
											</div>
											<!-- <p><label><input type="radio" name="choice2" />I agree to the <a href="#" title="">Term of Services</a> and <a href="#" title="">Privacy Policy</a>:</label></p> -->
											<input class="button btn btn-primary submit" type="submit" value="Donate Now" style="color: #f9f9f9;" />
										</form>
										<!-- 
										<ul>
											<li><a href="#" title="">Sign In</a></li>
											<li><a href="#" title="">Sign Up</a></li>
										</ul> -->
									</div>

								</div>
							</div>
						</div>	

					</div>
				</div>
			</div>
		</div>
	</div>
</section>

<!-- 
<section>
	<div class="block blackish">
	<div class="parallax" style="background:url(images/parallax3.jpg);"></div>
		<div class="container">
			<div class="row">
				<div class="col-md-12 column">
					<div class="pastors-carousel">
						<div class="pastors-message">
							<div class="row">
								<div class="col-md-3">
									<img src="images/resource/pastor1.jpg" alt="" />
								</div>
								<div class="col-md-9">
									<h4>GARY HARREL</h4>
									<span>DIRECTOR-BASELINE</span>
									<p>Ut ac eleifend mauris, ac porta lacus. Sed pharetra magna massa, sit amet condimentum risus congue ut. Duis eu neque convallis, auctor lacus sed, tincidunt arcu. Aliquam vitae hendrerit dolor. Ut ac eleifend mauris, ac porta lacus. Sed pharetra magna massa, sit amet condimentum risus congue ut. Ut ac eleifend mauris, ac porta lacus. Sed pharetra magna massa, sit amet condimentum risus congue ut. Duis eu neque convallis, auctor lacus sed, tincidunt arcu. </p>
									<ul class="sermon-media">
										<li><a href="http://vimeo.com/44867610" data-rel="prettyPhoto" title=""><i class="fa fa-film"></i></a></li>
										<li><a title=""><i class="audio-btn fa fa-headphones"></i>
											<div class="audioplayer"><audio src="sermon.mp3"></audio><span class="cross">X</span></div>
										</a></li>
										<li><a target="_blank" href="test.doc" title=""><i class="fa fa-download"></i></a></li>
										<li><a target="_blank" href="test.pdf" title=""><i class="fa fa-book"></i></a></li>
									</ul>
								</div>					
							</div>
						</div>
						<div class="pastors-message">
							<div class="row">
								<div class="col-md-3">
									<img src="images/resource/pastor2.jpg" alt="" />
								</div>
								<div class="col-md-9">
									<h4>GARY HARREL</h4>
									<span>DIRECTOR-BASELINE</span>
									<p>Ut ac eleifend mauris, ac porta lacus. Sed pharetra magna massa, sit amet condimentum risus congue ut. Duis eu neque convallis, auctor lacus sed, tincidunt arcu. Aliquam vitae hendrerit dolor. Ut ac eleifend mauris, ac porta lacus. Sed pharetra magna massa, sit amet condimentum risus congue ut. Ut ac eleifend mauris, ac porta lacus. Sed pharetra magna massa, sit amet condimentum risus congue ut. Duis eu neque convallis, auctor lacus sed, tincidunt arcu. </p>
									<ul class="sermon-media">
										<li><a href="http://vimeo.com/44867610" data-rel="prettyPhoto" title=""><i class="fa fa-film"></i></a></li>
										<li><a title=""><i class="audio-btn fa fa-headphones"></i>
											<div class="audioplayer"><audio src="sermon.mp3"></audio><span class="cross">X</span></div>
										</a></li>
										<li><a target="_blank" href="test.doc" title=""><i class="fa fa-download"></i></a></li>
										<li><a target="_blank" href="test.pdf" title=""><i class="fa fa-book"></i></a></li>
									</ul>
								</div>					
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>

<section>
	<div class="block blackish">
	<div class="parallax" style="background:url(images/parallax1.jpg);"></div>
		<div class="container">
			<div class="row">	
				<div class="col-md-12">
					<div class="parallax-title">
						<h3 class="special-text">REQUEST <span>A PRAYER</span></h3>
						<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Integer nec odio. Praesent libero <br/> Kindly start with your name then the request (e.g Mr Jade Barx - Pray for ....)</p>
						<div id="message"></div>
					</div>
					<form class="prayer-request" action="" method="post" id="prayerrequest">
						<input type="text" name="prayer_request" id="prayer_request" placeholder="Type your requests and let's agree together in prayer." required>
						{{-- <input type="hidden" id="ct" name="{{ csrf-token }}" value="{{ csrf_token() }}" /> --}}
						{{ csrf_field() }}
						<input type="hidden" id="verify" name="verify" value="27" />
						<input type="submit" value="Request Prayers" id="submit" />
					</form>
				</div>
			</div>
		</div>
	</div>
</section>
 -->
<section>
	<div class="block">
		<div class="container">
			<div class="row">
				<div class="title2">
					<!-- <span>Pellent Esque Tellus</span> -->
					<h2>OUR <span>UPCOMING EVENTS</span></h2>
				</div>

				<div class="col-md-12 column">
					<div class="counter-events">
						<div class="row">
						<div class="event-carousel">
						<div class="event-count">
							<div class="event-img">
								<img src="images/resource/event1.jpg" alt="" />
								<div class="downcount">
									<i class="fa fa-clock-o"></i>
									<ul class="countdown1">
										<li> <span class="days">00</span>
										<p class="days_ref">DAYS</p>
										</li>
										<li> <span class="hours">00</span>
										<p class="hours_ref">HOURS</p>
										</li>
										<li> <span class="minutes">00</span>
										<p class="minutes_ref">MINTS</p>
										</li>
										<li> <span class="seconds">00</span>
										<p class="seconds_ref">SECS</p>
										</li>
									</ul>
								</div>
							</div>
							<h4><a href="event-details" title="">Crazy for Jesus II</a></h4>
						</div>
						<div class="event-count">
							<div class="event-img">
								<img src="images/resource/event2.jpg" alt="" />
								<div class="downcount">
									<i class="fa fa-clock-o"></i>
									<ul class="countdown2">
										<li> <span class="days">00</span>
										<p class="days_ref">DAYS</p>
										</li>
										<li> <span class="hours">00</span>
										<p class="hours_ref">HOURS</p>
										</li>
										<li> <span class="minutes">00</span>
										<p class="minutes_ref">MINTS</p>
										</li>
										<li> <span class="seconds">00</span>
										<p class="seconds_ref">SECS</p>
										</li>
									</ul>
								</div>
							</div>
							<h4><a href="event-details" title="">Teens Got Talent</a></h4>
						</div>
						<div class="event-count">
							<div class="event-img">
								<img src="images/resource/event3.jpg" alt="" />
								<div class="downcount">
									<i class="fa fa-clock-o"></i>
									<ul class="countdown3">
										<li> <span class="days">00</span>
										<p class="days_ref">DAYS</p>
										</li>
										<li> <span class="hours">00</span>
										<p class="hours_ref">HOURS</p>
										</li>
										<li> <span class="minutes">00</span>
										<p class="minutes_ref">MINTS</p>
										</li>
										<li> <span class="seconds">00</span>
										<p class="seconds_ref">SECS</p>
										</li>
									</ul>
								</div>
							</div>
							<h4><a href="event-details" title="">Youth's Roadshow</a></h4>
						</div>
						<div class="event-count">
							<div class="event-img">
								<img src="images/resource/event4.jpg" alt="" />
								<div class="downcount">
									<i class="fa fa-clock-o"></i>
									<ul class="countdown4">
										<li> <span class="days">00</span>
										<p class="days_ref">DAYS</p>
										</li>
										<li> <span class="hours">00</span>
										<p class="hours_ref">HOURS</p>
										</li>
										<li> <span class="minutes">00</span>
										<p class="minutes_ref">MINTS</p>
										</li>
										<li> <span class="seconds">00</span>
										<p class="seconds_ref">SECS</p>
										</li>
									</ul>
								</div>
							</div>
							<h4><a href="event-details" title="">Sister's Annual Conference</a></h4>
						</div>
						<div class="event-count">
							<div class="event-img">
								<img src="images/resource/event5.jpg" alt="" />
								<div class="downcount">
									<i class="fa fa-clock-o"></i>
									<ul class="countdown5">
										<li> <span class="days">00</span>
										<p class="days_ref">DAYS</p>
										</li>
										<li> <span class="hours">00</span>
										<p class="hours_ref">HOURS</p>
										</li>
										<li> <span class="minutes">00</span>
										<p class="minutes_ref">MINTS</p>
										</li>
										<li> <span class="seconds">00</span>
										<p class="seconds_ref">SECS</p>
										</li>
									</ul>
								</div>
							</div>
							<h4><a href="event-details" title="">Kid's Summer CodeCamp</a></h4>
						</div>
						</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>


<section>
	<div class="block">
		<div class="container">
			<div class="row">
				<div class="title2">
					<!-- <span>Pellent Esque Tellus</span> -->
					<h2>WATCH <span>HOLY GHOST SERVICE</span> LIVE</h2> <!-- set event_title here -->
				</div>

				<div class="col-md-12 column">
					<div class="counter-events">
						<div class="row">
							<!-- 
								embed_id = 1503053823 
								// id="ls_embed_" + embed_id

								accountId = 204531
								eventId = 7648867
								width = 640; height = 360 // default for now ; no need to set

								// src="https://livestream.com/accounts/" + accountId + "/events/" + eventId + "/player?width=640&height=360&enableInfoAndActivity=true&defaultDrawer=&autoPlay=true&mute=false 
							-->
							<iframe id="ls_embed_1503053823" src="https://livestream.com/accounts/204531/events/7648867/player?width=640&height=360&enableInfoAndActivity=true&defaultDrawer=&autoPlay=true&mute=false" width="640" height="360" frameborder="0" scrolling="no" allowfullscreen> </iframe> <!-- 2017 Convention Holy Ghost Service -->
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>


<section>
	<div class="block gray">
		<div class="container">
			<div class="row">
				<div class="title3">
					<h2>OUR RECENT <span>BLOG</span></h2>
					<!-- <p>Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis <br/> dis parturient montes, nascetur ridiculus mus.</p> -->
				</div>

				<div class="col-md-12 column">
					<div class="row">
						<div class="masonary-blog">
							<div class="col-md-4">
								<div class="simple-blog">
									<div class="image">
										<img src="images/resource/gallery-large-3.jpg" alt="" />
										<a data-rel="prettyPhoto[blog1]" href="images/resource/gallery-large-3.jpg" title=""><i class="fa fa-link"></i></a>
									</div>
									<h4><a href="blog-details" title="">Dating is dead.</a></h4>
									<p>So says the media. Girls, stop expecting guys to make any formal attempt at winning your affections. Don't sit around waiting ....</p>
									<span>
										<a class="blog-date" href="blog-details" title=""><i class="fa fa-calendar-o"></i> JULY 3, 2017</a>
										<a class="blog-more" href="blog-details" title="">READ MORE <i class="fa fa-long-arrow-right"></i></a>
									</span>
								</div>
							</div>
							<div class="col-md-4">
								<div class="simple-blog">
									<div class="image">
										<img src="images/resource/gallery-large-3.jpg" alt="" />
										<a data-rel="prettyPhoto[blog2]" href="images/resource/gallery-large-3.jpg" title=""><i class="fa fa-link"></i></a>
									</div>
									<h4><a href="blog-details" title="">I Did Not Marry My One True Love</a></h4>
									<p>Remember that scene at the end of Jerry Maguire? Where Jerry tells Dorothy that she completes him, and she tearfully tells him to shut up, ....</p>
									<span>
										<a class="blog-date" href="blog-details" title=""><i class="fa fa-calendar-o"></i> JULY 3, 2017</a>
										<a class="blog-more" href="blog-details" title="">READ MORE <i class="fa fa-long-arrow-right"></i></a>
									</span>
								</div>
							</div>
							<div class="col-md-4">
								<div class="simple-blog">
									<div class="image">
										<img src="images/resource/gallery-large-3.jpg" alt="" />
										<a data-rel="prettyPhoto[blog3]" href="images/resource/gallery-large-3.jpg" title=""><i class="fa fa-link"></i></a>
									</div>
									<h4><a href="blog-details" title="">Is Betting Sin?</a></h4>
									<p>I recently won a bet of N100,000, am about paying my tithe but a thought came into my mind that I might be given God a sinful money, ....</p>
									<span>
										<a class="blog-date" href="blog-details" title=""><i class="fa fa-calendar-o"></i> JULY 3, 2017</a>
										<a class="blog-more" href="blog-details" title="">READ MORE <i class="fa fa-long-arrow-right"></i></a>
									</span>
								</div>
							</div>
						</div>
					</div>
					<a class="button3" href="blog" title="">SEE MORE BLOG POSTS</a>
				</div>
			</div>
		</div>
	</div>
</section>

<!--
<section>
	<div class="block">
		<div class="container">
			<div class="row">
				<div class="title3">
					<h2>JOIN OUR <span>SMS LIST</span></h2>
					<p>Separate multiple phone numbers with comma (e.g 08012345678,08001234567).</p>
				</div>
				<div class="col-md-12 column">
					<div id="message2"></div>
					<form class="newsletter-signup" action="" method="post" id="sms_newsletter">
						<input type="text" name="fullname_sms" id="fullname_sms" placeholder="Enter Your Full Name" required style="width: 30%;">
						<input type="text" name="phone_sms" id="phone_sms" placeholder="Enter Your Phone Number" required style="width: 30%;">
						<input type="text" name="captcha1" id="captcha1" placeholder="3 + 5 = " required style="width: 10%;">
						{{ csrf_field() }}
						<input type="submit" value="SUBMIT" id="submit2" style="width: 30%; padding-left: 10px; ">
					</form>

					<div class="social-corner">
						<p>Think people should hear about this?</p>
						<ul class="social-media">
							<li><a href="#" title=""><i class="fa fa-twitter"></i></a></li>
							<li><a href="#" title=""><i class="fa fa-facebook"></i></a></li>
						</ul>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>
<section>
	<div class="block remove-gap">
		<div class="container">
			<div class="row">
				<div class="title2">
					<span>Pellent Esque Tellus</span>
					<h2>OUR <span>GALLERY</span></h2>
				</div>
				<div class="col-md-12 column">
					<div class="remove-ext">
						<div class="row">
							<div class="mas-gallery">
								<div class="col-md-3">
									<div class="gallery">
										<img src="images/resource/gallery-thumb3.jpg" alt="" /> 
										<div class="gallery-title">
											<i class="fa fa-picture-o"></i>
											<h3>Christ Church Picture</h3>
										</div>
										<ul>
											<li><a data-rel="prettyPhoto[gallery1]" href="images/resource/gallery-large-3.jpg" title=""><i class="fa fa-search"></i></a></li>
										</ul>
									</div>
								</div>

								<div class="col-md-3">
									<div class="gallery">
										<img src="images/resource/gallery-thumb3.jpg" alt="" /> 
										<div class="gallery-title">
											<i class="fa fa-picture-o"></i>
											<h3>Edward III’s tomb, Westminster Abbey</h3>
										</div>
										<ul>
											<li><a data-rel="prettyPhoto[gallery2]" href="images/resource/gallery-large-3.jpg" title=""><i class="fa fa-search"></i></a></li>
										</ul>
									</div>
								</div>

								<div class="col-md-3">
									<div class="gallery">
										<img src="images/resource/gallery-thumb3.jpg" alt="" /> 
										<div class="gallery-title">
											<i class="fa fa-picture-o"></i>
											<h3>Church Latest Event Picture</h3>
										</div>
										<ul>
											<li><a data-rel="prettyPhoto[gallery3]" href="images/resource/gallery-large-3.jpg" title=""><i class="fa fa-search"></i></a></li>
										</ul>
									</div>
								</div>

								<div class="col-md-3">
									<div class="gallery">
										<img src="images/resource/gallery-thumb3.jpg" alt="" /> 
										<div class="gallery-title">
											<i class="fa fa-picture-o"></i>
											<h3>Bath Abbey	</h3>
										</div>
										<ul>
											<li><a data-rel="prettyPhoto[gallery4]" href="images/resource/gallery-large-3.jpg" title=""><i class="fa fa-search"></i></a></li>
										</ul>
									</div>
								</div>

							</div>
						</div>
					</div>

				</div>
			</div>
		</div>
	</div>
</section>
-->
@stop
@section('after_footer')

	<!-- SCRIPTS-->
	<script type="text/javascript" src="js/modernizr.custom.17475.js"></script>

	<script src="js/jquery.1.10.2.js" type="text/javascript"></script>
    <script src="js/bootstrap.min.js"></script>
	<script src="js/jquery.prettyPhoto.js" type="text/javascript"></script>
    <script src="js/owl.carousel.min.js"></script>
	<script src="js/mediaelement-and-player.min.js"></script>
    <script src="js/jquery.knob.js"></script>
    <script src="js/knob-script.js"></script>
    <script src="js/script.js"></script>
    {{-- <!-- <script src="js/styleswitcher.js"></script> --> --}}

	<script type="text/javascript" src="js/jquery.downCount.js"></script> 
	<script class="source" type="text/javascript">
		$('.countdown1').downCount({
		    date: '06/25/2022 12:00:00', // m/d/Y H:i:s
		    offset: +10
		});
		$('.countdown2').downCount({
		    date: '12/12/2018 12:00:00',
		    offset: +10
		});
		$('.countdown3').downCount({
		    date: '12/07/2018 12:00:00',
		    offset: +10
		});
		$('.countdown4').downCount({
		    date: '09/10/2018 12:00:00',
		    offset: +10
		});
		$('.countdown5').downCount({
		    date: '09/11/2018 12:00:00',
		    offset: +10
		});
	</script> 

	<script>
    $(document).ready(function() {
		$(".event-carousel").owlCarousel({
			autoPlay: false,
			rewindSpeed : 3000,
			slideSpeed:2000,
			items : 3,
			itemsDesktop : [1199,3],
			itemsDesktopSmall : [979,2],
			itemsTablet : [768,2],
			itemsMobile : [479,1],
			navigation : false,
		}); /*** PRODUCTS CAROUSEL ***/
		
		$(".pastors-carousel").owlCarousel({
			autoPlay: 5000,
			slideSpeed:1000,
			singleItem : true,
			transitionStyle : "goDown",		
			navigation : false
		}); /*** CAROUSEL ***/

	});	

	$('audio,video').mediaelementplayer();
	</script>
	
	<!-- SLIDER REVOLUTION -->
	<script type="text/javascript" src="js/revolution/jquery.themepunch.plugins.min.js"></script>
	<script type="text/javascript" src="js/revolution/jquery.themepunch.revolution.min.js"></script>
	<script type="text/javascript">
		var revapi;
		revapi = jQuery('.tp-banner').revolution(
			{
				delay:15000,
				startheight:570,
				startwidth:1100,
				autoHeight:"off",
				navigationType:"none",
				hideThumbs:10,
				fullWidth:"on",
				fullScreen:"off",
				fullScreenOffsetContainer: ""
			});

	</script>
	<script type="text/javascript">
	jQuery(document).ready(function() {
	function IsPhone(phone)
	{
		if(phone == "" || phone == null)
		{
			return true;
		} 
		else
		{
			var patt = /^[0-9,]+$/;
			return patt.test(phone);
		}
	}
	$("#submit2").click(function(){
		var phone = $("#phone_sms").val();
		if(IsPhone(phone) == false){ 
			alert("Invalid Phone !!!");
			$("#phone_sms").focus();
			return false;
		}
  });    
});
</script>
@stop