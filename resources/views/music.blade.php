@extends('base')
@section('title')Music @parent @stop
@section('meta')
@parent
<meta name="description" content="The Redeemed Christian Church of God Chapel of Life - A place of worship where Jesus is enthroned, the undiluted Word of God is preached, the presence of God dwells. Join us for any of our services and experience the love and power of God in all areas of your life. Jesus loves you." />
<meta name="keywords" content="chapel of life, rccg, church, redeemed, christ, love, faith, peace, christian faith, religion, festac, lagos, amuwo odofin, jesus, mile two, nigeria" />
@stop

@section('styles')
@parent

<link rel="stylesheet" href="css/mediaelementplayer.min.css" />
<link rel="stylesheet" type="text/css" href="css/colors/blue.css" title="color1" />
@stop

@section('content')
<div class="page-top">
	<div class="parallax" style="background:url(images/parallax1.jpg);"></div>	
	<div class="container"> 
		<h1>MUSIC <span>LIST</span></h1>
		<ul>
			<li><a href="./" title="">Home</a></li>
			<li><a href="#">Media Center</a></li>
			<li><a href="#" title="">Music</a></li>
		</ul>
	</div>
</div><!--- PAGE TOP -->

<section>
	<div class="block">
		<div class="container">
			<div class="row">
				<div class="col-md-8 column">
					<div class="latest-sermons remove-ext">
						<div class="sermon">
							<div class="row">
								<div class="col-md-12">
									<iframe width="100%" height="150px" scrolling="no" frameborder="no" src="https://w.soundcloud.com/player/?url=https%3A//api.soundcloud.com/tracks/338448041&amp;color=ff5500&amp;auto_play=false&amp;hide_related=false&amp;show_comments=true&amp;show_user=true&amp;show_reposts=false&amp;visual=true"></iframe>
								</div>
							</div>
						</div>
						
						<div class="sermon">
							<div class="row">
								<div class="col-md-12">
									<iframe width="100%" height="150px" scrolling="no" frameborder="no" src="https://w.soundcloud.com/player/?url=https%3A//api.soundcloud.com/tracks/338448041&amp;color=ff5500&amp;auto_play=false&amp;hide_related=false&amp;show_comments=true&amp;show_user=true&amp;show_reposts=false&amp;visual=true"></iframe>
								</div>
							</div>
						</div>
						
						<div class="sermon">
							<div class="row">
								<div class="col-md-12">
									<iframe width="100%" height="150px" scrolling="no" frameborder="no" src="https://w.soundcloud.com/player/?url=https%3A//api.soundcloud.com/tracks/338448041&amp;color=ff5500&amp;auto_play=false&amp;hide_related=false&amp;show_comments=true&amp;show_user=true&amp;show_reposts=false&amp;visual=true"></iframe>
								</div>
							</div>
						</div>
						
						<div class="sermon">
							<div class="row">
								<div class="col-md-12">
									<iframe width="100%" height="150px" scrolling="no" frameborder="no" src="https://w.soundcloud.com/player/?url=https%3A//api.soundcloud.com/tracks/338448041&amp;color=ff5500&amp;auto_play=false&amp;hide_related=false&amp;show_comments=true&amp;show_user=true&amp;show_reposts=false&amp;visual=true"></iframe>
								</div>
							</div>
						</div>
						
						<div class="sermon">
							<div class="row">
								<div class="col-md-12">
									<iframe width="100%" height="150px" scrolling="no" frameborder="no" src="https://w.soundcloud.com/player/?url=https%3A//api.soundcloud.com/tracks/338448041&amp;color=ff5500&amp;auto_play=false&amp;hide_related=false&amp;show_comments=true&amp;show_user=true&amp;show_reposts=false&amp;visual=true"></iframe>
								</div>
							</div>
						</div>
						
						<div class="sermon">
							<div class="row">
								<div class="col-md-12">
									<iframe width="100%" height="150px" scrolling="no" frameborder="no" src="https://w.soundcloud.com/player/?url=https%3A//api.soundcloud.com/tracks/338448041&amp;color=ff5500&amp;auto_play=false&amp;hide_related=false&amp;show_comments=true&amp;show_user=true&amp;show_reposts=false&amp;visual=true"></iframe>
								</div>
							</div>
						</div>

					</div><!-- LATEST SERMONS -->

						<div class="theme-pagination">
							<ul class="pagination">
								<li><a href="#"><i class="fa fa-angle-left"></i></a></li>
								<li><a href="#">1</a></li>
								<li><a href="#">2</a></li>
								<li><a href="#">3</a></li>
								<li><a href="#">4</a></li>
								<li><a href="#">5</a></li>
								<li><a href="#"><i class="fa fa-angle-right"></i></a></li>
							</ul>
						</div><!-- PAGINATION -->
					
				</div>

				<aside class="col-md-4 sidebar column">
					<div class="widget">
						<div class="widget-title"><h4>ABOUT US</h4></div>
						<div class="footer-logo">
							<img src="images/logo.png" alt="" />
						</div>
						<p>Suspendisse velit ante, aliquet vel adipi cing auctor, tincidunt a diam orem ipsum.</p>
						<div class="contact">
							<ul>
								<li><i class="fa fa-home"></i>Address : 242 NTB Street, NY, US</li>
								<li><i class="fa fa-envelope"></i>Email: youremail@yourdomain.com</li>
								<li><i class="fa fa-phone"></i>Telephone: +1555 1235</li>
							</ul>
						</div><!-- CONTACT INFO -->
					</div><!-- ABOUT WIDGET -->

					<div class="widget">
						<div class="widget-title"><h4>ARCHIVES</h4></div>
						<ul>
							<li><a href="blog.html" title=""><i class="fa fa-hand-o-right"></i>April 2014 <span>(07)</span></a></li>
							<li><a href="blog.html" title=""><i class="fa fa-hand-o-right"></i>May 2014 <span>(12)</span></a></li>
							<li><a href="blog.html" title=""><i class="fa fa-hand-o-right"></i>January 2014 <span>(54)</span></a></li>
							<li><a href="blog.html" title=""><i class="fa fa-hand-o-right"></i>July 2013 <span>(85)</span></a></li>
							<li><a href="blog.html" title=""><i class="fa fa-hand-o-right"></i>April 2013 <span>(42)</span></a></li>
						</ul>
					</div><!-- ARCHIVES -->

					<div class="widget">
						<div class="widget-title"><h4>OUR FLICKR</h4></div>
						<div class="gallery-widget">
							<div class="col-md-3"><a href="images/resource/flickr1.jpg" data-rel="prettyPhoto"><img src="images/resource/flickr1.jpg" alt="" /></a></div>
							<div class="col-md-3"><a href="images/resource/flickr2.jpg" data-rel="prettyPhoto"><img src="images/resource/flickr2.jpg" alt="" /></a></div>
							<div class="col-md-3"><a href="images/resource/flickr3.jpg" data-rel="prettyPhoto"><img src="images/resource/flickr3.jpg" alt="" /></a></div>
							<div class="col-md-3"><a href="images/resource/flickr4.jpg" data-rel="prettyPhoto"><img src="images/resource/flickr4.jpg" alt="" /></a></div>
							<div class="col-md-3"><a href="images/resource/flickr5.jpg" data-rel="prettyPhoto"><img src="images/resource/flickr5.jpg" alt="" /></a></div>
							<div class="col-md-3"><a href="images/resource/flickr6.jpg" data-rel="prettyPhoto"><img src="images/resource/flickr6.jpg" alt="" /></a></div>
							<div class="col-md-3"><a href="images/resource/flickr1.jpg" data-rel="prettyPhoto"><img src="images/resource/flickr1.jpg" alt="" /></a></div>
							<div class="col-md-3"><a href="images/resource/flickr2.jpg" data-rel="prettyPhoto"><img src="images/resource/flickr2.jpg" alt="" /></a></div>
						</div>
					</div><!-- GALLERY WIDGET -->

					<div class="widget">
						<div class="widget-title"><h4>NEWSLETTER SIGNUP</h4></div>
						<form>
							<input type="email" placeholder="Enter Your Email Address" />
							<input type="submit" value="SIGN UP NOW" />
						</form>
						<p>Suspendisse velit ante, aliquet vel adipi cing auctor, tincidunt a diam. Lorem ipsum dolor sit .</p>				
					</div><!-- NEWSLETTER SIGNUP -->
				</aside><!-- SIDEBAR -->
				
			</div>
		</div>
	</div>
</section>	
@stop
@section('after_footer')
	<!-- SCRIPTS-->
	<script type="text/javascript" src="js/modernizr.custom.17475.js"></script>

	<script src="js/jquery.1.10.2.js" type="text/javascript"></script>
    <script src="js/bootstrap.min.js"></script>
	<script src="js/jquery.prettyPhoto.js" type="text/javascript"></script>
	<script src="js/mediaelement-and-player.min.js"></script>
    <script src="js/script.js"></script>
    <script src="js/styleswitcher.js"></script>
	<script type="text/javascript" src="js/jquery.downCount.js"></script> 


    <script type="text/javascript">
		$(window).load(function(){
				$('audio,video').mediaelementplayer();
			
		})

    </script>
@stop