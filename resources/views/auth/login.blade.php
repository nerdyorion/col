<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <base href="{{ URL::to('/') }}">
  <link rel="icon" type="image/png" sizes="16x16" href="{{ asset('images/favicon.png') }}">
  <title>Login | {{ config('app.name') }}</title>
  <!-- Bootstrap CSS -->
  <link href="{{ asset('admin/assets/bower_components/bootstrap/dist/css/bootstrap.min.css') }}" rel="stylesheet">
  <link href="{{ asset('admin/assets/css/login.css') }}" rel="stylesheet" type="text/css" />
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.6.3/css/font-awesome.min.css" />
  <!-- HTML5 Shiv and Respond.js IE8 support -->
  <!--[if lt IE 9]>
  <script src="assets/https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
  <script src="assets/https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
  <![endif]-->
</head>
<!-- <body style="background: url(assets/images/login-bg.jpg) no-repeat center center fixed;"> -->
<body style="background: url({{ asset('admin/assets/images/login-bg.png') }}) repeat;">
  <section id="wrapper" class="login-register">
    <div class="container">

      <div class="row" id="pwd-container">
        <div class="col-md-4"></div>

        <div class="col-md-4">
          <section class="login-form" id="login-form">
            <form action="{{ route('login') }}" role="login" method="post" ?>
              {{ csrf_field() }}
              <div class="text-center"><img src="{{ asset('images/logo.png') }}" /></div>
              <input type="email" name="email" placeholder="Email" maxlength="255" required="required" class="form-control input-lg" value="{{ old('email') }}" autofocus="autofocus" />

              <input type="password" class="form-control input-lg" id="password" name="password" placeholder="Password" required="required" />

              <div class="pull-left">
                <label style="cursor: pointer;">
                  <input type="checkbox" name="remember" {{ old('remember') ? 'checked' : '' }}> Remember Me
                </label>
              </div>

              @if ($errors->has('email'))
              <!-- <span class="help-block"> -->
              <div id="error-msg" class="alert alert-danger alert-dismissible" role="alert" style="clear: both;">
                <strong>{{ $errors->first('email') }}</strong>
              </div>
              <!-- </span> -->
              @endif      

              <button type="submit" name="submit" class="btn btn-lg btn-primary btn-block">Sign in</button>
              <div>
                <a class="pull-left" href="{{ URL::to('/') }}" style="color: #6699CC">&larr; Website</a>
                <a class="pull-right" href="{{ route('password.reset') }}" style="color: #6699CC"><i class="fa fa-lock"></i> Forgot Password</a>
              </div>

            </form>
          </section>

          <div class="form-links">
            Powered by <a href="http://neegles.com" target="_blank">Neegles</a>
          </div>
        </div>

        <div class="col-md-4"></div>


      </div>

    </div>
  </section>
  <!-- jQuery -->
  <script src="{{ asset('admin/assets/bower_components/jquery/dist/jquery.min.js') }}"></script>
  <!-- jQuery UI -->
  <script src="{{ asset('admin/assets/js/jquery-ui.min.js') }}"></script>
  <!-- Bootstrap Core JavaScript -->
  <script src="{{ asset('admin/assets/bower_components/bootstrap/dist/js/bootstrap.min.js') }}"></script>
  <script type="text/javascript">
    $(document).ready(function() {
      $("#error-msg").show().animate({opacity: 1.0}, 5000).fadeOut(1000);
      $("#success-msg").show().animate({opacity: 1.0}, 5000).fadeOut(1000);
    });
  </script>
</body>

</html>
