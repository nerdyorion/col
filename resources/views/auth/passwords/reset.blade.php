<!DOCTYPE html>
<html>

<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <base href="{{ URL::to('/') }}">
  <link rel="icon" type="image/png" sizes="16x16" href="{{ asset('images/favicon.png') }}">
  <title>Reset Password | {{ config('app.name') }}</title>
  <!-- Bootstrap CSS -->
  <link href="{{ asset('admin/assets/bower_components/bootstrap/dist/css/bootstrap.min.css') }}" rel="stylesheet">
  <link href="{{ asset('admin/assets/css/login.css') }}" rel="stylesheet" type="text/css" />
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.6.3/css/font-awesome.min.css" />
  <!-- HTML5 Shiv and Respond.js IE8 support -->
  <!--[if lt IE 9]>
  <script src="assets/https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
  <script src="assets/https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
  <![endif]-->
</head>
<body style="background: url({{ asset('admin/assets/images/login-bg.png') }}) repeat;">
  <section id="wrapper" class="login-register">
    <div class="container">

      <div class="row" id="pwd-container">
        <div class="col-md-4"></div>

        <div class="col-md-4">
          <section class="login-form" id="forgot-password-form">
            <form action="{{ route('password.reset') }}" role="login" method="post" ?>
              {{ csrf_field() }}
              <div class="text-center"><img src="{{ asset('images/logo.png') }}" /></div>

              <input type="hidden" name="token" value="{{ $token }}">

              <input type="email" name="email" placeholder="Email" maxlength="255" required="required" class="form-control input-lg" value="{{ $email or old('email') }}" autofocus="autofocus" /> 

              <input type="password" class="form-control input-lg" id="password" name="password" placeholder="Password" required="required" />

              <input type="password" class="form-control input-lg" id="password-confirm" name="password_confirmation" placeholder="Confirm Password" required="required" />

              @if ($errors->has('email'))
              <div id="error-msg" class="alert alert-danger alert-dismissible" role="alert" style="clear: both;">
                  <strong>{{ $errors->first('email') }}</strong>
              </div>
              @endif

              @if ($errors->has('password'))
              <div id="error-msg" class="alert alert-danger alert-dismissible" role="alert" style="clear: both;">
                  <strong>{{ $errors->first('password') }}</strong>
              </div>
              @endif

              @if ($errors->has('password_confirmation'))
              <div id="error-msg" class="alert alert-danger alert-dismissible" role="alert" style="clear: both;">
                  <strong>{{ $errors->first('password_confirmation') }}</strong>
              </div>
              @endif

              @if (session('status'))
              <div id="success-msg" class="alert alert-success alert-dismissible">
                {{ session('status') }}
                </div>
                @endif

            <button type="submit" name="submit" class="btn btn-lg btn-primary btn-block">Reset Password</button>
            <div>
                <a class="pull-left" href="{{ URL::to('/') }}" style="color: #6699CC">&larr; Website</a>
                <a class="pull-right" href="{{ route('login') }}" style="color: #6699CC" id="link-si"><i class="fa fa-sign-in"></i> Sign in</a>
            </div>

        </form>
    </section> 

    <div class="form-links">
        Powered by <a href="http://neegles.com" target="_blank">Neegles</a>
    </div>
</div>

<div class="col-md-4"></div>


</div>

</div>
</section>
<!-- jQuery -->
<script src="{{ asset('admin/assets/bower_components/jquery/dist/jquery.min.js') }}"></script>
<!-- jQuery UI -->
<script src="{{ asset('admin/assets/js/jquery-ui.min.js') }}"></script>
<!-- Bootstrap Core JavaScript -->
<script src="{{ asset('admin/assets/bower_components/bootstrap/dist/js/bootstrap.min.js') }}"></script>
<script type="text/javascript">
    $(document).ready(function() {
      $("#error-msg").show().animate({opacity: 1.0}, 5000).fadeOut(1000);
      $("#success-msg").show().animate({opacity: 1.0}, 5000).fadeOut(1000);
  });
</script>
</body>

</html>