@extends('base')
@section('title')Good Women Ministry @parent @stop
@section('meta')
@parent
<meta name="description" content="The Redeemed Christian Church of God Chapel of Life - A place of worship where Jesus is enthroned, the undiluted Word of God is preached, the presence of God dwells. Join us for any of our services and experience the love and power of God in all areas of your life. Jesus loves you." />
<meta name="keywords" content="chapel of life, rccg, church, redeemed, christ, love, faith, peace, christian faith, religion, festac, lagos, amuwo odofin, jesus, mile two, nigeria" />
@stop
@section('styles')
@parent

<link rel="stylesheet" href="css/mediaelementplayer.min.css" />
<link rel="stylesheet" type="text/css" href="css/colors/blue.css" title="color1" />
@stop

@section('content')
<div class="page-top">
	<div class="parallax" style="background:url(images/parallax1.jpg);"></div>	
	<div class="container"> 
		<h1>GOOD WOMEN</h1>
		<ul>
			<li><a href="./" title="">Home</a></li>
			<li><a href="#">Ministries</a></li>
			<li><a href="#" title="">Good Women</a></li>
		</ul>
	</div>
</div><!--- PAGE TOP -->

<section>
	<div class="block">
		<div class="container">
			<div class="row">
				<div class="col-md-8 column">
					<div class="single-page">
						<img src="images/resource/sermon-single.jpg" alt="" />
						<h2>CHILDREN MINISTRY</h2>
						<div class="meta">
							<ul>
								<li><i class="fa fa-reply"></i> Posted In <a href="#" title="">Sermons</a></li>
								<li><i class="fa fa-calendar-o"></i> November 01, 2013</li>
								<li><i class="fa fa-user"></i> <a href="#" title="">PAUL LAZARIUS</a></li>
							</ul>
							<img src="images/resource/author.jpg" alt="" />
						</div><!-- POST META -->


						<div class="event-info">
							<div class="col-md-6">
								<div class="map">
									<iframe src="https://www.google.com/maps?f=q&amp;source=s_q&amp;hl=en&amp;geocode=&amp;q=uk&amp;aq=&amp;sll=18.312811,-4.306641&amp;sspn=46.292419,86.572266&amp;ie=UTF8&amp;hq=&amp;hnear=United+Kingdom&amp;ll=52.352119,-2.647705&amp;spn=0.685471,1.352692&amp;t=p&amp;z=10&amp;output=embed"></iframe>	
								</div><!--- GOOGLE MAP -->

							</div>
							<div class="col-md-6">
								<ul>
									<li><i class="fa fa-map-marker"></i> South Lanarkshire, UK</li>
									<li><i class="fa fa-map-marker"></i> 20th Ave S. Federal Way, WA 214214 </li>
									<li><i class="fa fa-clock-o"></i> 7:00pm - 8:30pm  ( MON - WED )</li>
									<li><i class="fa fa-clock-o"></i> 11:44 pm – 11:00 am  ( THU - SAT )</li>

								</ul>
							</div>
						</div>
					</div><!-- SERMON SINGLE -->
					
					<p>Aenean leo vene quam. Pellntes ique ornare sem eius modte venenatis vestibum. Cras mattis itugir purus. Aenean le vene quam. Pellntes ique ornare seeim eiusmodte venenatis vestibum. Cras mattis citur exquisitely fari then far purus. Aenean leo vene quam. Pellntes ique ornare sem eiusmodte venen. Et tollit utamur nam, dcum ullumo etiam velit. Ne scripserit. Sea ex utamur phaedrum, nisl no, no reque sensibus duo. Meini coposae, paulo mediocrem etiam negleg enur. Vis ut argum entum lorem ipsum dolor sit amet, consectetur adipscing elit. Nulla convallis egestas rhoncus. Don eofacilisis fermentum sem, ac viverra ante lucus vel. Donec vel maurs quam. Lorem ipsum dolor sit amet, consect etur adpiscing elit. Nulla convallis egestas rhoncus. Donec facilisis ferme ntum sem, ac viverra ante luctus vel. Donec vel maus quam.Lorem ipsum dolor sit amet, consectetur dipiscing elit. Nulla convallis egestas rhoncus. </p>

					<a class="flwpug_getpaid" data-PBFPubKey="FLWPUBK-038467def5060dbf738445f5ccecced9-X" data-txref="rave-checkout-1503338840" data-amount="1500" data-customer_email="tayo@hollatags.com" data-currency = "NGN" data-pay_button_text = "Purchase Now" data-country="NG" data-custom_title = "Neegles SMS" data-custom_description = "Swift SMS Delivery" data-redirect_url = "" data-custom_logo = "" data-payment_method = "both" data-exclude_banks=""></a>	

					<script type="text/javascript" src="https://api.ravepay.co/flwv3-pug/getpaidx/api/flwpbf-inline.js"></script>

				</div>

				<aside class="col-md-4 sidebar column">
					<div class="widget">
						<div class="widget-title"><h4>Events Calendar</h4></div>
						<iframe src="https://calendar.google.com/calendar/embed?mode=AGENDA&amp;showCalendars=0&amp;showTitle=0&amp;showDate=0&amp;showPrint=0&amp;showTz=0&amp;height=400&amp;wkst=1&amp;bgcolor=%23FFFFFF&amp;src=9fkdkvdkkcj070qq06nuq2qqk4%40group.calendar.google.com&ctz=Africa/Lagos" style="border: 0" width="100%" height="400px" frameborder="0" scrolling="no"></iframe>
					</div><!-- Calendar -->

					<div class="widget">
						<div class="widget-title"><h4>OUR FLICKR</h4></div>
						<div class="gallery-widget">
							<div class="col-md-3"><a href="images/resource/flickr1.jpg" data-rel="prettyPhoto"><img src="images/resource/flickr1.jpg" alt="" /></a></div>
							<div class="col-md-3"><a href="images/resource/flickr2.jpg" data-rel="prettyPhoto"><img src="images/resource/flickr2.jpg" alt="" /></a></div>
							<div class="col-md-3"><a href="images/resource/flickr3.jpg" data-rel="prettyPhoto"><img src="images/resource/flickr3.jpg" alt="" /></a></div>
							<div class="col-md-3"><a href="images/resource/flickr4.jpg" data-rel="prettyPhoto"><img src="images/resource/flickr4.jpg" alt="" /></a></div>
							<div class="col-md-3"><a href="images/resource/flickr5.jpg" data-rel="prettyPhoto"><img src="images/resource/flickr5.jpg" alt="" /></a></div>
							<div class="col-md-3"><a href="images/resource/flickr6.jpg" data-rel="prettyPhoto"><img src="images/resource/flickr6.jpg" alt="" /></a></div>
							<div class="col-md-3"><a href="images/resource/flickr1.jpg" data-rel="prettyPhoto"><img src="images/resource/flickr1.jpg" alt="" /></a></div>
							<div class="col-md-3"><a href="images/resource/flickr2.jpg" data-rel="prettyPhoto"><img src="images/resource/flickr2.jpg" alt="" /></a></div>
						</div>
					</div><!-- GALLERY WIDGET -->

					<div class="widget">
						<div class="widget-title"><h4>NEWSLETTER SIGNUP</h4></div>
						<form>
							<input type="email" placeholder="Enter Your Email Address" />
							<input type="submit" value="SIGN UP NOW" />
						</form>
						<p>Suspendisse velit ante, aliquet vel adipi cing auctor, tincidunt a diam. Lorem ipsum dolor sit .</p>				
					</div><!-- NEWSLETTER SIGNUP -->
				</aside><!-- SIDEBAR -->
				
			</div>
		</div>
	</div>
</section>	
@stop
@section('after_footer')
	<!-- SCRIPTS-->
	<script type="text/javascript" src="js/modernizr.custom.17475.js"></script>

	<script src="js/jquery.1.10.2.js" type="text/javascript"></script>
    <script src="js/bootstrap.min.js"></script>
	<script src="js/jquery.prettyPhoto.js" type="text/javascript"></script>
    <script src="js/script.js"></script>
    <script src="js/styleswitcher.js"></script>
	<script type="text/javascript" src="js/jquery.downCount.js"></script> 
@stop