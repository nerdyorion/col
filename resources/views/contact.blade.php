@extends('base')
@section('title')Contact Us @parent @stop
@section('meta')
@parent
<meta name="description" content="The Redeemed Christian Church of God Chapel of Life - A place of worship where Jesus is enthroned, the undiluted Word of God is preached, the presence of God dwells. Join us for any of our services and experience the love and power of God in all areas of your life. Jesus loves you." />
<meta name="keywords" content="chapel of life, rccg, church, redeemed, christ, love, faith, peace, christian faith, religion, festac, lagos, amuwo odofin, jesus, mile two, nigeria" />
@stop
@section('styles')
@parent

<link rel="stylesheet" type="text/css" href="css/colors/blue.css" title="color1" />
@stop

@section('content')
<div class="page-top">
	<div class="parallax" style="background:url(images/parallax1.jpg);"></div>	
	<div class="container"> 
		<h1>CONTACT <span>US</span></h1>
		<ul>
			<li><a href="./" title="">Home</a></li>
			<li><a href="#" title="">Get In Touch</a></li>
			<li><a href="#">Contact Us</a></li>
		</ul>
	</div>
</div><!--- PAGE TOP -->

<section>
	<div class="block">
		<div class="container">
			<div class="row">
				<div class="col-md-12">
					<div class="map">
							<iframe src="https://www.google.com/maps/embed?pb=!1m14!1m8!1m3!1d15857.795899939321!2d3.3057415!3d6.464666!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x0%3A0x6e431da4b512334b!2sRCCG+Chapel+of+Life!5e0!3m2!1sen!2sng!4v1498760679622" width="600" height="450" frameborder="0" style="border:0" allowfullscreen></iframe>
					</div><!--- GOOGLE MAP -->
				</div>
			</div>
		</div>
	</div>
</section>


<section>
	<div class="block remove-gap">
		<div class="container">
			<div class="row">
				<div class="col-md-12">
					<div class="title2">
						<span>Send a mail</span>
						<h2>WE'D LOVE TO <span>HEAR FROM YOU</span></h2>
					</div>

					<div class="row">
						<div class="col-md-6 column">
							<h4>CONTACT INFORMATION</h4>
							<div class="space"></div>
							<p>Geoffrey rush four seasons in one day, the hawks etihad stadium movida formula onegrand rix ball, chopper read the espy victory vs heart lygon street spruikers cumulus inc, citylink spiegeltent bill clinton ate two bowls the emerald peacock collingwood ferals, Rod aver dumplings dandenong.</p>
							<div class="space"></div>
							<p>The emerald peacock empire of the sun, etihad stadium movida swanston spiegeltent fr	on bogans, dandenong neatly trimmed moustaches hu tong dumplings rooftop bars chapel street, east brunswick club mamasita the G’ kylie minogue trams.</p>
							<div class="space"></div>
							<ul class="social-media">
								<li><a href="#" title=""><i class="fa fa-twitter"></i></a></li>
								<li><a href="#" title=""><i class="fa fa-facebook"></i></a></li>
							</ul>
						</div><!--- CONTACT INFORMATION -->
						<div class="col-md-6 column">
							<h4>FILL IN THE FORM BELOW</h4>
							<div class="space"></div>
							<div id="message">@include('templates.admin.partials.alerts')</div>
							<form class="theme-form" method="post" action="" name="contactform" id="contactform">
								<input name="full_name" class="half-field form-control" type="text" id="name"  placeholder="Name" maxlength="255" required />
								<input name="email" class="half-field form-control" type="email" id="email" placeholder="Email" maxlength="255" required />
								<input name="phone" class="half-field form-control" type="text" id="phone" placeholder="Phone" maxlength="255" />
								<input name="subject" class="half-field form-control" type="text" id="subject" placeholder="Subject" maxlength="2000" required />
								<textarea name="message" class="form-control" id="comments" placeholder="Message" required ></textarea>
								<input name="verify" class="half-field form-control" type="text" id="verify" placeholder="5 + 22 = " required />
								{{ csrf_field() }}
								<input class="submit" type="submit"  id="submit" value="SUBMIT" />
							</form><!--- FORM -->
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>	

<section>
	<div class="block remove-gap">
		<div class="container">
			<div class="row">
				<div class="col-md-12">
					<div class="contact-info">
						<div class="col-md-3">
							<div class="info-block">
								<i class="fa fa-home"></i>
								<p>Plot 7, Crystal Estate 3<sup>rd</sup> Gate, Durbar Road.</p>
							</div>
						</div>
						<div class="col-md-3">
							<div class="info-block">
								<i class="fa fa-info"></i>
								<p>www.rccgchapeloflife.org</p>
							</div>
						</div>
						<div class="col-md-3">
							<div class="info-block">
								<i class="fa fa-envelope-o"></i>
								<p><a href="mailto:info@rccgchapeloflife.org">info@rccgchapeloflife.org</a></p>
							</div>
						</div>
						<div class="col-md-3">
							<div class="info-block">
								<i class="fa fa-mobile"></i>
								<p><a href="tel:+123 456 7890">(000) +123 456 7890</a></p>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</section><!--- CONTACT INFORMATION -->
@stop
@section('after_footer')
	<!-- SCRIPTS-->
	<script type="text/javascript" src="js/modernizr.custom.17475.js"></script>

	<script src="js/jquery.1.10.2.js" type="text/javascript"></script>
    <script src="js/bootstrap.min.js"></script>
	<script src="js/jquery.prettyPhoto.js" type="text/javascript"></script>
    <script src="js/script.js"></script>
    <script src="js/styleswitcher.js"></script>
	<script type="text/javascript" src="js/jquery.downCount.js"></script> 
<script type="text/javascript">
jQuery(document).ready(function() {   
function IsPhone(phone)
{
	if(phone == "" || phone == null){ return true; } else {
	var patt = /^[0-9+]+$/;
	return patt.test(phone);}
}
 
$("#submit").click(function(){
	var phone = $("#phone").val();
	if(IsPhone(phone) == false){ 
		alert("Invalid Phone !!!");
		$("#phone").focus();
		return false;
	}
  });    
});
</script>
@stop