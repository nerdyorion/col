@extends('base')
@section('title')About Us @parent @stop
@section('meta')
@parent
<meta name="description" content="The Redeemed Christian Church of God Chapel of Life - A place of worship where Jesus is enthroned, the undiluted Word of God is preached, the presence of God dwells. Join us for any of our services and experience the love and power of God in all areas of your life. Jesus loves you." />
<meta name="keywords" content="chapel of life, rccg, church, redeemed, christ, love, faith, peace, christian faith, religion, festac, lagos, amuwo odofin, jesus, mile two, nigeria" />
@stop
@section('styles')
@parent

<link rel="stylesheet" href="css/mediaelementplayer.min.css" />
<link rel="stylesheet" type="text/css" href="css/colors/blue.css" title="color1" />
@stop

@section('content')
<div class="page-top">
	<div class="parallax" style="background:url(images/parallax1.jpg);"></div>	
	<div class="container"> 
		<h1>ABOUT <span>US</span></h1>
		<ul>
			<li><a href="./" title="">Home</a></li>
			<li><a href="#">The Church</a></li>
			<li><a href="#">About Us</a></li>
		</ul>
	</div>
</div>

<section>
	<div class="block">
		<div class="container">
			<div class="row">
				<div class="col-md-6 column">
					<div class="simple-text">
						<h3>WHO WE ARE AND WHAT IS OUR MISSION?</h3>
						<p>Lorem ipsum dolor sit amet, consectetur adipiscing elt. Aioielit lorem, in lacinia libero lacina ac. Pellente sque vulputat eusmod, nulla dolor placerat eiest, et adipiscing urn aom. Aioielit lorem, in lacinia libero lacina ac. Pellente sque lacinia libero lacina ac. Pellente sque vulputa teuismod, nulla dolor placerat eiest, et adipiscing urna metuis. Moli viourem sit amet, Lorem ipsum dolor sit amet, elt. <a href="#" title="">Aioielit lorem</a>, in lacinia libero lacina ac. Pellente sque vulput ate uismod.</p>
						<a class="button" href="#" title="">DONATE NOW</a>
					</div>
				</div>
				<div class="col-md-6 column">
					<div class="video">
						<div class="video-img">
							<img src="images/resource/video.jpg" alt="" />
							<a href="http://vimeo.com/44867610"  data-rel="prettyPhoto" title=""><i class="fa fa-play"></i></a>
						</div>
					</div><!-- VIDEO -->
				</div>
			</div>
		</div>
	</div>
</section>


<section>
	<div class="block blackish">
	<div class="parallax" style="background:url(images/parallax3.jpg);"></div>
		<div class="container">
			<div class="row">
				<div class="col-md-12 column">
					<div class="pastors-carousel">
						<div class="pastors-message">
							<div class="row">
								<div class="col-md-3">
									<img src="images/resource/pastor1.jpg" alt="" />
								</div>
								<div class="col-md-9">
									<h4>GARY HARREL</h4>
									<span>DIRECTOR-BASELINE</span>
									<p>Ut ac eleifend mauris, ac porta lacus. Sed pharetra magna massa, sit amet condimentum risus congue ut. Duis eu neque convallis, auctor lacus sed, tincidunt arcu. Aliquam vitae hendrerit dolor. Ut ac eleifend mauris, ac porta lacus. Sed pharetra magna massa, sit amet condimentum risus congue ut. Ut ac eleifend mauris, ac porta lacus. Sed pharetra magna massa, sit amet condimentum risus congue ut. Duis eu neque convallis, auctor lacus sed, tincidunt arcu. </p>
									<ul class="sermon-media">
										<li><a href="http://vimeo.com/44867610" data-rel="prettyPhoto" title=""><i class="fa fa-film"></i></a></li>
										<li><a title=""><i class="audio-btn fa fa-headphones"></i>
											<div class="audioplayer"><audio src="sermon.mp3"></audio><span class="cross">X</span></div>
										</a></li>
										<li><a target="_blank" href="test.doc" title=""><i class="fa fa-download"></i></a></li>
										<li><a target="_blank" href="test.pdf" title=""><i class="fa fa-book"></i></a></li>
									</ul>
								</div>					
							</div>
						</div>
						<div class="pastors-message">
							<div class="row">
								<div class="col-md-3">
									<img src="images/resource/pastor2.jpg" alt="" />
								</div>
								<div class="col-md-9">
									<h4>GARY HARREL</h4>
									<span>DIRECTOR-BASELINE</span>
									<p>Ut ac eleifend mauris, ac porta lacus. Sed pharetra magna massa, sit amet condimentum risus congue ut. Duis eu neque convallis, auctor lacus sed, tincidunt arcu. Aliquam vitae hendrerit dolor. Ut ac eleifend mauris, ac porta lacus. Sed pharetra magna massa, sit amet condimentum risus congue ut. Ut ac eleifend mauris, ac porta lacus. Sed pharetra magna massa, sit amet condimentum risus congue ut. Duis eu neque convallis, auctor lacus sed, tincidunt arcu. </p>
									<ul class="sermon-media">
										<li><a href="http://vimeo.com/44867610" data-rel="prettyPhoto" title=""><i class="fa fa-film"></i></a></li>
										<li><a title=""><i class="audio-btn fa fa-headphones"></i>
											<div class="audioplayer"><audio src="sermon.mp3"></audio><span class="cross">X</span></div>
										</a></li>
										<li><a target="_blank" href="test.doc" title=""><i class="fa fa-download"></i></a></li>
										<li><a target="_blank" href="test.pdf" title=""><i class="fa fa-book"></i></a></li>
									</ul>
								</div>					
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>

<section>
	<div class="block">
		<div class="container">
			<div class="row">
				<div class="col-md-12">
					<div class="title">
						<span>Pellent Esque Tellus</span>
						<h2>MEET <span>OUR STAFF</span></h2>
					</div>

					<div class="row">
						<div class="team-carousel">
							<div class="member">
								<div class="team">
									<div class="team-img">
										<img src="images/resource/1.jpg" alt="" />
									</div>
									<div class="member-detail">
										<h3><a href="team-single.html" title="">BOB SMITH</a></h3>
										<span>SENIOR PASTOR</span>
										<p>Lorem ipsum dolor sit a onsec tetiri adien tashn commodo leg tashn dol tashin tyons piscin.</p>
									</div>
								</div>
							</div><!-- MEMBER -->
							<div class="member">
								<div class="team">
									<div class="team-img">
										<img src="images/resource/1.jpg" alt="" />
									</div>
									<div class="member-detail">
										<h3><a href="team-single.html" title="">LEOMA KITN</a></h3>
										<span>SENIOR PASTOR</span>
										<p>Lorem ipsum dolor sit a onsec tetiri adien tashn commodo leg tashn dol tashin tyons piscin.</p>
									</div>
								</div>
							</div><!-- MEMBER -->
							<div class="member">
								<div class="team">
									<div class="team-img">
										<img src="images/resource/1.jpg" alt="" />
									</div>
									<div class="member-detail">
										<h3><a href="team-single.html" title="">RIFF KALE</a></h3>
										<span>SENIOR PASTOR</span>
										<p>Lorem ipsum dolor sit a onsec tetiri adien tashn commodo leg tashn dol tashin tyons piscin.</p>
									</div>
								</div>
							</div><!-- MEMBER -->
							<div class="member">
								<div class="team">
									<div class="team-img">
										<img src="images/resource/1.jpg" alt="" />
									</div>
									<div class="member-detail">
										<h3><a href="team-single.html" title="">TOM FOBE</a></h3>
										<span>SENIOR PASTOR</span>
										<p>Lorem ipsum dolor sit a onsec tetiri adien tashn commodo leg tashn dol tashin tyons piscin.</p>
									</div>
								</div>
							</div><!-- MEMBER -->
							<div class="member">
								<div class="team">
									<div class="team-img">
										<img src="images/resource/1.jpg" alt="" />
									</div>
									<div class="member-detail">
										<h3><a href="team-single.html" title="">SEIMN LINEO</a></h3>
										<span>SENIOR PASTOR</span>
										<p>Lorem ipsum dolor sit a onsec tetiri adien tashn commodo leg tashn dol tashin tyons piscin.</p>
									</div>
								</div>
							</div><!-- MEMBER -->
							<div class="member">
								<div class="team">
									<div class="team-img">
										<img src="images/resource/1.jpg" alt="" />
									</div>
									<div class="member-detail">
										<h3><a href="team-single.html" title="">KOJEO SLORM</a></h3>
										<span>SENIOR PASTOR</span>
										<p>Lorem ipsum dolor sit a onsec tetiri adien tashn commodo leg tashn dol tashin tyons piscin.</p>
									</div>
								</div>
							</div><!-- MEMBER -->
						</div><!-- TEAM CAROUSEL -->
					</div>						
				</div>
			</div>
		</div>
	</div>
</section>
@stop
@section('after_footer')
	<!-- SCRIPTS-->
	<script type="text/javascript" src="js/modernizr.custom.17475.js"></script>

	<script src="js/jquery.1.10.2.js" type="text/javascript"></script>
    <script src="js/bootstrap.min.js"></script>
	<script src="js/jquery.prettyPhoto.js" type="text/javascript"></script>
    <script src="js/owl.carousel.min.js"></script>
	<script src="js/mediaelement-and-player.min.js"></script>
    <script src="js/script.js"></script>
    <script src="js/styleswitcher.js"></script>
	<script type="text/javascript" src="js/jquery.downCount.js"></script> 

    <script type="text/javascript">
    $(document).ready(function() {
		$(".pastors-carousel").owlCarousel({
			autoPlay: 5000,
			slideSpeed:1000,
			singleItem : true,
			transitionStyle : "fadeUp",		
			navigation : false
		}); /*** PASTORS MESSAGE CAROUSEL ***/

		$(".team-carousel").owlCarousel({
			autoPlay: 8000,
			rewindSpeed : 3000,
			slideSpeed:2000,
			items : 4,
			itemsDesktop : [1199,3],
			itemsDesktopSmall : [979,2],
			itemsTablet : [768,2],
			itemsMobile : [479,1],
			navigation : false,
		}); /*** TEAM CAROUSEL ***/

	});    

				$('audio,video').mediaelementplayer();

    </script>
@stop