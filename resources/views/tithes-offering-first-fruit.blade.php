@extends('base')
@section('title')Pay Your Tithes, Offering, First Fruits @parent @stop
@section('meta')
@parent
<meta name="description" content="The Redeemed Christian Church of God Chapel of Life - A place of worship where Jesus is enthroned, the undiluted Word of God is preached, the presence of God dwells. Join us for any of our services and experience the love and power of God in all areas of your life. Jesus loves you." />
<meta name="keywords" content="chapel of life, rccg, church, redeemed, christ, love, faith, peace, christian faith, religion, festac, lagos, amuwo odofin, jesus, mile two, nigeria" />
@stop
@section('styles')
@parent

<link rel="stylesheet" type="text/css" href="css/colors/blue.css" title="color1" />
@stop

@section('content')
<div class="page-top">
	<div class="parallax" style="background:url(images/parallax1.jpg);"></div>	
	<div class="container"> 
		<h1>Tithes, Offering, First Fruits</h1>
		<!-- <h1>Church Projects <span>BLOG</span></h1> -->
		<ul>
			<li><a href="./" title="">Home</a></li>
			<li><a href="#">Online Giving</a></li>
			<li><a href="#">Tithes / Offering / First-Fruit</a></li>
		</ul>
	</div>
</div>

<section>
	<div class="block">
		<div class="container">
			<div class="row">
				<div class="col-md-8 column">
					<div class="remove-ext">
						<div class="blog-post">
							<div class="row">
								<div class="col-md-5">
									<div class="image">
										<img src="images/resource/gallery-large-3.jpg" alt="" />
										<a href="blog-single.html" title=""><i class="fa fa-link"></i></a>
									</div>
								</div>
								<div class="col-md-7">
									<div class="blog-detail">
										<h3><a href="blog-single.html" title="">Tithe</a></h3>
										<p>Aenen leo vene quam. Pellntes quie venenatis vestib citur onecs.Suspendisse velit ante, aliquet vel adipi cing auctor, tincidunt a diam. Lorem ipsum dolor sit .</p>
										<span><i class="fa fa-calendar-o"></i> November 01, 2013</span>
									</div>
								</div>
							</div>
						</div><!-- BLOG POST -->

						<div class="blog-post">
							<div class="row">
								<div class="col-md-5">
									<div class="image">
										<img src="images/resource/gallery-large-3.jpg" alt="" />
										<a href="blog-single.html" title=""><i class="fa fa-link"></i></a>
									</div>
								</div>
								<div class="col-md-7">
									<div class="blog-detail">
										<h3><a href="blog-single.html" title="">Offering</a></h3>
										<p>Aenen leo vene quam. Pellntes quie venenatis vestib citur onecs.Suspendisse velit ante, aliquet vel adipi cing auctor, tincidunt a diam. Lorem ipsum dolor sit .</p>
										<span><i class="fa fa-calendar-o"></i> November 01, 2013</span>
									</div>
								</div>
							</div>
						</div><!-- BLOG POST -->
						<div class="blog-post">
							<div class="row">
								<div class="col-md-5">
									<div class="image">
										<img src="images/resource/gallery-large-3.jpg" alt="" />
										<a href="blog-single.html" title=""><i class="fa fa-link"></i></a>
									</div>
								</div>
								<div class="col-md-7">
									<div class="blog-detail">
										<h3><a href="blog-single.html" title="">First Fruit</a></h3>
										<p>Aenen leo vene quam. Pellntes quie venenatis vestib citur onecs.Suspendisse velit ante, aliquet vel adipi cing auctor, tincidunt a diam. Lorem ipsum dolor sit .</p>
										<span><i class="fa fa-calendar-o"></i> November 01, 2013</span>
									</div>
								</div>
							</div>
						</div><!-- BLOG POST -->
					</div>
				</div>

				<aside class="col-md-4 sidebar column">
					<div class="widget">
						<form class="search-form" action="" method="get">
							<input type="text" placeholder="START SEARCHING" name="q" />
							<input type="submit" value="" />
						</form>
					</div><!-- SEARCH FORM -->

					<div class="widget">
						<div class="widget-title"><h4>TAG CLOUD</h4></div>
						<div class="tagclouds">
							<a href="blog.html" title="">Aenen</a>
							<a href="blog.html" title="">Suspendise</a>
							<a href="blog.html" title="">Citrous</a>
							<a href="blog.html" title="">Valitsantego</a>
							<a href="blog.html" title="">Pellntesious</a>
							<a href="blog.html" title="">Vestibu</a>
							<a href="blog.html" title="">Aenen</a>
							<a href="blog.html" title="">Suspendise</a>
							<a href="blog.html" title="">Citrous</a>
						</div>
					</div><!-- TAG CLOUD -->

					<div class="widget">
						<div class="widget-title"><h4>RECENT BLOG</h4></div>
						<div class="remove-ext">
							<div class="widget-blog">
								<div class="widget-blog-img"><img src="images/resource/widget-blog.jpg" alt="" /></div>
								<p><a href="blog-single.html" title="">Suspendisse velit anteg, aliquet vel adiping.</a></p>
								<span><i class="fa fa-calendar-o"></i> November 01, 2013</span>
							</div><!-- WIDGET BLOG -->

							<div class="widget-blog">
								<div class="widget-blog-img"><img src="images/resource/widget-blog2.jpg" alt="" /></div>
								<p><a href="blog-single.html" title="">Suspendisse velit anteg, aliquet vel adiping.</a></p>
								<span><i class="fa fa-calendar-o"></i> November 01, 2013</span>
							</div><!-- WIDGET BLOG -->
						</div>						
					</div><!-- RECENT BLOG -->

					<div class="widget">
						<div class="widget-title"><h4>VIDEO</h4></div>
						<div class="video">
							<div class="video-img">
								<img src="images/resource/video.jpg" alt="" />
								<a href="http://vimeo.com/44867610"  data-rel="prettyPhoto" title=""><i class="fa fa-play"></i></a>
							</div>
						</div>
					</div><!-- VIDEO -->


					<!--<div class="widget">
						<div class="widget-title"><h4>META</h4></div>
						<ul>
							<li><a href="blog.html" title=""><i class="fa fa-hand-o-right"></i>Log in</a></li>
							<li><a href="blog.html" title=""><i class="fa fa-hand-o-right"></i>Entries RSS</a></li>
							<li><a href="blog.html" title=""><i class="fa fa-hand-o-right"></i>Comments RSS</a></li>
							<li><a href="blog.html" title=""><i class="fa fa-hand-o-right"></i>Wordpress.org</a></li>
						</ul>
					</div> META -->
				</aside><!-- SIDEBAR -->
				
			</div>
		</div>
	</div>
</section>
@stop
@section('after_footer')
	<!-- SCRIPTS-->
	<script type="text/javascript" src="js/modernizr.custom.17475.js"></script>

	<script src="js/jquery.1.10.2.js" type="text/javascript"></script>
    <script src="js/bootstrap.min.js"></script>
	<script src="js/jquery.prettyPhoto.js" type="text/javascript"></script>
    <script src="js/script.js"></script>
    <script src="js/styleswitcher.js"></script>
	<script type="text/javascript" src="js/jquery.downCount.js"></script> 
@stop