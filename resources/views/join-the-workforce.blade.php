@extends('base')
@section('title')Join The Workforce @parent @stop
@section('meta')
@parent
<meta name="description" content="The Redeemed Christian Church of God Chapel of Life - A place of worship where Jesus is enthroned, the undiluted Word of God is preached, the presence of God dwells. Join us for any of our services and experience the love and power of God in all areas of your life. Jesus loves you." />
<meta name="keywords" content="chapel of life, rccg, church, redeemed, christ, love, faith, peace, christian faith, religion, festac, lagos, amuwo odofin, jesus, mile two, nigeria" />
@stop
@section('styles')
@parent

<link rel="stylesheet" type="text/css" href="css/colors/blue.css" title="color1" />
@stop

@section('content')
<div class="page-top">
	<div class="parallax" style="background:url(images/parallax1.jpg);"></div>	
	<div class="container"> 
		<h1>Join The Workforce</h1>
		<ul>
			<li><a href="./" title="">Home</a></li>
			<li><a href="#" title="">Get In Touch</a></li>
			<li><a href="#">Join The Workforce</a></li>
		</ul>
	</div>
</div><!--- PAGE TOP -->

<section>
	<div class="block">
		<div class="container">
			<div class="row">
				<div class="col-md-12">
					<div class="row">
						<div class="col-md-8 col-md-offset-2 column">
							<h4>FILL IN THE FORM BELOW</h4>
							<div class="space"></div>
							<div id="message"></div>
							<form class="theme-form" method="post" action="" name="contactform" id="contactform">
								<input name="name" class="half-field form-control" type="text" id="name"  placeholder="Name" required />
								<input name="email" class="half-field form-control" type="email" id="email" placeholder="Email" required />
								<input name="phone" class="half-field form-control" type="text" id="phone" placeholder="Phone" required />
								<input name="department" class="half-field form-control" type="text" id="department" placeholder="Department" required />
								<textarea name="reason" class="form-control" id="reason" placeholder="Reason" required ></textarea>
								<input name="verify" class="half-field form-control" type="text" id="verify" placeholder="5 + 22 = " required />
								{{ csrf_field() }}
								<input class="submit" type="submit"  id="submit" value="SUBMIT" />
							</form><!--- FORM -->
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>	

<section>
	<div class="block remove-gap">
		<div class="container">
			<div class="row">
				<div class="col-md-12">
					<div class="contact-info">
						<div class="col-md-3">
							<div class="info-block">
								<i class="fa fa-home"></i>
								<p>Plot 7, Crystal Estate 3<sup>rd</sup> Gate, Durbar Road.</p>
							</div>
						</div>
						<div class="col-md-3">
							<div class="info-block">
								<i class="fa fa-info"></i>
								<p>www.rccgchapeloflife.org</p>
							</div>
						</div>
						<div class="col-md-3">
							<div class="info-block">
								<i class="fa fa-envelope-o"></i>
								<p><a href="mailto:info@rccgchapeloflife.org">info@rccgchapeloflife.org</a></p>
							</div>
						</div>
						<div class="col-md-3">
							<div class="info-block">
								<i class="fa fa-mobile"></i>
								<p><a href="tel:+123 456 7890">(000) +123 456 7890</a></p>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</section><!--- CONTACT INFORMATION -->
@stop
@section('after_footer')
	<!-- SCRIPTS-->
	<script type="text/javascript" src="js/modernizr.custom.17475.js"></script>

	<script src="js/jquery.1.10.2.js" type="text/javascript"></script>
    <script src="js/bootstrap.min.js"></script>
	<script src="js/jquery.prettyPhoto.js" type="text/javascript"></script>
    <script src="js/script.js"></script>
    <script src="js/styleswitcher.js"></script>
	<script type="text/javascript" src="js/jquery.downCount.js"></script> 
<script type="text/javascript">
jQuery(document).ready(function() {   
function IsPhone(phone)
{
	if(phone == "" || phone == null){ return true; } else {
	var patt = /^[0-9+]+$/;
	return patt.test(phone);}
}
 
$("#submit").click(function(){
	var phone = $("#phone").val();
	if(IsPhone(phone) == false){ 
		alert("Invalid Phone !!!");
		$("#phone").focus();
		return false;
	}
  });    
});
</script>
@stop