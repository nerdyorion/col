@extends('base')

@section('title')Blog Title @parent @stop
@section('meta')
@parent
<meta name="description" content="The Redeemed Christian Church of God Chapel of Life - A place of worship where Jesus is enthroned, the undiluted Word of God is preached, the presence of God dwells. Join us for any of our services and experience the love and power of God in all areas of your life. Jesus loves you." />
<meta name="keywords" content="chapel of life, rccg, church, redeemed, christ, love, faith, peace, christian faith, religion, festac, lagos, amuwo odofin, jesus, mile two, nigeria" />
@stop
@section('styles')
@parent

<link rel="stylesheet" href="css/mediaelementplayer.min.css" />
<link rel="stylesheet" type="text/css" href="css/colors/blue.css" title="color1" />
@stop

@section('content')
<div class="page-top">
	<div class="parallax" style="background:url(images/parallax1.jpg);"></div>	
	<div class="container"> 
		<h1>BLOG<span> TITLE</span></h1>
		<ul>
			<li><a href="./" title="">Home</a></li>
			<li><a href="blog" title="">Blog</a></li>
			<li><a href="#" title="">Blog Title</a></li>
		</ul>
	</div>
</div><!--- PAGE TOP -->

<section>
	<div class="block">
		<div class="container">
			<div class="row">
				<div class="col-md-8 column">
					<div class="single-page">
						<img src="images/resource/sermon-single.jpg" alt="" />
						<h2>Francis Chan - Passion 2013 - God Is Faithful</h2>
						<div class="meta">
							<ul>
								<li><i class="fa fa-reply"></i> Posted In <a href="#" title="">Sermons</a></li>
								<li><i class="fa fa-calendar-o"></i> November 01, 2013</li>
								<li><i class="fa fa-user"></i> <a href="#" title="">PAUL LAZARIUS</a></li>
							</ul>
							<img src="images/resource/author.jpg" alt="" />
						</div><!-- POST META -->
					</div><!-- SERMON SINGLE -->
					
					<p>Aenean leo vene quam. Pellntes ique ornare sem eius modte venenatis vestibum. Cras mattis itugir purus. Aenean le vene quam. Pellntes ique ornare seeim eiusmodte venenatis vestibum. Cras mattis citur exquisitely fari then far purus. Aenean leo vene quam. Pellntes ique ornare sem eiusmodte venen. Et tollit utamur nam, dcum ullumo etiam velit. Ne scripserit. Sea ex utamur phaedrum, nisl no, no reque sensibus duo. Meini coposae, paulo mediocrem etiam negleg enur. Vis ut argum entum lorem ipsum dolor sit amet, consectetur adipscing elit. Nulla convallis egestas rhoncus. Don eofacilisis fermentum sem, ac viverra ante lucus vel. Donec vel maurs quam. Lorem ipsum dolor sit amet, consect etur adpiscing elit. Nulla convallis egestas rhoncus. Donec facilisis ferme ntum sem, ac viverra ante luctus vel. Donec vel maus quam.Lorem ipsum dolor sit amet, consectetur dipiscing elit. Nulla convallis egestas rhoncus. </p>

					<blockquote>
					<div class="parallax" style="background:url(images/parallax2.jpg);"></div>
					<i class="fa fa-quote-left"></i>Voluptate illum dolore ita ipsum, quid deserunt singulis, labore admodum ita multos malis ea nam nam tamen fore amet.<i class="fa fa-quote-right"></i></blockquote><!-- BLOCKQUOTE -->

					<p>Aenean leo vene quam. Pellntes ique ornare sem eius modte venenatis vestibum. Cras mattis itugir purus. Aenean le vene quam. Pellntes ique ornare seeim eiusmodte venenatis vestibum. Cras mattis citur exquisitely fari then far purus. Aenean leo vene quam. Pellntes ique ornare sem eiusmodte venen. Et tollit utamur nam, dcum ullumo etiam velit. Ne scripserit. Sea ex utamur phaedrum, nisl no, no reque sensibus duo. Meini coposae, paulo mediocrem etiam negleg enur. Vis ut argum entum lorem ipsum dolor sit amet, consectetur adipscing elit. Nulla convallis egestas rhoncus. Don eofacilisis fermentum sem, ac viverra ante lucus vel. Donec vel maurs quam. Lorem ipsum dolor sit amet, consect etur adpiscing elit. Nulla convallis egestas rhoncus. Donec facilisis ferme ntum sem, ac viverra ante luctus vel. Donec vel maus quam.Lorem ipsum dolor sit amet, consectetur dipiscing elit. Nulla convallis egestas rhoncus. </p>

					<div class="pastor-info">
						<img src="images/resource/pastor-info.jpg" alt="" />
						<h4>PAUL LAZARIUS <span>PASTOR</span></h4>
						<p>Aenean leo vene quam. Pellntes ique ornare sem eiusmodte venenatis vestibum. Cras mattis citur purus. Cras mattis citugir purus. Aenean levene quam. Pellntes ique ornae seeim eiusmodte venenatis vestibum...</p>
					</div><!-- PASTOR INFO -->

					<div class="share-this">
						<h5><i class="fa fa-share"></i> SHARE THIS SERMON</h5>
						<ul class="social-media">
							<li><a href="#" title=""><i class="fa fa-linkedin"></i></a></li>
							<li><a href="#" title=""><i class="fa fa-google-plus"></i></a></li>
							<li><a href="#" title=""><i class="fa fa-twitter"></i></a></li>
							<li><a href="#" title=""><i class="fa fa-facebook"></i></a></li>
						</ul>				
					</div><!-- SHARE THIS -->
					<!-- 
					<div class="comments">
						<div id="disqus_thread"></div>
						<script>
							/**
							*  RECOMMENDED CONFIGURATION VARIABLES: EDIT AND UNCOMMENT THE SECTION BELOW TO INSERT DYNAMIC VALUES FROM YOUR PLATFORM OR CMS.
							*  LEARN WHY DEFINING THESE VARIABLES IS IMPORTANT: https://disqus.com/admin/universalcode/#configuration-variables*/
							/*
							var disqus_config = function () {
							this.page.url = PAGE_URL;  // Replace PAGE_URL with your page's canonical URL variable '{{url()->current()}}'
							this.page.identifier = PAGE_IDENTIFIER; // Replace PAGE_IDENTIFIER with your page's unique identifier variable
							};
							*/
							// (function() { // DON'T EDIT BELOW THIS LINE
							// 	var d = document, s = d.createElement('script');
							// 	s.src = 'https://rccg-chapel-of-life.disqus.com/embed.js';
							// 	s.setAttribute('data-timestamp', +new Date());
							// 	(d.head || d.body).appendChild(s);
							// })();
						</script>
						<noscript>Please enable JavaScript to view the <a href="https://disqus.com/?ref_noscript">comments powered by Disqus.</a>	</noscript>
					</div> -->

					<!-- <div class="comments">
						<h4>COMMENTS</h4>
					
						<ul>
							<li>
								<div class="comment">
									<div class="avatar"><img src="images/resource/comment1.jpg" alt="" /><a href="#" title="">REPLY</a></div>
									<h5>Thoms Gomz Britian
									<i><span>September</span> 24, 2013 at <span>1:05 pm</span></i></h5>
									<p>Praesent rhoncus nunc vitae metus condi tum viverra. Fusce sed estorci condime felis. Ndisse ullamcorper vulputate sagittis. Quisque ullamcorper euismod.</p>
								</div>	
								<ul>
									<li>
										<div class="comment">
											<div class="avatar"><img src="images/resource/comment2.jpg" alt="" /><a href="#" title="">REPLY</a></div>
											<h5>Thoms Gomz Britian
											<i><span>September</span> 24, 2013 at <span>1:05 pm</span></i></h5>
											<p>Praesent rhoncus nunc vitae metus condi tum viverra. Fusce sed estorci condime felis. Ndisse ullamcorper vulputate sagittis .</p>
										</div>
									</li>
								</ul>								
							</li>
							<li>
								<div class="comment">
									<div class="avatar"><img src="images/resource/comment3.jpg" alt="" /><a href="#" title="">REPLY</a></div>
									<h5>Thoms Gomz Britian
									<i><span>September</span> 24, 2013 at <span>1:05 pm</span></i></h5>
									<p>Praesent rhoncus nunc vitae metus condi tum viverra. Fusce sed estorci condime felis. Ndisse ullamcorper vulputate sagittis. Quisque ullamcorper euismod.</p>
								</div>
							</li>
						
						</ul>
					</div> -->									

					<!-- <div class="leave-comment">
						<h4><i class="fa fa-edit"></i> LEAVE A COMMENT</h4>
						<p>Your email address will not be published.</p>
						<form>
							<input type="text" placeholder="Name" />
							<input type="email" placeholder="Email" />
							<textarea placeholder="Description"></textarea>
							<input type="submit" value="Comment" />
						</form>
					</div> -->
				</div>
				<aside class="col-md-4 sidebar column">
					<div class="widget">
						<form class="search-form">
							<input type="text" placeholder="START SEARCHING" />
							<input type="submit" value="" />
						</form>
					</div><!-- SEARCH FORM -->

					<div class="widget">
						<div class="widget-title"><h4>TAG CLOUD</h4></div>
						<div class="tagclouds">
							<a href="blog" title="">Aenen</a>
							<a href="blog" title="">Suspendise</a>
							<a href="blog" title="">Citrous</a>
							<a href="blog" title="">Valitsantego</a>
							<a href="blog" title="">Pellntesious</a>
							<a href="blog" title="">Vestibu</a>
							<a href="blog" title="">Aenen</a>
							<a href="blog" title="">Suspendise</a>
							<a href="blog" title="">Citrous</a>
						</div>
					</div><!-- TAG CLOUD -->

					<div class="widget">
						<div class="widget-title"><h4>RECENT BLOG</h4></div>
						<div class="remove-ext">
							<div class="widget-blog">
								<div class="widget-blog-img"><img src="images/resource/widget-blog.jpg" alt="" /></div>
								<p><a href="blog-single.html" title="">Suspendisse velit anteg, aliquet vel adiping.</a></p>
								<span><i class="fa fa-calendar-o"></i> November 01, 2013</span>
							</div><!-- WIDGET BLOG -->

							<div class="widget-blog">
								<div class="widget-blog-img"><img src="images/resource/widget-blog2.jpg" alt="" /></div>
								<p><a href="blog-single.html" title="">Suspendisse velit anteg, aliquet vel adiping.</a></p>
								<span><i class="fa fa-calendar-o"></i> November 01, 2013</span>
							</div><!-- WIDGET BLOG -->
						</div>						
					</div><!-- RECENT BLOG -->

					<div class="widget">
						<div class="widget-title"><h4>VIDEO</h4></div>
						<div class="video">
							<div class="video-img">
								<img src="images/resource/video.jpg" alt="" />
								<a href="http://vimeo.com/44867610"  data-rel="prettyPhoto" title=""><i class="fa fa-play"></i></a>
							</div>
						</div>
					</div><!-- VIDEO -->


					<div class="widget">
						<div class="widget-title"><h4>META</h4></div>
						<ul>
							<li><a href="blog.html" title=""><i class="fa fa-hand-o-right"></i>Log in</a></li>
							<li><a href="blog.html" title=""><i class="fa fa-hand-o-right"></i>Entries RSS</a></li>
							<li><a href="blog.html" title=""><i class="fa fa-hand-o-right"></i>Comments RSS</a></li>
							<li><a href="blog.html" title=""><i class="fa fa-hand-o-right"></i>Wordpress.org</a></li>
						</ul>
					</div><!-- META -->
				</aside><!-- SIDEBAR -->
			</div>
		</div>
	</div>
</section>
@stop
@section('after_footer')
	<!-- SCRIPTS-->
	<script type="text/javascript" src="js/modernizr.custom.17475.js"></script>

	<script src="js/jquery.1.10.2.js" type="text/javascript"></script>
    <script src="js/bootstrap.min.js"></script>
	<script src="js/jquery.prettyPhoto.js" type="text/javascript"></script>
    <script src="js/script.js"></script>
    <script src="js/styleswitcher.js"></script>
	<script type="text/javascript" src="js/jquery.downCount.js"></script> 
@stop