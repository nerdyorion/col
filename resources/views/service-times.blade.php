@extends('base')
@section('title')Service Times @parent @stop
@section('meta')
@parent
<meta name="description" content="The Redeemed Christian Church of God Chapel of Life - A place of worship where Jesus is enthroned, the undiluted Word of God is preached, the presence of God dwells. Join us for any of our services and experience the love and power of God in all areas of your life. Jesus loves you." />
<meta name="keywords" content="chapel of life, rccg, church, redeemed, christ, love, faith, peace, christian faith, religion, festac, lagos, amuwo odofin, jesus, mile two, nigeria" />
@stop
@section('styles')
@parent

<link rel="stylesheet" href="css/mediaelementplayer.min.css" />
<link rel="stylesheet" type="text/css" href="css/colors/blue.css" title="color1" />
@stop

@section('content')
<div class="page-top">
	<div class="parallax" style="background:url(images/parallax1.jpg);"></div>	
	<div class="container"> 
		<h1>SERVICES <span>TIMES</span></h1>
		<ul>
			<li><a href="./" title="">Home</a></li>
			<li><a href="#" title="">The Church</a></li>
			<li><a href="#">Service Times</a></li>
		</ul>
	</div>
</div><!--- PAGE TOP -->

<section>
	<div class="block">
		<div class="container">
			<div class="row">
				<div class="col-md-12 column">
					<div class="row">
						<div class="service-listing">
						<div class="col-md-4 column">
							<div class="service-block">
								<div class="service-image">
									<img src="images/resource/partnerships.jpg" alt="" />
									<i class="fa fa-codepen"></i>
								</div>
								<h3>Our Partnerships</h3>
								<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry.</p>
								<a href="service-single.html" title="">Read More</a>
							</div>
						</div>

						<div class="col-md-4 column">
							<div class="service-block">
								<div class="service-image">
									<img src="images/resource/prayers.jpg" alt="" />
									<i class="fa fa-vine"></i>
								</div>
								<h3>Our Prayers</h3>
								<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry.</p>
								<a href="service-single.html" title="">Read More</a>
							</div>
						</div>

						<div class="col-md-4 column">
							<div class="service-block">
								<div class="service-image">
									<img src="images/resource/worship.jpg" alt="" />
									<i class="fa fa-stumbleupon"></i>
								</div>
								<h3>Our Worships</h3>
								<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry.</p>
								<a href="#" title="">Read More</a>
							</div>
						</div>

						<div class="col-md-4 column">
							<div class="service-block">
								<div class="service-image">
									<img src="images/resource/partnerships.jpg" alt="" />
									<i class="fa fa-codepen"></i>
								</div>
								<h3>Our Partnerships</h3>
								<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry.</p>
								<a href="service-single.html" title="">Read More</a>
							</div>
						</div>

						<div class="col-md-4 column">
							<div class="service-block">
								<div class="service-image">
									<img src="images/resource/prayers.jpg" alt="" />
									<i class="fa fa-vine"></i>
								</div>
								<h3>Our Prayers</h3>
								<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry.</p>
								<a href="service-single.html" title="">Read More</a>
							</div>
						</div>

						<div class="col-md-4 column">
							<div class="service-block">
								<div class="service-image">
									<img src="images/resource/worship.jpg" alt="" />
									<i class="fa fa-stumbleupon"></i>
								</div>
								<h3>Our Worships</h3>
								<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry.</p>
								<a href="service-single.html" title="">Read More</a>
							</div>
						</div>
						<div class="col-md-4 column">
							<div class="service-block">
								<div class="service-image">
									<img src="images/resource/partnerships.jpg" alt="" />
									<i class="fa fa-codepen"></i>
								</div>
								<h3>Our Partnerships</h3>
								<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry.</p>
								<a href="service-single.html" title="">Read More</a>
							</div>
						</div>

						<div class="col-md-4 column">
							<div class="service-block">
								<div class="service-image">
									<img src="images/resource/prayers.jpg" alt="" />
									<i class="fa fa-vine"></i>
								</div>
								<h3>Our Prayers</h3>
								<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry.</p>
								<a href="service-single.html" title="">Read More</a>
							</div>
						</div>

						<div class="col-md-4 column">
							<div class="service-block">
								<div class="service-image">
									<img src="images/resource/worship.jpg" alt="" />
									<i class="fa fa-stumbleupon"></i>
								</div>
								<h3>Our Worships</h3>
								<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry.</p>
								<a href="service-single.html" title="">Read More</a>
							</div>
						</div>
						</div>

					</div>

						<div class="theme-pagination">
							<ul class="pagination">
								<li><a href="#"><i class="fa fa-angle-left"></i></a></li>
								<li><a href="#">1</a></li>
								<li><a href="#">2</a></li>
								<li><a href="#">3</a></li>
								<li><a href="#">4</a></li>
								<li><a href="#">5</a></li>
								<li><a href="#"><i class="fa fa-angle-right"></i></a></li>
							</ul>
						</div><!-- PAGINATION -->
					
				</div>				
			</div>
		</div>
	</div>
</section>	
@stop
@section('after_footer')
	<!-- SCRIPTS-->
	<script type="text/javascript" src="js/modernizr.custom.17475.js"></script>

	<script src="js/jquery.1.10.2.js" type="text/javascript"></script>
    <script src="js/bootstrap.min.js"></script>
	<script src="js/jquery.prettyPhoto.js" type="text/javascript"></script>
    <script src="js/script.js"></script>
    <script src="js/styleswitcher.js"></script>
	<script type="text/javascript" src="js/jquery.downCount.js"></script> 
@stop