@extends('base')
@section('title')Videos @parent @stop
@section('meta')
@parent
<meta name="description" content="The Redeemed Christian Church of God Chapel of Life - A place of worship where Jesus is enthroned, the undiluted Word of God is preached, the presence of God dwells. Join us for any of our services and experience the love and power of God in all areas of your life. Jesus loves you." />
<meta name="keywords" content="chapel of life, rccg, church, redeemed, christ, love, faith, peace, christian faith, religion, festac, lagos, amuwo odofin, jesus, mile two, nigeria" />
@stop
@section('styles')
@parent

<link rel="stylesheet" href="css/mediaelementplayer.min.css" />
<link rel="stylesheet" type="text/css" href="css/colors/blue.css" title="color1" />
@stop

@section('content')
<div class="page-top">
	<div class="parallax" style="background:url(images/parallax1.jpg);"></div>	
	<div class="container"> 
		<h1>OUR <span>VIDEOS</span></h1>
		<ul>
			<li><a href="./" title="">Home</a></li>
			<li><a href="#">Media Center</a></li>
			<li><a href="#">Videos</a></li>
		</ul>
	</div>
</div><!--- PAGE TOP -->

<section>
	<div class="block">
		<div class="container">
			<div class="row">
				<div class="col-md-12">
					<div class="remove-ext">
						<div class="row">
							<div class="mas-gallery" >
								<!-- Mark Zuckerberg in CcHub Lagos -->
								<div class="col-md-4">
									<div class="gallery">
										<iframe width="100%" height="235px" src="https://www.youtube.com/embed/KYXpl9iyrs0" frameborder="0" allowfullscreen></iframe>
									</div>
								</div>

								<!-- Mark Zuckerberg in CcHub Lagos -->
								<div class="col-md-4">
									<div class="gallery">
										<iframe width="100%" height="235px" src="https://www.youtube.com/embed/KYXpl9iyrs0" frameborder="0" allowfullscreen></iframe>
									</div>
								</div>

								<!-- Mark Zuckerberg in CcHub Lagos -->
								<div class="col-md-4">
									<div class="gallery">
										<iframe width="100%" height="235px" src="https://www.youtube.com/embed/KYXpl9iyrs0" frameborder="0" allowfullscreen></iframe>
									</div>
								</div>

								<!-- Mark Zuckerberg in CcHub Lagos -->
								<div class="col-md-4">
									<div class="gallery">
										<iframe width="100%" height="235px" src="https://www.youtube.com/embed/KYXpl9iyrs0" frameborder="0" allowfullscreen></iframe>
									</div>
								</div>

							</div>
						</div>						
					</div>

					<!-- Max. 6 per page -->
					<div class="theme-pagination">
						<ul class="pagination">
							<li><a href="#"><i class="fa fa-angle-left"></i></a></li>
							<li><a href="#">1</a></li>
							<li><a href="#">2</a></li>
							<li><a href="#">3</a></li>
							<li><a href="#">4</a></li>
							<li><a href="#">5</a></li>
							<li><a href="#"><i class="fa fa-angle-right"></i></a></li>
						</ul>
					</div><!-- PAGINATION -->
					
				</div>
			</div>
		</div>
	</div>
</section>	
@stop
@section('after_footer')
	<!-- SCRIPTS-->
	<script type="text/javascript" src="js/modernizr.custom.17475.js"></script>

	<script src="js/jquery.1.10.2.js" type="text/javascript"></script>
    <script src="js/bootstrap.min.js"></script>
	<script src="js/jquery.prettyPhoto.js" type="text/javascript"></script>
    <script src="js/script.js"></script>
    <script src="js/styleswitcher.js"></script>
	<script type="text/javascript" src="js/jquery.downCount.js"></script> 

	<script src="js/jquery.isotope.min.js"></script>
	<script>
	$(window).load(function(){
		$(function(){
			var $portfolio = $('.mas-gallery');
			$portfolio.isotope({
			masonry: {
			  columnWidth: 1
			}
			});
		});
	});

	</script>
@stop