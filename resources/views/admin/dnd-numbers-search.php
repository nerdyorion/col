  <!-- Page Content -->
  <div id="page-wrapper">
    <div class="container-fluid">
      <div class="row bg-title">
        <div class="col-lg-12">
          <h4 class="page-title">Search DND Number</h4>
          <ol class="breadcrumb">
            <li><a href="./">Dashboard</a></li>
            <li class="active">Search DND Number</li>
          </ol>
        </div>
        <!-- /.col-lg-12 -->
      </div>
      <!-- row -->
      <div class="row">
        <div class="col-sm-12">
          <div class="white-box">
            <div class="row">
              <div class="col-md-12">
                <?php echo form_open_multipart('DND-Numbers/search', 'class="form-horizontal", onsubmit="return validate();"'); ?>
                <!--<form class="form-horizontal" action="<?php echo current_url(); ?>" enctype="multipart/form-data" method="post">-->
                  <div class="form-group">
                    <label for="network_id" class="col-sm-3 control-label">Network:</label>
                    <div class="col-sm-9">
                      <select class="form-control" name="network_id" id="network_id">
                        <option value="0">-- All --</option>
                        <option value="1" <?php echo set_value('network_id') == 1 ? 'selected="selected"' : ''; ?>>MTN</option>
                        <option value="2" <?php echo set_value('network_id') == 2 ? 'selected="selected"' : ''; ?>>EMTS</option>
                        <option value="3" <?php echo set_value('network_id') == 3 ? 'selected="selected"' : ''; ?>>GLO</option>
                        <option value="4" <?php echo set_value('network_id') == 4 ? 'selected="selected"' : ''; ?>>AIRTEL</option>
                        <option value="5" <?php echo set_value('network_id') == 5 ? 'selected="selected"' : ''; ?>>NTEL</option>
                      </select>
                    </div>
                  </div>
                  <div class="form-group">
                    <label for="msisdn" class="col-sm-3 control-label">MSISDN: <span class="text-danger">*</span></label>
                    <div class="col-sm-9">
                      <!--<input type="text" class="form-control" onkeypress='return event.charCode >= 48 && event.charCode <= 57'>-->
                      <textarea rows="5" type="text" class="form-control" name="msisdn" maxlength="5000" id="msisdn" required=""><?php echo set_value('msisdn'); ?></textarea>
                    </div>
                  </div>
                  <div class="form-group m-b-0">
                    <div class="col-sm-offset-3 col-sm-9">
                      <button type="submit" class="btn btn-info waves-effect waves-light">Search</button>
                    </div>
                  </div>
                </form>
              </div>
            </div>
          </div>
        </div>
        
      </div>
      <!-- row -->
      <div class="row">
        <div class="col-sm-12">
          <div class="white-box">
            <h3>Search Result</h3>
            <!--<p class="text-muted m-b-20">Lorem<code>ipsum.table-bordered</code>.</p>-->
            <div class="table-responsive">
              <table class="table table-bordered">
                <thead>
                  <tr>
                    <th>#</th>
                    <th>Network</th>
                    <th>MSISDN</th>
                    <th>Blocking Mode</th>
                    <th>Operate Date</th>
                    <th class="text-nowrap">Action</th>
                  </tr>
                </thead>
                <tbody>
                <?php if(empty($rows)): ?>
                  <tr>
                    <td colspan="5" align="center">No data returned.</td>
                  </tr>
                <?php else: ?>
                <?php $sn = 1; foreach ($rows as $row): ?>
                  <tr>
                    <td><?php echo $sn++; ?></td>
                    <td><?php echo $row['network_title_short']; ?></td>
                    <td><?php echo $row['msisdn']; ?></td>
                    <td><?php echo $row['blocking_mode']; ?></td>
                    <td><?php echo date('M d, Y h:i A', strtotime($row['operate_date'])); ?></td>
                    <td class="text-nowrap">
                      <!--
                      <a href="#" data-toggle="tooltip" data-original-title="Edit"> <i class="fa fa-pencil text-inverse m-r-10"></i> </a> 
                      <a href="#" data-toggle="tooltip" data-original-title="Delete"> <i class="fa fa-close text-danger"></i> </a> 
                      -->
                      <a href="DND-Numbers/view/<?php echo $row['id']; ?>" data-toggle="tooltip" data-original-title="View"> <i class="fa fa-folder-open text-inverse m-r-10"></i> </a> 
                    </td>
                  </tr>
                <?php endforeach; ?>
                <?php endif; ?>
                </tbody>
              </table>
            </div>
          </div>
        </div>
      </div>
    </div>
    <!-- /.container-fluid -->
  </div>
  <!-- /#page-wrapper -->

<?php $this->load->view($this->config->item('template_dir_admin') . 'footer', array('error', $error, 'error_code', $error_code)); echo "\n";  // load footer view ?>
<script src="assets/js/jasny-bootstrap.js"></script>
<script type="text/javascript">
/*
  function validate()
  {
    if((document.getElementById("network_id").value == '0') || (document.getElementById("network_id").value == null)){
      alert('Please select network.');
      return false;
    }
    else {
      return true;
    }
  }
  */
</script>

</body>
</html>
