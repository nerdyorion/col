  <!-- Page Content -->
  <div id="page-wrapper">
    <div class="container-fluid">
      <div class="row bg-title">
        <div class="col-lg-12">
          <h4 class="page-title">Search MNP Number</h4>
          <ol class="breadcrumb">
            <li><a href="./">Dashboard</a></li>
            <li><a class="active">Search MNP Number</a></li>
          </ol>
        </div>
        <!-- /.col-lg-12 -->
      </div>
      <!-- row -->
      <div class="row">
        <div class="col-sm-12">
          <div class="white-box">
            <div class="row">
              <div class="col-md-12">
                <?php echo form_open_multipart('MNP-Numbers/search', 'class="form-horizontal", onsubmit="return validate();"'); ?>
                <!--<form class="form-horizontal" action="<?php echo current_url(); ?>" enctype="multipart/form-data" method="post">-->
                  <div class="form-group">
                    <label for="msisdn" class="col-sm-3 control-label">MSISDN: <span class="text-danger">*</span></label>
                    <div class="col-sm-9">
                      <!--<input type="text" class="form-control" onkeypress='return event.charCode >= 48 && event.charCode <= 57'>-->
                      <textarea rows="5" type="text" class="form-control" name="msisdn" maxlength="5000" id="msisdn" required=""><?php echo set_value('msisdn'); ?></textarea>
                    </div>
                  </div>
                  <div class="form-group m-b-0">
                    <div class="col-sm-offset-3 col-sm-9">
                      <button type="submit" class="btn btn-info waves-effect waves-light">Search</button>
                    </div>
                  </div>
                </form>
              </div>
            </div>
          </div>
        </div>
        
      </div>
      <!-- row -->
      <div class="row">
        <div class="col-sm-12">
          <div class="white-box">
            <h3>Search Result</h3>
            <!--<p class="text-muted m-b-20">Lorem<code>ipsum.table-bordered</code>.</p>-->
            <div class="table-responsive">
              <table class="table table-bordered">
                <thead>
                  <tr>
                    <th>#</th>
                    <th>MSISDN</th>
                    <th>Donor ID</th>
                    <th>Recipient ID</th>
                    <th>Timestamp</th>
                  </tr>
                </thead>
                <tbody>
                <?php if(empty($rows)): ?>
                  <tr>
                    <td colspan="5" align="center">No data returned.</td>
                  </tr>
                <?php else: ?>
                <?php $sn = 1; foreach ($rows as $row): ?>
                  <tr>
                    <td><?php echo $sn++; ?></td>
                    <td><?php echo $row['msisdn']; ?></td>
                    <td><?php echo $row['donor_id']; ?></td>
                    <td><?php echo $row['recipient_id']; ?></td>
                    <td><?php echo date('M d, Y h:i A', strtotime($row['date_time_stamp'])); ?></td>
                  </tr>
                <?php endforeach; ?>
                <?php endif; ?>
                </tbody>
              </table>
            </div>
          </div>
        </div>
      </div>
    </div>
    <!-- /.container-fluid -->
  </div>
  <!-- /#page-wrapper -->

<?php $this->load->view($this->config->item('template_dir_admin') . 'footer', array('error', $error, 'error_code', $error_code)); echo "\n";  // load footer view ?>
<script src="assets/js/jasny-bootstrap.js"></script>

</body>
</html>
