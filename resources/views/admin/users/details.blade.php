@extends('templates.admin.layout', ['page_title' => $data['page_title']])
@section('content')
  <!-- Page Content -->
  <div id="page-wrapper">
    <div class="container-fluid">
      <div class="row bg-title">
        <div class="col-lg-12">
          <h4 class="page-title">User Details</h4>
          <ol class="breadcrumb">
            <li><a href="{{ URL::to('/') }}/manage/">Dashboard</a></li>
            <li><a href="{{ URL::to('/') }}/manage/users">Users</a></li>
            <li class="active">Details</li>
          </ol>
        </div>
        <!-- /.col-lg-12 -->
      </div>

      <!-- row -->
      <div class="row">
        <div class="col-md-12">
          <div class="white-box">
            <div class="table-responsive">
              <table class="table table-bordered table-striped">
                <tbody>
                  <tr>
                    <th>Full Name</th>
                    <td>{{ $data['row']['name'] }}</td>
                  </tr>
                  <tr>
                    <th>Role</th>
                    <td>{{ $data['row']['role_name'] }}</td>
                  </tr>
                  <tr>
                    <th>Email</th>
                    <td><a href="mailto:{{ $data['row']['email'] }}">{{ $data['row']['email'] }}</a></td>
                  </tr>
                    <th>Phone</th>
                    <td>{{ $data['row']['phone'] }}</td>
                  </tr>
                  <tr>
                    <th>Gender</th>
                    <td>{{ $data['row']['gender'] }}</td>
                  </tr>
                  <tr>
                    <th>Last Login</th>
                    <td>{{ $data['row']['last_login'] == "0000-00-00 00:00:00" || $data['row']['last_login'] == NULL ? "-" : date('M d, Y h:i A', strtotime($data['row']['last_login'])) }}</td>
                  </tr>
                </tbody>
              </table>
            </div>
            <p class="text-center">
              <button type="button" class="btn btn-info waves-effect waves-light center" onclick="history.go(-1); return false;">&larr; Back</button>
            </p>
          </div>
        </div>
      </div>
      <!-- /.row -->
    </div>
    <!-- /.container-fluid -->
  </div>
  <!-- /#page-wrapper -->
@stop