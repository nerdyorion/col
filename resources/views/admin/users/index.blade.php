@extends('templates.admin.layout', ['page_title' => $data['page_title']])
@section('content')
  <!-- Page Content -->
  <div id="page-wrapper">
    <div class="container-fluid">
      <div class="row bg-title">
        <div class="col-lg-12">
          <h4 class="page-title">Users</h4>
          <ol class="breadcrumb">
            <li><a href="">Dashboard</a></li>
            <li class="active">Users</li>
          </ol>
        </div>
        <!-- /.col-lg-12 -->
      </div>

      <!-- row -->
      <div class="row">
        <div class="col-md-12 col-lg-12 col-sm-12 col-xs-12">
          <div class="white-box">
            <!--<h3>Users</h3>-->
            <!-- Nav tabs -->
            <ul class="nav nav-tabs" role="tablist">
              <li role="presentation" class="active"><a href="#all" aria-controls="all" role="tab" data-toggle="tab"><span class="visible-xs"><i class="ti-view-list"></i></span><span class="hidden-xs"> View All</span></a></li>
              <li role="presentation"><a href="#add" aria-controls="add" role="tab" data-toggle="tab"><span class="visible-xs"><i class="ti-plus"></i></span> <span class="hidden-xs">Add New</span></a></li>
            </ul>

            <!-- Tab panes -->
            <div class="tab-content">
              <div role="tabpanel" class="tab-pane active" id="all"> 
                <div class="col-md-12">
                  <div class="table-responsive">
                    <table class="table table-bordered">
                      <thead>
                        <tr>
                          <th>#</th>
                          <th>Full Name</th>
                          <th>Email</th>
                          <th>Gender</th>
                          <th>Role</th>
                          <th>Last Login</th>
                          <th class="text-nowrap">Actions</th>
                        </tr>
                      </thead>
                      <tbody>
                      @if($data['rows']->isEmpty())
                        <tr>
                          <td colspan="6" align="center">No data returned.</td>
                        </tr>
                      @else
                      <?php $sn = 1; ?>
                      @foreach ($data['rows'] as $row)
                        <tr>
                          <td><?php echo $sn++; ?></td>
                          <td>{{ $row['name'] }}</td>
                          <td><a href="mailto:{{ $row['email'] }}">{{ $row['email'] }}</a></td>
                          <td>{{ $row['gender'] }}</td>
                          <td>{{ $row['role_name'] }}</td>
                          <td>{{ $row['last_login'] == NULL ? "-" : date('M d, Y h:i A', strtotime($row['last_login'])) }}</td>
                          <!--<td><?php //echo date('M d, Y h:i A', strtotime($row['last_login'])) == "Jan 01, 1970 12:00 AM" ? "-" : date('M d, Y h:i A', strtotime($row['last_login'])); ?></td>-->
                          <td class="text-nowrap">
                            <form action="users/{{ $row['id'] }}" method="post" onsubmit="return confirm('Are you sure you want to delete this record?');">
                              {{ csrf_field() }}
                              <input type="hidden" name="_method" value="DELETE">
                              <!-- <a href="users/{{ $row['id'] }}/edit" data-toggle="tooltip" data-original-title="Edit"> <i class="fa fa-pencil text-inverse m-r-10"></i></a>  -->
                              <!-- <a href="users/delete/{{ $row['id'] }}" data-toggle="tooltip" data-original-title="Delete"> <span class="text-danger"><i class="fa fa-close text-danger m-r-10" onclick="if(confirm('Are you sure you want to delete this record?')) return true; else return false;"></i></span> </a> -->
                              <!-- <a href="users/view/{{ $row['id'] }}" data-toggle="tooltip" data-original-title="View"> <i class="fa fa-folder-open text-inverse m-r-10"></i> </a>  -->
                              <button type="button" class="btn btn-primary btn-circle btn-xs" data-toggle="tooltip" data-original-title="Edit" onclick="window.location='users/{{ $row['id'] }}/edit'"><i class="fa fa-pencil"></i></button> 
                              <button type="submit" class="btn btn-danger btn-circle btn-xs" data-toggle="tooltip" data-original-title="Delete"><i class="fa fa-trash-o"></i></button> 
                              <button type="button" class="btn btn-primary btn-circle btn-xs" data-toggle="tooltip" data-original-title="View" onclick="window.location='users/{{ $row['id'] }}'"><i class="fa fa-folder-open"></i></button> 
                            </form>
                          </td>
                        </tr>
                      @endforeach
                      @endif
                      </tbody>
                    </table>
                  </div>
                </div>
                
                <div class="col-md-3 pull-right">
                  <p><a href="?page=2">Pagination</a>.</p>
                </div>
                
                <div class="clearfix"></div>
              </div>
              <div role="tabpanel" class="tab-pane" id="add">
                <div class="col-md-12">
                <form action="users" method="post" class="form-horizontal" onsubmit="return validate();">
                  {{ csrf_field() }}
                  <div class="form-group">
                    <label for="role_id" class="col-sm-2 control-label">Role: <span class="text-danger">*</span></label>
                    <div class="col-sm-10">
                      <select class="form-control" name="role_id" id="role_id" required="required">
                        {{-- <!-- <option value="{{ $key }}" {{ (Input::old("title") == $key ? "selected":"") }}>{{ $val }}</option> --> --}}
                        <option value="0" {{ old('role_id') == NULL ? 'selected="selected"' : '' }} >-- Select --</option>
                        <option value="1" {{ old('role_id') == "1" ? 'selected="selected"' : '' }} >Super Administrator</option>
                        <option value="2" {{ old('role_id') == "2" ? 'selected="selected"' : '' }} >Administrator</option>
                        <option value="3" {{ old('role_id') == "3" ? 'selected="selected"' : '' }} >Report Admin</option>
                      </select>
                    </div>
                  </div>
                  <div class="form-group">
                    <label for="full_name" class="col-sm-2 control-label">Full Name: <span class="text-danger">*</span></label>
                    <div class="col-sm-10">
                      <input type="text" class="form-control" name="full_name" maxlength="2000" id="full_name" value="{{ old('full_name') }}" placeholder="" required="required">
                    </div>
                  </div>
                  <div class="form-group">
                    <label for="email" class="col-sm-2 control-label">Email: <span class="text-danger">*</span></label>
                    <div class="col-sm-10">
                      <input type="email" class="form-control" name="email" maxlength="255" id="email" value="{{ old('email') }}" placeholder="" required="required">
                    </div>
                  </div>
                  <div class="form-group">
                    <label for="phone" class="col-sm-2 control-label">Phone:</label>
                    <div class="col-sm-10">
                      <input type="text" class="form-control" onkeypress='return event.charCode >= 48 && event.charCode <= 57' name="phone" maxlength="13" id="phone" value="{{ old('phone') }}" placeholder="">
                    </div>
                  </div>
                  <div class="form-group">
                    <label for="password" class="col-sm-2 control-label">Password: <span class="text-danger">*</span></label>
                    <div class="col-sm-10">
                      <input type="password" class="form-control" name="password" maxlength="8000" id="password" value="" placeholder="" required="required">
                    </div>
                  </div>
                  <div class="form-group">
                    <label for="password_confirmation" class="col-sm-2 control-label">Confirm Password: <span class="text-danger">*</span></label>
                    <div class="col-sm-10">
                      <input type="password" class="form-control" name="password_confirmation" maxlength="8000" id="password_confirmation" value="" placeholder="" required="required">
                    </div>
                  </div>
                  <div class="form-group">
                    <label for="gender" class="col-sm-2 control-label">Gender: <span class="text-danger">*</span></label>
                    <div class="col-sm-10">
                      <select class="form-control" name="gender" id="gender" required="required">
                        <option value="0" {{ old('gender') == NULL ? 'selected="selected"' : '' }} >-- Select --</option>
                        <option value="Male" {{ old('gender') == "Male" ? 'selected="selected"' : '' }} >Male</option>
                        <option value="Female" {{ old('gender') == "Female" ? 'selected="selected"' : '' }} >Female</option>
                      </select>
                    </div>
                  </div>
                  <div class="form-group">
                    <label for="address" class="col-sm-2 control-label">Address:</label>
                    <div class="col-sm-10">
                      <textarea class="form-control" name="address" id="address" maxlength="8000">{{ old('address') }}</textarea>
                    </div>
                  </div>
                  <div class="form-group m-b-0">
                    <div class="col-sm-offset-3 col-sm-9">
                      <button type="submit" class="btn btn-info waves-effect waves-light">Add New</button>
                    </div>
                  </div>
                </form>
                </div>
                <div class="clearfix"></div>
              </div>
            </div>
          </div>
        </div>
      </div> 
      <!-- /.row -->
    </div>
    <!-- /.container-fluid -->
  </div>
  <!-- /#page-wrapper -->
  
<script type="text/javascript">
  function validate()
  {
    var password = document.getElementById("password").value;
    var password_confirmation = document.getElementById("password_confirmation").value;
    var gender = document.getElementById("gender").value;
    var role_id = document.getElementById("role_id").value;
    if(password != password_confirmation ){
      alert('Passwords do not match.');
      document.getElementById("password_confirmation").focus();
      return false;
    }
    else if(gender == 0 ){
      alert('Please specify gender.');
      return false;
    }
    else if(role_id == 0 ){
      alert('Please specify role.');
      return false;
    }
    else {
      return true;
    }
  }
</script>
@stop