@extends('templates.admin.layout', ['page_title' => $data['page_title']])
@section('content')
  <!-- Page Content -->
  <div id="page-wrapper">
    <div class="container-fluid">
      <div class="row bg-title">
        <div class="col-lg-12">
          <h4 class="page-title">Update User</h4>
          <ol class="breadcrumb">
            <li><a href="{{ URL::to('/') }}/manage/">Dashboard</a></li>
            <li><a href="{{ URL::to('/') }}/manage/users">Users</a></li>
            <li class="active">Edit</li>
          </ol>
        </div>
        <!-- /.col-lg-12 -->
      </div>

      <!-- row -->
      <div class="row">
        <div class="col-md-12 col-lg-12 col-sm-12 col-xs-12">
          <div class="white-box">
            <!--<h3>Users</h3>-->
            <!-- Nav tabs -->
            <ul class="nav nav-tabs" role="tablist">
              <li role="presentation" class="active"><a href="#update" aria-controls="update" role="tab" data-toggle="tab"><span class="visible-xs"><i class="ti-pencil"></i></span><span class="hidden-xs"> Update</span></a></li>
            </ul>

            <!-- Tab panes -->
            <div class="tab-content">
              <div role="tabpanel" class="tab-pane active" id="update">
                <div class="col-md-12">
                <form action="users/{{ $data['id'] }}" method="post" class="form-horizontal" onsubmit="return validate();">
                  {{ csrf_field() }}
                  <input type="hidden" name="_method" value="PUT">
                  <div class="form-group">
                    <label for="role_id" class="col-sm-2 control-label">Role: <span class="text-danger">*</span></label>
                    <div class="col-sm-10">
                      <select class="form-control" name="role_id" id="role_id" required="required">
                        <option value="0">-- Select --</option>
                        <option value="1" {{ strtoupper($data['row']['role_name']) == "SUPER ADMINISTRATOR" ? 'selected="selected"' : '' }}>Super Administrator</option>
                        <option value="2" {{ strtoupper($data['row']['role_name']) == "ADMINISTRATOR" ? 'selected="selected"' : '' }}>Administrator</option>
                        <option value="3" {{ strtoupper($data['row']['role_name']) == "REPORT ADMIN" ? 'selected="selected"' : '' }}>Report Admin</option>
                      </select>
                    </div>
                  </div>
                  <div class="form-group">
                    <label for="full_name" class="col-sm-2 control-label">Full Name: <span class="text-danger">*</span></label>
                    <div class="col-sm-10">
                      <input type="text" class="form-control" name="full_name" maxlength="2000" id="full_name" value="{{ $data['row']['name'] }}" placeholder="" required="required">
                    </div>
                  </div>
                  <div class="form-group">
                    <label for="email" class="col-sm-2 control-label">Email: <span class="text-danger">*</span></label>
                    <div class="col-sm-10">
                      <input type="email" class="form-control" name="email" maxlength="255" id="email" value="{{ $data['row']['email'] }}" placeholder="" required="required">
                    </div>
                  </div>
                  <div class="form-group">
                    <label for="phone" class="col-sm-2 control-label">Phone:</label>
                    <div class="col-sm-10">
                      <input type="text" class="form-control" onkeypress='return event.charCode >= 48 && event.charCode <= 57' name="phone" maxlength="13" id="phone" value="{{ $data['row']['phone'] }}" placeholder="">
                    </div>
                  </div>
                  <div class="form-group">
                    <label for="gender" class="col-sm-2 control-label">Gender: <span class="text-danger">*</span></label>
                    <div class="col-sm-10">
                      <select class="form-control" name="gender" id="gender" required="required">
                        <option value="0" selected="selected">-- Select --</option>
                        <option value="Male" {{ strtolower($data['row']['gender']) == "male" ? 'selected="selected"' : '' }}>Male</option>
                        <option value="Female" {{ strtolower($data['row']['gender']) == "female" ? 'selected="selected"' : '' }}>Female</option>
                      </select>
                    </div>
                  </div>
                  <div class="form-group">
                    <label for="address" class="col-sm-2 control-label">Address:</label>
                    <div class="col-sm-10">
                      <textarea class="form-control" name="address" id="address" maxlength="8000">{{ $data['row']["address"] }}</textarea>
                    </div>
                  </div>
                  <div class="form-group m-b-0">
                    <div class="col-sm-offset-3 col-sm-9">
                      <button type="submit" class="btn btn-info waves-effect waves-light">Update</button>
                      <button type="button" class="btn btn-info waves-effect waves-light center" onclick="history.go(-1); return false;">&larr; Back</button>
                    </div>
                  </div>
                </form>
                </div>
                <div class="clearfix"></div>
              </div>
            </div>
          </div>
        </div>
      </div> 
      <!-- /.row -->
    </div>
    <!-- /.container-fluid -->
  </div>
  <!-- /#page-wrapper -->

<script type="text/javascript">
  function validate()
  {
    var gender = document.getElementById("gender").value;
    var role_id = document.getElementById("role_id").value;
    if(gender == 0 ){
      alert('Please specify gender.');
      return false;
    }
    else if(role_id == 0 ){
      alert('Please specify role.');
      return false;
    }
    else {
      return true;
    }
  }
</script>
@stop