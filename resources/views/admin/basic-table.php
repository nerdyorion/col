<!DOCTYPE html>
<html lang="en">

<!-- Mirrored from themedesigner.in/demo/myadmin/myadmin-dark/basic-table.html by HTTrack Website Copier/3.x [XR&CO'2014], Fri, 24 Jun 2016 17:37:59 GMT -->
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta name="description" content="">
<meta name="author" content="">
<link rel="icon" type="image/png" sizes="16x16" href="assets/images/favicon.png">
<title>My Admin - is a responsive admin template</title>
<!-- Bootstrap Core CSS -->
<link href="assets/bower_components/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">
<!-- Menu CSS -->
<link href="assets/bower_components/metisMenu/dist/metisMenu.min.css" rel="stylesheet">
<!-- Custom CSS -->
<link href="assets/css/style.css" rel="stylesheet">
<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
<!--[if lt IE 9]>
    <script src="assets/https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="assets/https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
<![endif]-->
</head>
<body>
<!-- Preloader -->
<div class="preloader">
    <div class="cssload-speeding-wheel"></div>
</div>
<div id="wrapper">
  <!-- Navigation -->
  <nav class="navbar navbar-default navbar-static-top" style="margin-bottom: 0">
    <div class="navbar-header"> <a class="navbar-toggle hidden-sm hidden-md hidden-lg " href="javascript:void(0)" data-toggle="collapse" data-target=".navbar-collapse"><i class="ti-menu"></i></a>
      <div class="top-left-part"><a class="logo" href="assets/index-2.html"><i class="glyphicon glyphicon-fire"></i>&nbsp;<span class="hidden-xs">My Admin</span></a></div>
      <ul class="nav navbar-top-links navbar-left hidden-xs">
        <li><a href="javascript:void(0)" class="open-close hidden-xs waves-effect waves-light"><i class="icon-arrow-left-circle ti-menu"></i></a></li>
        <li class="dropdown"> <a class="dropdown-toggle waves-effect waves-light" data-toggle="dropdown" href="#"><i class="icon-envelope"></i> <span class="badge badge-xs badge-warning">5</span></a>
          <ul class="dropdown-menu nicescroll mailbox">
            <li>
              <div class="drop-title">You have 4 new messages</div>
            </li>
            <li>
              <div class="message-center"> <a href="#">
                <div class="user-img"> <img src="assets/images/users/pawandeep.jpg" alt="user" class="img-circle"> <span class="profile-status online pull-right"></span> </div>
                <div class="mail-contnet">
                  <h5>Pavan kumar</h5>
                  <span class="mail-desc">Just see the my admin!</span> <span class="time">9:30 AM</span> </div>
                </a> <a href="#">
                <div class="user-img"> <img src="assets/images/users/sonu.jpg" alt="user" class="img-circle"> <span class="profile-status busy pull-right"></span> </div>
                <div class="mail-contnet">
                  <h5>Sonu Nigam</h5>
                  <span class="mail-desc">I've sung a song! See you at</span> <span class="time">9:10 AM</span> </div>
                </a> <a href="#">
                <div class="user-img"> <img src="assets/images/users/arijit.jpg" alt="user" class="img-circle"> <span class="profile-status away pull-right"></span> </div>
                <div class="mail-contnet">
                  <h5>Arijit Sinh</h5>
                  <span class="mail-desc">I am a singer!</span> <span class="time">9:08 AM</span> </div>
                </a> <a href="#">
                <div class="user-img"> <img src="assets/images/users/pawandeep.jpg" alt="user" class="img-circle"> <span class="profile-status offline pull-right"></span> </div>
                <div class="mail-contnet">
                  <h5>Pavan kumar</h5>
                  <span class="mail-desc">Just see the my admin!</span> <span class="time">9:02 AM</span> </div>
                </a> </div>
            </li>
            <li> <a class="text-center" href="javascript:void(0);"> <strong>See all notifications</strong> <i class="fa fa-angle-right"></i> </a></li>
          </ul>
          <!-- /.dropdown-messages -->
        </li>
        <!-- /.dropdown -->
        <li class="dropdown"> <a class="dropdown-toggle waves-effect waves-light" data-toggle="dropdown" href="#"><i class="icon-note"></i> <span class="badge badge-xs badge-danger">5</span></a>
          <ul class="dropdown-menu dropdown-tasks">
            <li> <a href="#">
              <div>
                <p> <strong>Task 1</strong> <span class="pull-right text-muted">40% Complete</span> </p>
                <div class="progress progress-striped active">
                  <div class="progress-bar progress-bar-success" role="progressbar" aria-valuenow="40" aria-valuemin="0" aria-valuemax="100" style="width: 40%"> <span class="sr-only">40% Complete (success)</span> </div>
                </div>
              </div>
              </a> </li>
            <li class="divider"></li>
            <li> <a href="#">
              <div>
                <p> <strong>Task 2</strong> <span class="pull-right text-muted">20% Complete</span> </p>
                <div class="progress progress-striped active">
                  <div class="progress-bar progress-bar-info" role="progressbar" aria-valuenow="20" aria-valuemin="0" aria-valuemax="100" style="width: 20%"> <span class="sr-only">20% Complete</span> </div>
                </div>
              </div>
              </a> </li>
            <li class="divider"></li>
            <li> <a href="#">
              <div>
                <p> <strong>Task 3</strong> <span class="pull-right text-muted">60% Complete</span> </p>
                <div class="progress progress-striped active">
                  <div class="progress-bar progress-bar-warning" role="progressbar" aria-valuenow="60" aria-valuemin="0" aria-valuemax="100" style="width: 60%"> <span class="sr-only">60% Complete (warning)</span> </div>
                </div>
              </div>
              </a> </li>
            <li class="divider"></li>
            <li> <a href="#">
              <div>
                <p> <strong>Task 4</strong> <span class="pull-right text-muted">80% Complete</span> </p>
                <div class="progress progress-striped active">
                  <div class="progress-bar progress-bar-danger" role="progressbar" aria-valuenow="80" aria-valuemin="0" aria-valuemax="100" style="width: 80%"> <span class="sr-only">80% Complete (danger)</span> </div>
                </div>
              </div>
              </a> </li>
            <li class="divider"></li>
            <li> <a class="text-center" href="#"> <strong>See All Tasks</strong> <i class="fa fa-angle-right"></i> </a> </li>
          </ul>
          <!-- /.dropdown-tasks -->
        </li>
        <!-- /.dropdown -->
        <li class="dropdown"> <a class="dropdown-toggle waves-effect waves-light" data-toggle="dropdown" href="#"><i class="icon-bell"></i> <span class="badge badge-xs badge-info">5</span></a>
          <ul class="dropdown-menu dropdown-alerts">
            <li> <a href="#">
              <div> <i class="ti-comments fa-fw"></i> New Comment <span class="pull-right text-muted small">4 minutes ago</span> </div>
              </a> </li>
            <li class="divider"></li>
            <li> <a href="#">
              <div> <i class="ti-twitter fa-fw"></i> 3 New Followers <span class="pull-right text-muted small">12 minutes ago</span> </div>
              </a> </li>
            <li class="divider"></li>
            <li> <a href="#">
              <div> <i class="ti-email fa-fw"></i> Message Sent <span class="pull-right text-muted small">4 minutes ago</span> </div>
              </a> </li>
            <li class="divider"></li>
            <li> <a href="#">
              <div> <i class="ti-pencil-alt fa-fw"></i> New Task <span class="pull-right text-muted small">4 minutes ago</span> </div>
              </a> </li>
            <li class="divider"></li>
            <li> <a href="#">
              <div> <i class="ti-upload fa-fw"></i> Server Rebooted <span class="pull-right text-muted small">4 minutes ago</span> </div>
              </a> </li>
            <li class="divider"></li>
            <li> <a class="text-center" href="#"> <strong>See All Alerts</strong> <i class="fa fa-angle-right"></i> </a> </li>
          </ul>
          <!-- /.dropdown-alerts -->
        </li>
      </ul>
      <ul class="nav navbar-top-links navbar-right pull-right">
        <li>
          <form role="search" class="app-search hidden-xs">
            <input type="text" placeholder="Search..." class="form-control">
            <a href="#"><i class="fa fa-search"></i></a>
          </form>
        </li>
        <li class="dropdown"> <a class="dropdown-toggle profile-pic" data-toggle="dropdown" href="#"> <img src="assets/images/users/hritik.jpg" alt="user-img" width="36" class="img-circle"><b class="hidden-xs">Maruti</b> </a>
          <ul class="dropdown-menu dropdown-user">
            <li><a href="#"><i class="ti-user"></i> My Profile</a></li>
            <li><a href="#"><i class="ti-wallet"></i> My Balance</a></li>
            <li><a href="#"><i class="ti-email"></i> Inbox</a></li>
            <li role="separator" class="divider"></li>
            <li><a href="#"><i class="ti-settings"></i> Account Setting</a></li>
            <li role="separator" class="divider"></li>
            <li><a href="#"><i class="fa fa-power-off"></i> Logout</a></li>
          </ul>
          <!-- /.dropdown-user -->
        </li>
        <!-- /.dropdown -->
      </ul>
    </div>
    <!-- /.navbar-header -->
    <!-- /.navbar-top-links -->
    <!-- /.navbar-static-side -->
  </nav>
  <div class="navbar-default sidebar nicescroll" role="navigation">
    <div class="sidebar-nav navbar-collapse ">
      <ul class="nav" id="side-menu">
        <li class="sidebar-search hidden-sm hidden-md hidden-lg">
          <div class="input-group custom-search-form">
            <input type="text" class="form-control" placeholder="Search...">
            <span class="input-group-btn">
            <button class="btn btn-default" type="button"> <i class="fa fa-search"></i> </button>
            </span> </div>
          <!-- /input-group -->
        </li>
        <li class="nav-small-cap">Main Menu</li>
        <li> <a href="assets/index-2.html" class="waves-effect"><i class="glyphicon glyphicon-fire fa-fw"></i> Dashboard <span class="fa arrow"></span></a>
          <ul class="nav nav-second-level">
            <li> <a href="assets/index-2.html">Dashboard 1</a> </li>
            <li> <a href="assets/index2.html">Dashboard 2</a> </li>
            <li> <a href="assets/index3.html">Dashboard 3</a> </li>
          </ul>
        </li>
        <li> <a href="#" class="waves-effect"><i class="ti-magnet fa-fw"></i> UI Elements<span class="fa arrow"></span> <span class="label label-rouded label-purple pull-right">13</span> </a>
          <ul class="nav nav-second-level">
            <li><a href="assets/panels-wells.html">Panels and Wells</a></li>
            <li><a href="assets/buttons.html">Buttons</a></li>
            <li><a href="assets/notifications.html">Notifications</a></li>
            <li><a href="assets/typography.html">Typography</a></li>
            <li><a href="assets/grid.html">Grid</a></li>
            <li><a href="assets/tabs.html">Tabs</a></li>
            <li><a href="assets/modals.html">Modals</a></li>
            <li><a href="assets/progressbars.html">Progress Bars</a></li>
            <li><a href="assets/notification.html">Notification 2</a></li>
            <li><a href="assets/carousel.html">Carousel</a></li>
            <li><a href="assets/timeline.html">Timeline</a></li>
            <li><a href="assets/nestable.html">Nesteble</a></li>
            <li><a href="assets/bootstrap.html">Other</a></li>
          </ul>
          <!-- /.nav-second-level -->
        </li>
        <li> <a href="#" class="waves-effect"><i class="ti-pie-chart fa-fw"></i> Charts<span class="fa arrow"></span></a>
          <ul class="nav nav-second-level">
            <li> <a href="assets/flot.html">Flot Charts</a> </li>
            <li><a href="assets/morris-chart.html">Morris Chart</a></li>
<li><a href="assets/chart-js.html">Chart-js</a></li>
            <li><a href="assets/peity-chart.html">Peity Charts</a></li>
            <li><a href="assets/sparkline-chart.html">Sparkline charts</a></li>
            <li><a href="assets/extra-charts.html">Extra Charts</a></li>
          </ul>
          <!-- /.nav-second-level -->
        </li>
        <li> <a href="assets/tables.html" class="waves-effect active"><i class="ti-layout fa-fw"></i> Tables<span class="fa arrow"></span></a>
          <ul class="nav nav-second-level">
            <li><a href="assets/basic-table.html">Basic Tables</a></li>
            <li><a href="assets/data-table.html">Data Table</a></li>
            <li><a href="assets/bootstrap-tables.html">Bootstrap Tables</a></li>
          </ul>
        </li>
        <li> <a href="assets/forms.html" class="waves-effect"><i class="ti-pencil-alt fa-fw"></i> Forms<span class="fa arrow"></span></a>
          <ul class="nav nav-second-level">
            <li><a href="assets/form-elements.html">Basic Forms</a></li>
<li><a href="assets/advanced-form.html">Form Addons</a></li>
<li><a href="assets/form-material-elements.html">Form Material</a></li>
<li><a href="assets/form-upload.html">File Upload</a></li>
<li><a href="assets/form-mask.html">Form Mask</a></li>
<li><a href="assets/form-validation.html">Form Validation</a></li>
<li><a href="assets/wysiwig.html">Wysiwig Editors</a></li>
            <li><a href="assets/form-dropzone.html">File Dropzone</a></li>
            <li><a href="assets/form-xeditable.html">X-editable</a></li>
          </ul>
        </li>
        <li> <a href="assets/widgets.html" class="waves-effect"><i class="ti-widget fa-fw"></i> Widgets</a> </li>
        <li> <a href="#" class="waves-effect"><i class="ti-face-smile fa-fw"></i> Icons<span class="fa arrow"></span></a>
          <ul class="nav nav-second-level">
            <li> <a href="assets/fontawesome.html">Font awesome</a> </li>
            <li> <a href="assets/themifyicon.html">Themify Icons</a> </li>
            <li> <a href="assets/simple-line.html">Simple line Icons</a> </li>
            <li> <a href="assets/weather.html">Weather Icons</a> </li>
          </ul>
          <!-- /.nav-second-level -->
        </li>
        <li> <a href="assets/map-google.html" class="waves-effect"><i class="ti-location-pin fa-fw"></i> Google Map</a> </li>
        <li> <a href="assets/map-vector.html" class="waves-effect"><i class="ti-ink-pen fa-fw"></i> Vector Map</a> </li>
        <li> <a href="#" class="waves-effect"><i class="ti-files fa-fw"></i> Sample Pages<span class="fa arrow"></span></a>
          <ul class="nav nav-second-level">
            <li><a href="assets/blank.html">Blank Page</a></li>
            <li><a href="assets/login.html">Login Page</a></li>
<li><a href="assets/login2.html">Login Page 2</a></li>
<li><a href="assets/profile.html">Profile</a></li>
            <li><a href="assets/invoice.html">Invoice</a></li>
            <li><a href="assets/faq.html">FAQ</a></li>
            <li><a href="assets/gallery.html">Gallery</a></li>
            <li><a href="assets/pricing.html">Pricing</a></li>
            <li><a href="assets/register.html">Register</a></li>
            <li><a href="assets/recoverpw.html">Recover Password</a></li>
            <li><a href="assets/lock-screen.html">Lock Screen</a></li>
            <li><a href="assets/400.html">Error 400</a></li>
            <li><a href="assets/403.html">Error 403</a></li>
            <li><a href="assets/404.html">Error 404</a></li>
            <li><a href="assets/500.html">Error 500</a></li>
            <li><a href="assets/503.html">Error 503</a></li>
          </ul>
          <!-- /.nav-second-level -->
        </li>
        <li> <a href="assets/inbox.html" class="waves-effect"><i class="ti-email fa-fw"></i> Inbox</a> </li>
        <li> <a href="assets/calendar.html" class="waves-effect"><i class="ti-calendar fa-fw"></i> Calender</a> </li>
        <li> <a href="#" class="waves-effect"><i class="ti-share fa-fw"></i> Multi-Level Dropdown<span class="fa arrow"></span></a>
          <ul class="nav nav-second-level">
            <li> <a href="#">Second Level Item</a> </li>
            <li> <a href="#">Second Level Item</a> </li>
            <li> <a href="#" class="waves-effect">Third Level <span class="fa arrow"></span></a>
              <ul class="nav nav-third-level">
                <li> <a href="#">Third Level Item</a> </li>
                <li> <a href="#">Third Level Item</a> </li>
                <li> <a href="#">Third Level Item</a> </li>
                <li> <a href="#">Third Level Item</a> </li>
              </ul>
              <!-- /.nav-third-level -->
            </li>
          </ul>
          <!-- /.nav-second-level -->
        </li>
      </ul>
    </div>
    <!-- /.sidebar-collapse -->
  </div>
  <!-- Page Content -->
  <div id="page-wrapper">
    <div class="container-fluid">
      <div class="row bg-title">
        <div class="col-lg-12">
          <h4 class="page-title">Basic Tables</h4>
          <ol class="breadcrumb">
            <li><a href="#">Dashboard</a></li>
            <li><a href="#">Tables</a></li>
            <li class="active">Basic Table</li>
          </ol>
        </div>
        <!-- /.col-lg-12 -->
      </div>
      <!-- row -->
      <div class="row">
        <div class="col-sm-6">
          <div class="white-box">
            <h3>Basic Table</h3>
            <p class="text-muted">Add class <code>.table</code></p>
            <div class="table-responsive">
              <table class="table">
                <thead>
                  <tr>
                    <th>#</th>
                    <th>First Name</th>
                    <th>Last Name</th>
                    <th>Username</th>
                    <th>Role</th>
                  </tr>
                </thead>
                <tbody>
                  <tr>
                    <td>1</td>
                    <td>Deshmukh</td>
                    <td>Prohaska</td>
                    <td>@Genelia</td>
                    <td><span class="label label-danger">admin</span> </td>
                  </tr>
                  <tr>
                    <td>2</td>
                    <td>Deshmukh</td>
                    <td>Gaylord</td>
                    <td>@Ritesh</td>
                    <td><span class="label label-info">member</span> </td>
                  </tr>
                  <tr>
                    <td>3</td>
                    <td>Sanghani</td>
                    <td>Gusikowski</td>
                    <td>@Govinda</td>
                    <td><span class="label label-warning">developer</span> </td>
                  </tr>
                  <tr>
                    <td>4</td>
                    <td>Roshan</td>
                    <td>Rogahn</td>
                    <td>@Hritik</td>
                    <td><span class="label label-success">supporter</span> </td>
                  </tr>
                  <tr>
                    <td>5</td>
                    <td>Joshi</td>
                    <td>Hickle</td>
                    <td>@Maruti</td>
                    <td><span class="label label-info">member</span> </td>
                  </tr>
                  <tr>
                    <td>6</td>
                    <td>Nigam</td>
                    <td>Eichmann</td>
                    <td>@Sonu</td>
                    <td><span class="label label-success">supporter</span> </td>
                  </tr>
                </tbody>
              </table>
            </div>
          </div>
        </div>
        <div class="col-sm-6">
          <div class="white-box">
            <h3>Table Hover</h3>
            <p class="text-muted">Add class <code>.table-hover</code></p>
            <div class="table-responsive">
              <table class="table table-hover">
                <thead>
                  <tr>
                    <th>#</th>
                    <th>Products</th>
                    <th>Popularity</th>
                    <th>Sales</th>
                  </tr>
                </thead>
                <tbody>
                  <tr>
                    <td>1</td>
                    <td>Milk Powder</td>
                    <td><span class="peity-line" data-width="120" data-peity='{ "fill": ["#13dafe"], "stroke":["#13dafe"]}' data-height="40">0,-3,-2,-4,-5,-4,-3,-2,-5,-1</span> </td>
                    <td><span class="text-danger text-semibold"><i class="fa fa-level-down" aria-hidden="true"></i> 28.76%</span> </td>
                  </tr>
                  <tr>
                    <td>2</td>
                    <td>Air Conditioner</td>
                    <td><span class="peity-line" data-width="120" data-peity='{ "fill": ["#13dafe"], "stroke":["#13dafe"]}' data-height="40">0,-1,-1,-2,-3,-1,-2,-3,-1,-2</span> </td>
                    <td><span class="text-warning text-semibold"><i class="fa fa-level-down" aria-hidden="true"></i> 8.55%</span> </td>
                  </tr>
                  <tr>
                    <td>3</td>
                    <td>RC Cars</td>
                    <td><span class="peity-line" data-width="120" data-peity='{ "fill": ["#13dafe"], "stroke":["#13dafe"]}' data-height="40">0,3,6,1,2,4,6,3,2,1</span> </td>
                    <td><span class="text-success text-semibold"><i class="fa fa-level-up" aria-hidden="true"></i> 58.56%</span> </td>
                  </tr>
                  <tr>
                    <td>4</td>
                    <td>Down Coat</td>
                    <td><span class="peity-line" data-width="120" data-peity='{ "fill": ["#13dafe"], "stroke":["#13dafe"]}' data-height="40">0,3,6,4,5,4,7,3,4,2</span> </td>
                    <td><span class="text-info text-semibold"><i class="fa fa-level-up" aria-hidden="true"></i> 35.76%</span> </td>
                  </tr>
                </tbody>
              </table>
            </div>
          </div>
        </div>
      </div>
      <div class="row">
        <div class="col-sm-12">
          <div class="white-box">
            <h3>Bordered Table</h3>
            <p class="text-muted m-b-20">Add<code>.table-bordered</code>for borders on all sides of the
              table and cells.</p>
            <div class="table-responsive">
              <table class="table table-bordered">
                <thead>
                  <tr>
                    <th>Task</th>
                    <th>Progress</th>
                    <th>Deadline</th>
                    <th class="text-nowrap">Action</th>
                  </tr>
                </thead>
                <tbody>
                  <tr>
                    <td>Lunar probe project</td>
                    <td><div class="progress progress-xs margin-vertical-10 ">
                        <div class="progress-bar progress-bar-danger" style="width: 35%"></div>
                      </div></td>
                    <td>May 15, 2015</td>
                    <td class="text-nowrap"><a href="#" data-toggle="tooltip" data-original-title="Edit"> <i class="fa fa-pencil text-inverse m-r-10"></i> </a> <a href="#" data-toggle="tooltip" data-original-title="Close"> <i class="fa fa-close text-danger"></i> </a> </td>
                  </tr>
                  <tr>
                    <td>Dream successful plan</td>
                    <td><div class="progress progress-xs margin-vertical-10 ">
                        <div class="progress-bar progress-bar-warning" style="width: 50%"></div>
                      </div></td>
                    <td>July 1, 2015</td>
                    <td class="text-nowrap"><a href="#" data-toggle="tooltip" data-original-title="Edit"> <i class="fa fa-pencil text-inverse m-r-10"></i> </a> <a href="#" data-toggle="tooltip" data-original-title="Close"> <i class="fa fa-close text-danger"></i> </a> </td>
                  </tr>
                  <tr>
                    <td>Office automatization</td>
                    <td><div class="progress progress-xs margin-vertical-10 ">
                        <div class="progress-bar progress-bar-success" style="width: 100%"></div>
                      </div></td>
                    <td>Apr 12, 2015</td>
                    <td class="text-nowrap"><a href="#" data-toggle="tooltip" data-original-title="Edit"> <i class="fa fa-pencil text-inverse m-r-10"></i> </a> <a href="#" data-toggle="tooltip" data-original-title="Close"> <i class="fa fa-close text-danger"></i> </a> </td>
                  </tr>
                  <tr>
                    <td>The sun climbing plan</td>
                    <td><div class="progress progress-xs margin-vertical-10 ">
                        <div class="progress-bar progress-bar-primary" style="width: 70%"></div>
                      </div></td>
                    <td>Aug 9, 2015</td>
                    <td class="text-nowrap"><a href="#" data-toggle="tooltip" data-original-title="Edit"> <i class="fa fa-pencil text-inverse m-r-10"></i> </a> <a href="#" data-toggle="tooltip" data-original-title="Close"> <i class="fa fa-close text-danger"></i> </a> </td>
                  </tr>
                  <tr>
                    <td>Open strategy</td>
                    <td><div class="progress progress-xs margin-vertical-10 ">
                        <div class="progress-bar progress-bar-primary" style="width: 85%"></div>
                      </div></td>
                    <td>Apr 2, 2015</td>
                    <td class="text-nowrap"><a href="#" data-toggle="tooltip" data-original-title="Edit"> <i class="fa fa-pencil text-inverse m-r-10"></i> </a> <a href="#" data-toggle="tooltip" data-original-title="Close"> <i class="fa fa-close text-danger"></i> </a> </td>
                  </tr>
                  <tr>
                    <td>Tantas earum numeris</td>
                    <td><div class="progress progress-xs margin-vertical-10 ">
                        <div class="progress-bar progress-bar-warning" style="width: 50%"></div>
                      </div></td>
                    <td>July 11, 2015</td>
                    <td class="text-nowrap"><a href="#" data-toggle="tooltip" data-original-title="Edit"> <i class="fa fa-pencil text-inverse m-r-10"></i> </a> <a href="#" data-toggle="tooltip" data-original-title="Close"> <i class="fa fa-close text-danger"></i> </a> </td>
                  </tr>
                </tbody>
              </table>
            </div>
          </div>
        </div>
      </div>
      <div class="row">
        <div class="col-sm-12">
          <div class="white-box">
            <h3>Striped Table</h3>
            <p class="text-muted m-b-20">Add<code>.table-striped</code>for borders on all sides of the
              table and cells.</p>
            <div class="table-responsive">
              <table class="table table-striped">
                <thead>
                  <tr>
                    <th>Task</th>
                    <th>Progress</th>
                    <th>Deadline</th>
                    <th class="text-nowrap">Action</th>
                  </tr>
                </thead>
                <tbody>
                  <tr>
                    <td>Lunar probe project</td>
                    <td><div class="progress progress-xs margin-vertical-10 ">
                        <div class="progress-bar progress-bar-danger" style="width: 35%"></div>
                      </div></td>
                    <td>May 15, 2015</td>
                    <td class="text-nowrap"><a href="#" data-toggle="tooltip" data-original-title="Edit"> <i class="fa fa-pencil text-inverse m-r-10"></i> </a> <a href="#" data-toggle="tooltip" data-original-title="Close"> <i class="fa fa-close text-danger"></i> </a> </td>
                  </tr>
                  <tr>
                    <td>Dream successful plan</td>
                    <td><div class="progress progress-xs margin-vertical-10 ">
                        <div class="progress-bar progress-bar-warning" style="width: 50%"></div>
                      </div></td>
                    <td>July 1, 2015</td>
                    <td class="text-nowrap"><a href="#" data-toggle="tooltip" data-original-title="Edit"> <i class="fa fa-pencil text-inverse m-r-10"></i> </a> <a href="#" data-toggle="tooltip" data-original-title="Close"> <i class="fa fa-close text-danger"></i> </a> </td>
                  </tr>
                  <tr>
                    <td>Office automatization</td>
                    <td><div class="progress progress-xs margin-vertical-10 ">
                        <div class="progress-bar progress-bar-success" style="width: 100%"></div>
                      </div></td>
                    <td>Apr 12, 2015</td>
                    <td class="text-nowrap"><a href="#" data-toggle="tooltip" data-original-title="Edit"> <i class="fa fa-pencil text-inverse m-r-10"></i> </a> <a href="#" data-toggle="tooltip" data-original-title="Close"> <i class="fa fa-close text-danger"></i> </a> </td>
                  </tr>
                  <tr>
                    <td>The sun climbing plan</td>
                    <td><div class="progress progress-xs margin-vertical-10 ">
                        <div class="progress-bar progress-bar-primary" style="width: 70%"></div>
                      </div></td>
                    <td>Aug 9, 2015</td>
                    <td class="text-nowrap"><a href="#" data-toggle="tooltip" data-original-title="Edit"> <i class="fa fa-pencil text-inverse m-r-10"></i> </a> <a href="#" data-toggle="tooltip" data-original-title="Close"> <i class="fa fa-close text-danger"></i> </a> </td>
                  </tr>
                  <tr>
                    <td>Open strategy</td>
                    <td><div class="progress progress-xs margin-vertical-10 ">
                        <div class="progress-bar progress-bar-primary" style="width: 85%"></div>
                      </div></td>
                    <td>Apr 2, 2015</td>
                    <td class="text-nowrap"><a href="#" data-toggle="tooltip" data-original-title="Edit"> <i class="fa fa-pencil text-inverse m-r-10"></i> </a> <a href="#" data-toggle="tooltip" data-original-title="Close"> <i class="fa fa-close text-danger"></i> </a> </td>
                  </tr>
                  <tr>
                    <td>Tantas earum numeris</td>
                    <td><div class="progress progress-xs margin-vertical-10 ">
                        <div class="progress-bar progress-bar-warning" style="width: 50%"></div>
                      </div></td>
                    <td>July 11, 2015</td>
                    <td class="text-nowrap"><a href="#" data-toggle="tooltip" data-original-title="Edit"> <i class="fa fa-pencil text-inverse m-r-10"></i> </a> <a href="#" data-toggle="tooltip" data-original-title="Close"> <i class="fa fa-close text-danger"></i> </a> </td>
                  </tr>
                </tbody>
              </table>
            </div>
          </div>
        </div>
      </div>
      <!-- /.row -->
    </div>
    <!-- /.container-fluid -->
  </div>
  <!-- /#page-wrapper -->
    <footer class="footer text-center"> 2016 &copy; Myadmin brought to you by themedesigner.in </footer>
</div>
<!-- /#wrapper -->
<!-- jQuery -->
<script src="assets/bower_components/jquery/dist/jquery.min.js"></script>
<!-- Bootstrap Core JavaScript -->
<script src="assets/bower_components/bootstrap/dist/js/bootstrap.min.js"></script>
<!-- Menu Plugin JavaScript -->
<script src="assets/bower_components/metisMenu/dist/metisMenu.min.js"></script>
<!--Nice scroll JavaScript -->
<script src="assets/js/jquery.nicescroll.js"></script>
<!-- jQuery peity -->
<script src="assets/bower_components/peity/jquery.peity.min.js"></script>
<script src="assets/bower_components/peity/jquery.peity.init.js"></script>
<!--Wave Effects -->
<script src="assets/js/waves.js"></script>
<!-- Custom Theme JavaScript -->
<script src="assets/js/myadmin.js"></script>
</body>

<!-- Mirrored from themedesigner.in/demo/myadmin/myadmin-dark/basic-table.html by HTTrack Website Copier/3.x [XR&CO'2014], Fri, 24 Jun 2016 17:37:59 GMT -->
</html>
