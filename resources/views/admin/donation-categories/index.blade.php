@extends('templates.admin.layout', ['page_title' => $data['page_title']])
@section('content')
  <!-- Page Content -->
  <div id="page-wrapper">
    <div class="container-fluid">
      <div class="row bg-title">
        <div class="col-lg-12">
          <h4 class="page-title">Donation Categories</h4>
          <ol class="breadcrumb">
            <li><a href="">Dashboard</a></li>
            <li class="active">Donation Categories</li>
          </ol>
        </div>
        <!-- /.col-lg-12 -->
      </div>

      <!-- row -->
      <div class="row">
        <div class="col-md-12 col-lg-12 col-sm-12 col-xs-12">
          <div class="white-box">
            <!--<h3>Users</h3>-->
            <!-- Nav tabs -->
            <ul class="nav nav-tabs" role="tablist">
              <li role="presentation" class="active"><a href="#all" aria-controls="all" role="tab" data-toggle="tab"><span class="visible-xs"><i class="ti-view-list"></i></span><span class="hidden-xs"> View All</span></a></li>
              <li role="presentation"><a href="#add" aria-controls="add" role="tab" data-toggle="tab"><span class="visible-xs"><i class="ti-plus"></i></span> <span class="hidden-xs">Add New</span></a></li>
            </ul>

            <!-- Tab panes -->
            <div class="tab-content">
              <div role="tabpanel" class="tab-pane active" id="all"> 
                <div class="col-md-12">
                  <div class="table-responsive">
                    <table class="table table-bordered">
                      <thead>
                        <tr>
                          <th>#</th>
                          <th>Name</th>
                          <th>Description</th>
                          <th class="text-nowrap">Actions</th>
                        </tr>
                      </thead>
                      <tbody>
                      @if($data['rows']->isEmpty())
                        <tr>
                          <td colspan="4" align="center">No data returned.</td>
                        </tr>
                      @else
                      <?php $sn = 1; ?>
                      @foreach ($data['rows'] as $row)
                        <tr>
                          <td><?php echo $sn++; ?></td>
                          <td>{{ $row['name'] }}</td>
                          <td>{{ $row['description'] == NULL ? "-" : $row['description'] }}</td>
                          <td class="text-nowrap">
                            <form action="donation-categories/{{ $row['id'] }}" method="post" onsubmit="return confirm('Are you sure you want to delete this record?');">
                              {{ csrf_field() }}
                              <input type="hidden" name="_method" value="DELETE">
                              <button type="button" class="btn btn-primary btn-circle btn-xs" data-toggle="tooltip" data-original-title="Edit" onclick="window.location='donation-categories/{{ $row['id'] }}/edit'"><i class="fa fa-pencil"></i></button> 
                              @if($row['can_be_deleted'] == 1)
                              <button type="submit" class="btn btn-danger btn-circle btn-xs" data-toggle="tooltip" data-original-title="Delete"><i class="fa fa-trash-o"></i></button> 
                              @endif
                            </form>
                          </td>
                        </tr>
                      @endforeach
                      @endif
                      </tbody>
                    </table>
                  </div>
                </div>
                
                <div class="col-md-3 pull-right">
                  <p><a href="?page=2">Pagination</a>.</p>
                </div>
                
                <div class="clearfix"></div>
              </div>
              <div role="tabpanel" class="tab-pane" id="add">
                <div class="col-md-12">
                <form action="donation-categories" method="post" class="form-horizontal">
                  {{ csrf_field() }}
                  <div class="form-group">
                    <label for="name" class="col-sm-2 control-label">Name: <span class="text-danger">*</span></label>
                    <div class="col-sm-10">
                      <input type="text" class="form-control" name="name" maxlength="255" id="name" value="{{ old('name') }}" placeholder="" required="required">
                    </div>
                  </div>
                  <div class="form-group">
                    <label for="description" class="col-sm-2 control-label">Description:</label>
                    <div class="col-sm-10">
                      <textarea class="form-control" name="description" id="description" maxlength="8000">{{ old('description') }}</textarea>
                    </div>
                  </div>
                  <div class="form-group m-b-0">
                    <div class="col-sm-offset-3 col-sm-9">
                      <button type="submit" class="btn btn-info waves-effect waves-light">Add New</button>
                    </div>
                  </div>
                </form>
                </div>
                <div class="clearfix"></div>
              </div>
            </div>
          </div>
        </div>
      </div> 
      <!-- /.row -->
    </div>
    <!-- /.container-fluid -->
  </div>
  <!-- /#page-wrapper -->
@stop