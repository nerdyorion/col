@extends('templates.admin.layout', ['page_title' => $data['page_title']])
@section('content')
  <!-- Page Content -->
  <div id="page-wrapper">
    <div class="container-fluid">
      <div class="row bg-title">
        <div class="col-lg-12">
          <h4 class="page-title">Dashboard</h4>
          <!--
          <ol class="breadcrumb">
            <li><a href="#">Dashboard</a></li>
            <li class="active">Dashboard 1</li>
          </ol>
          -->
        </div>
        <!-- /.col-lg-12 -->
      </div>
      <!-- /.row -->
      <div class="row">
        <div class="col-md-8 col-lg-9 col-sm-12">
          <div class="white-box">
            <div class="row row-in">
              <div class="col-lg-3 col-sm-6">
                <div class="col-in text-center">
                  <h5 class="text-danger"><strong>SMS Units</strong></h5>
                  <h3 class="counter" style="word-wrap: break-word; line-height: 1em;"><?php echo number_format(0); ?></h3>
                </div>
              </div>
              <div class="col-lg-3 col-sm-6">
                <div class="col-in text-center b-r-none">
                  <h5 class="text-muted text-warning"><strong>Failed Subs. (Total)</strong></h5>
                  <h3 class="counter" style="word-wrap: break-word; line-height: 1em;"><?php echo number_format(0); ?></h3>
                </div>
              </div>
              <div class="col-lg-3 col-sm-6">
                <div class="col-in text-center">
                  <h5 class="text-muted text-purple"><strong>Donations (Today)</strong></h5>
                  <h3 class="counter" style="word-wrap: break-word; line-height: 1em;"><?php echo number_format(0); ?></h3>
                </div>
              </div>
              <div class="col-lg-3 col-sm-6">
                <div class="col-in text-center b-0">
                  <h5 class="text-info"><strong>Donations (Total)</strong></h5>
                  <h3 class="counter" style="word-wrap: break-word; line-height: 1em;"><?php echo number_format(0); ?></h3>
                </div>
              </div>
            </div>
          </div>
        </div>
        <div class="col-md-4 col-lg-3 col-sm-6 ">
          <div class="bg-purple m-b-20">
            <div id="myCarousel" class="carousel vcarousel slide vertical p-20">
              <!-- Carousel items -->
              <div class="carousel-inner ">
                <div class="active item"> <i class="fa fa-map-marker fa-2x text-white"></i>
                  <p class="text-white">Jul 24, 2017 09:43 AM</p>
                  <!-- <p class="text-white"><?php // echo $last_login == "0000-00-00 00:00:00" ? "-" : date('M d, Y h:i A', strtotime($last_login )); ?></p> -->
                  <h4 class="text-white">Last <span class="font-bold">Login</span><br />&nbsp;</h4>
                </div>
              </div>
            </div>
          </div>
        </div>
        
        <!-- <script id="bw-widget-src" src="//bibles.org/widget/client"></script>
        <script>
          // BIBLESEARCH.widget({
          //   "versions": "eng-KJVA"
          // });
        </script> -->

      </div>
      <!--row -->
    </div>
    <!-- /.container-fluid -->
  </div>
  <!-- /#page-wrapper -->
  @stop
