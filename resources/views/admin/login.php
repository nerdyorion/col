<!DOCTYPE html>
<html>

<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <base href="<?php echo base_url();?>">
  <link rel="icon" type="image/png" sizes="16x16" href="assets/images/favicon.ico">
  <title><?php echo $page_title; ?> | <?php echo $this->config->item('app_name'); ?></title>
  <!-- Bootstrap CSS -->
  <link href="assets/bower_components/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">
  <link href="assets/css/login.css" rel="stylesheet" type="text/css" />
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.6.3/css/font-awesome.min.css" />
  <!-- HTML5 Shiv and Respond.js IE8 support -->
  <!--[if lt IE 9]>
  <script src="assets/https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
  <script src="assets/https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
  <![endif]-->
</head>
<!-- <body style="background: url(assets/images/login-bg.jpg) no-repeat center center fixed;"> -->
<body style="background: url(assets/images/login-bg3.png) repeat;">
  <section id="wrapper" class="login-register">
    <div class="container">

      <div class="row" id="pwd-container">
        <div class="col-md-4"></div>

        <div class="col-md-4">
          <section class="login-form">
            <?php echo form_open('admin123/login', 'role="login"'); ?>
            <!--<img src="http://i.imgur.com/RcmcLv4.png" class="img-responsive" alt="" />-->
            <!-- <h3 class="text-center"><i class="fa fa-tags fa-rotate-90"></i> <?php echo $this->config->item('app_name'); ?></h3> -->
            <h3 class="text-center"><img src="assets/images/logo-white.png" /></h3>
            <input type="email" name="email" placeholder="Email" maxlength="255" required="required" class="form-control input-lg" value="" />

            <input type="password" class="form-control input-lg" id="password" name="password" placeholder="Password" required="required" />  

            <div id="success-msg" style="display:none" class="alert alert-success alert-dismissible" role="alert"></div>
            <div id="error-msg" style="display:none" class="alert alert-danger alert-dismissible" role="alert"></div>        

            <button type="submit" name="submit" class="btn btn-lg btn-primary btn-block">Sign in</button>
            <div>
              <a href="<?php echo base_url();?>">&larr; Store Front</a>
            </div>

          </form>

          <div class="form-links">
            Powered by <a href="http://hollatags.com" target="_blank">HollaTags</a>
          </div>
        </section>  
      </div>

      <div class="col-md-4"></div>


    </div>

    <p>
      <br>
      <br>

    </p>     


  </div>
</section>
<!-- jQuery -->
<script src="assets/bower_components/jquery/dist/jquery.min.js"></script>
<!-- Bootstrap Core JavaScript -->
<script src="assets/bower_components/bootstrap/dist/js/bootstrap.min.js"></script>
<script type="text/javascript">
  $(document).ready(function() {
    <?php if(!empty($error_code)): ?>
      //alert("1" + '<?php echo $error; ?>');
      $("#error-msg").fadeToggle(350);
      $("#error-msg").html('<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button> <?php echo $error; ?>');
    <?php elseif((empty($error_code)) && (!empty($error))): ?>
      //alert("0" + <?php echo $error; ?>);
      $("#success-msg").fadeToggle(350);
      $("#success-msg").html('<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button> <?php echo $error; ?>');
    <?php else: ?>
  <?php endif; ?>
});
</script>
</body>

</html>
