  <!-- Page Content -->
  <div id="page-wrapper">
    <div class="container-fluid">
      <div class="row bg-title">
        <div class="col-lg-12">
          <h4 class="page-title">DND Number Details</h4>
          <ol class="breadcrumb">
            <li><a href="./">Dashboard</a></li>
            <li><a href="DND-Numbers">DND Numbers</a></li>
            <li class="active">Details</li>
          </ol>
        </div>
        <!-- /.col-lg-12 -->
      </div>

      <!-- row -->
      <div class="row">
        <div class="col-md-12">
          <div class="white-box">
            <div class="table-responsive">
              <table class="table table-bordered table-striped">
                <tbody>
                  <tr>
                    <th>Network</th>
                    <td><?php echo $row['network']; ?></td>
                  </tr>
                  <tr>
                    <th>MSISDN</th>
                    <td><?php echo $row['msisdn']; ?></td>
                  </tr>
                  <tr>
                    <th>Blocking Mode</th>
                    <td><?php echo $row['blocking_mode']; ?></td>
                  </tr>
                  <tr>
                    <th>Operate Date</th>
                    <td><?php echo date('M d, Y h:i A', strtotime($row['operate_date'])); ?></td>
                  </tr>
                  <tr>
                    <th>Banking Financial Services</th>
                    <td><?php echo $row['banking_financial_services']; ?></td>
                  </tr>
                  <tr>
                    <th>Real Estate</th>
                    <td><?php echo $row['real_estate']; ?></td>
                  </tr>
                  <tr>
                    <th>Health</th>
                    <td><?php echo $row['health']; ?></td>
                  </tr>
                  <tr>
                    <th>Education</th>
                    <td><?php echo $row['education']; ?></td>
                  </tr>
                  <tr>
                    <th>Consumer Goods</th>
                    <td><?php echo $row['consumer_goods']; ?></td>
                  </tr>
                  <tr>
                    <th>Broadcasting</th>
                    <td><?php echo $row['broadcasting']; ?></td>
                  </tr>
                  <tr>
                    <th>Tourism Leisure</th>
                    <td><?php echo $row['tourism_leisure']; ?></td>
                  </tr>
                  <tr>
                    <th>Sports</th>
                    <td><?php echo $row['sports']; ?></td>
                  </tr>
                  <tr>
                    <th>Religion</th>
                    <td><?php echo $row['religion']; ?></td>
                  </tr>
                  <tr>
                    <th>Subscription</th>
                    <td><?php echo $row['subscription']; ?></td>
                  </tr>
                  <tr>
                    <th>Broadcast</th>
                    <td><?php echo $row['broadcast']; ?></td>
                  </tr>
                  <tr>
                    <th>Bulk SMS</th>
                    <td><?php echo $row['bulk_sms']; ?></td>
                  </tr>
                  <tr>
                    <th>Bank Alerts</th>
                    <td><?php echo $row['bank_alerts']; ?></td>
                  </tr>
                  <tr>
                    <th>MTN Campaigns</th>
                    <td><?php echo $row['mtn_campaigns']; ?></td>
                  </tr>
                </tbody>
              </table>
            </div>
            <p class="text-center">
              <button type="button" class="btn btn-info waves-effect waves-light center" onclick="history.go(-1); return false;">&larr; Back</button>
            </p>
          </div>
        </div>
      </div>
      <!-- /.row -->
    </div>
    <!-- /.container-fluid -->
  </div>
  <!-- /#page-wrapper -->

<?php $this->load->view($this->config->item('template_dir_admin') . 'footer', array('error', $error, 'error_code', $error_code)); echo "\n";  // load footer view ?>
<script src="assets/js/jasny-bootstrap.js"></script>

</body>
</html>
