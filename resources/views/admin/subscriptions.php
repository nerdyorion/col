<!-- Custom CSS -->
<link href="assets/bower_components/bootstrap-datepicker/bootstrap-datepicker.min.css" rel="stylesheet" type="text/css" />
<link href="assets/bower_components/custom-select/custom-select.css" rel="stylesheet" type="text/css" />
<link href="assets/bower_components/bootstrap-select/bootstrap-select.min.css" rel="stylesheet" />

<style type="text/css">
  .comment {
    border: none;
  }
  a.morelink {
    text-decoration:none;
    outline: none;
    /*color: inherit;*/
    color: #337ab7;
  }
  .morecontent span {
    display: none;
    text-decoration:none;
    outline: none;
    color: #797979;
  }
  .moreellipses
  {
    outline: none;
  }
</style>
<!-- Page Content -->
  <div id="page-wrapper">
    <div class="container-fluid">
      <div class="row bg-title">
        <div class="col-lg-12">
          <h4 class="page-title">Subscriptions</h4>
          <ol class="breadcrumb">
            <li><a href="./">Dashboard</a></li>
            <li class="active">Subscriptions</li>
          </ol>
        </div>
        <!-- /.col-lg-12 -->
      </div>
      <!-- row -->
      <div class="row">
        <div class="col-sm-12">
          <div class="white-box">
            <div class="row">
              <div class="col-md-12">
                <!-- <?php echo form_open(current_url() . '/filter', 'class="form-horizontal"'); ?> -->
                <form action="<?php echo current_url(); ?>" class="form-horizontal" method="get">
                  <div class="form-group">
                    <label for="network_id" class="col-sm-2 control-label">Product(s):</label>
                    <div class="col-sm-10">
                      <select class="selectpicker" multiple data-style="btn-white" data-width="100%" name="products[]">
                        <?php foreach ($products as $product): ?>  <!-- selected="selected" -->
                          <option value="<?php echo $product['id']; ?>" <?php echo set_select('products', $product['id']); ?>><?php echo $product['name']; ?></option>
                        <?php endforeach; ?>
                      </select>

                    </div>
                  </div>
                  <div class="form-group">
                    <label class="col-sm-2 control-label">Date Range</label>
                    <div class="col-sm-10">
                      <div class="input-daterange input-group" id="date-range">
                        <input type="text" class="form-control" name="start" value="<?php echo set_value('start'); ?>" />
                        <span class="input-group-addon bg-info b-0 text-white">to</span>
                        <input type="text" class="form-control" name="end" value="<?php echo set_value('end'); ?>" />
                      </div>
                    </div>
                  </div>
                  <div class="form-group">
                    <label for="msisdn" class="col-sm-2 control-label">MSISDN:</label>
                    <div class="col-sm-10">
                      <input type="text" class="form-control" name="msisdn" maxlength="13" id="msisdn" value="<?php echo set_value('msisdn'); ?>" placeholder="">
                    </div>
                  </div>
                  <div class="form-group">
                    <label for="successful" class="col-sm-2 control-label">Successful?</label>
                    <div class="col-sm-10">
                      <select class="form-control" name="successful" id="successful"">
                        <option value="-1" <?php echo set_select('successful', '-1', TRUE); ?>>All</option>
                        <option value="1" <?php echo set_select('successful', '1'); ?>>Yes</option>
                        <option value="0" <?php echo set_select('successful', '0'); ?>>No</option>
                      </select>
                    </div>
                  </div>
                  <div class="form-group m-b-0">
                    <div class="col-sm-offset-3 col-sm-9">
                      <button type="submit" class="btn btn-info waves-effect waves-light">Search</button>
                    </div>
                  </div>
                </form>
              </div>
            </div>
          </div>
        </div>
        
      </div>
      <!-- row -->
      <div class="row">
        <div class="col-sm-12">
          <div class="white-box">
            <h3>Subscriptions</h3>
            <!--<p class="text-muted m-b-20">Lorem<code>ipsum.table-bordered</code>.</p>-->
            <div class="table-responsive">
              <table class="table table-bordered">
                <thead>
                  <tr>
                    <th>#</th>
                    <th>Product</th>
                    <th>MSISDN</th>
                    <th>Device</th>
                    <th>Successful?</th>
                    <th>Date</th>
                  </tr>
                </thead>
                <tbody>
                <?php if(empty($rows)): ?>
                  <tr>
                    <td colspan="5" align="center">No data returned.</td>
                  </tr>
                <?php else: ?>
                <?php foreach ($rows as $row): ?>
                  <tr>
                    <td><?php echo $sn++; ?></td>
                    <td><a href="admin123/Products/view/<?php echo $row['product_id']; ?>" data-toggle="tooltip" data-original-title="View <?php echo $row['name']; ?>"><?php echo $row['name']; ?></a></td>
                    <td class="comment more"><?php echo $row['phone']; ?></td>
                    <td class="comment more"><?php echo $row['via']; ?></td>
                    <td><?php echo $row['successful'] == 0 ? '<a href="javascript:void(0);" class="bg-danger" style="color: #ffffff;">&nbsp;No&nbsp;</a> ' . $row['error_reason'] : '<a href="javascript:void(0);" class="bg-success" style="color: #ffffff;">&nbsp;Yes&nbsp;</a>'; ?></td>
                    <td><?php echo date('M d, Y h:i A', strtotime($row['date_created'])); ?></td>
                  </tr>
                <?php endforeach; ?>
                <?php endif; ?>
                </tbody>
              </table>

              <div class="col-md-3 pull-right pagination">
                <p><?php echo $links; ?></p>
              </div>

              <div class="clearfix"></div>
            </div>
          </div>
        </div>
      </div>
    </div>
    <!-- /.container-fluid -->
  </div>
  <!-- /#page-wrapper -->

<?php $this->load->view($this->config->item('template_dir_admin') . 'footer', array('error', $error, 'error_code', $error_code)); echo "\n";  // load footer view ?>
<script src="assets/js/jasny-bootstrap.js"></script>
<!-- jQuery Color & Datepicker -->
<script src="assets/bower_components/bootstrap-datepicker/bootstrap-datepicker.min.js"></script>
<script src="assets/bower_components/custom-select/custom-select.min.js" type="text/javascript"></script>
<script src="assets/bower_components/bootstrap-select/bootstrap-select.min.js" type="text/javascript"></script>

<script type="text/javascript">
  $(document).ready(function() {
    var showChar = 14;
    var ellipsestext = "****";
    var moretext = ">>";
    var lesstext = "<<";
    $('.more').each(function() {
      var content = $(this).html();

      if(content.length > showChar) {

        var c = content.substr(0, showChar);
        var h = content.substr(showChar-1, content.length - showChar);

        var html = c + '<span class="moreellipses">' + ellipsestext+ '&nbsp;</span><span class="morecontent"><span>' + h + '</span>&nbsp;&nbsp;<a href="" class="morelink">' + moretext + '</a></span>';

        $(this).html(html);
      }

    });

    $(".morelink").click(function(){
      if($(this).hasClass("less")) {
        $(this).removeClass("less");
        $(this).html(moretext);
      } else {
        $(this).addClass("less");
        $(this).html(lesstext);
      }
      $(this).parent().prev().toggle();
      $(this).prev().toggle();
      return false;
    });

    // $(".select2").select2();
    $('.selectpicker').selectpicker();

    // Date Picker
    $('.mydatepicker, #datepicker2').datepicker();
    $('#datepicker-autoclose').datepicker({
      autoclose: true,
      todayHighlight: true
    });

    $('#date-range').datepicker({
      toggleActive: true,
      format: "yyyy-mm-dd"
    });



  });
</script>

</body>
</html>
