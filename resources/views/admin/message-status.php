<?php 
//ini_set('max_execution_time', 600); //600 seconds = 10 minutes
set_time_limit(0);
?>
<!-- Custom CSS -->
<link href="assets/css/bootstrap-datepicker.min.css" rel="stylesheet">
  <!-- Page Content -->
  <div id="page-wrapper">
    <div class="container-fluid">
      <div class="row bg-title">
        <div class="col-lg-12">
          <h4 class="page-title">Check Message Status</h4>
          <ol class="breadcrumb">
            <li><a href="./">Dashboard</a></li>
            <li class="active">Check Message Status</li>
          </ol>
        </div>
        <!-- /.col-lg-12 -->
      </div>
      <!-- row -->
      <div class="row">
        <div class="col-sm-12">
          <div class="white-box">
            <div class="row">
              <div class="col-md-12">
                <?php echo form_open('Message-Status', 'class="form-horizontal"'); ?>
                  <div class="form-group">
                    <label for="client_id" class="col-sm-3 control-label">Client ID: <span class="text-danger">*</span></label>
                    <div class="col-sm-9">
                      <input type="text" class="form-control" name="client_id" maxlength="8000" id="client_id" value="<?php echo set_value('client_id'); ?>" placeholder="" required="required">
                    </div>
                  </div>
                  <div class="form-group">
                    <label for="date" class="col-sm-3 control-label">Date: <span class="text-danger">*</span></label>
                    <div class="col-sm-9">
                      <div class="input-group date">
                        <input type="text" class="form-control" data-date-format="yyyy-mm-dd" name="date" maxlength="10" id="date" value="<?php echo set_value('date'); ?>" placeholder="" required="required">
                        <div class="input-group-addon">
                          <span class="glyphicon glyphicon-th"></span>
                        </div>
                      </div><!--
                      <input type="date" class="form-control" name="date" maxlength="20" id="date" value="<?php echo set_value('date'); ?>" placeholder="" required="required">-->
                    </div>
                  </div>
                  <div class="form-group">
                    <label for="msisdn" class="col-sm-3 control-label">MSISDN: <span class="text-danger">*</span></label>
                    <div class="col-sm-9">
                      <textarea rows="5" type="text" class="form-control" name="msisdn" maxlength="2000" id="msisdn" required="required"><?php echo set_value('msisdn'); ?></textarea>
                    </div>
                  </div><!--
                  <div class="form-group">
                    <label for="msisdn" class="col-sm-3 control-label">MSISDN: <span class="text-danger">*</span></label>
                    <div class="col-sm-9">
                      <input type="text" class="form-control" name="msisdn" maxlength="2000" id="msisdn" value="<?php echo set_value('msisdn'); ?>" placeholder="" required="required">
                    </div>
                  </div>-->
                  <div class="form-group m-b-0">
                    <div class="col-sm-offset-3 col-sm-9">
                      <button type="submit" class="btn btn-info waves-effect waves-light">Search</button>
                    </div>
                  </div>
                </form>
              </div>
            </div>
          </div>
        </div>
        
      </div>
      <!-- row -->
      <div class="row">
        <div class="col-sm-12">
          <div class="white-box">
            <h3>Search Result</h3>
            <!--<p class="text-muted m-b-20">Lorem<code>ipsum.table-bordered</code>.</p>-->
            <div class="table-responsive">
              <table class="table table-bordered">
                <thead>
                  <tr>
                    <th>#</th>
                    <th>MSISDN</th>
                    <th>DateTime</th>
                    <th>Sender ID</th>
                    <th>Status</th>
                    <th>Status Description</th>
                  </tr>
                </thead>
                <tbody>
                <?php if(empty($rows)): ?>
                  <tr>
                    <td colspan="6" align="center">No data returned.</td>
                  </tr>
                <?php else: ?>
                <?php $sn = 1; foreach ($rows as $row): ?>
                  <tr>
                    <td><?php echo $sn++; ?></td>
                    <td><?php echo $row['msisdn']; ?></td>
                    <td><?php echo date('M d, Y h:i A', strtotime($row['datetime'])); ?></td>
                    <td><?php echo $row['sender_id']; ?></td>
                    <td><?php echo $row['status']; ?></td>
                    <td><?php echo $row['status_description']; ?></td>
                  </tr>
                <?php endforeach; ?>
                <?php endif; ?>
                </tbody>
              </table>
            </div>
          </div>
        </div>
      </div>
    </div>
    <!-- /.container-fluid -->
  </div>
  <!-- /#page-wrapper -->

<?php $this->load->view($this->config->item('template_dir_admin') . 'footer', array('error', $error, 'error_code', $error_code)); echo "\n";  // load footer view ?>
<script src="assets/js/jasny-bootstrap.js"></script>
<script src="assets/js/bootstrap-datepicker.min.js"></script>
<script type="text/javascript">
  $('#date').datepicker({
    //format: "yyyy-mm-dd"
    format: "yyyy-mm-dd",
    startDate: "1900-01-01",
    endDate: "2116-01-01",
    todayBtn: "linked",
    autoclose: true,
    todayHighlight: true
  });
</script>

</body>
</html>
