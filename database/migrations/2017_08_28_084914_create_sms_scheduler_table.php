<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSmsSchedulerTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('sms_scheduler', function (Blueprint $table) {
            $table->increments('id');
            $table->text('member_ids_phones')->nullable();
            $table->string('sender_id', 50)->nullable();
            $table->string('message', 1000)->nullable();
            $table->boolean('is_sent')->default(0);
            $table->integer('created_by');
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('sms_scheduler');
    }
}
