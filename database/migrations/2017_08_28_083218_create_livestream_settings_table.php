<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateLivestreamSettingsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('livestream_settings', function (Blueprint $table) {
            $table->increments('id');
            $table->string('event_title', 255);
            $table->string('embed_id', 20)->nullable();
            $table->string('accountId', 20)->nullable();
            $table->string('eventId', 20)->nullable();
            $table->string('width', 5)->nullable();
            $table->string('height', 5)->nullable();
            $table->integer('event_month')->nullable();
            $table->integer('event_year')->nullable();
            $table->boolean('homepage')->default(0);
            $table->integer('created_by');
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('livestream_settings');
    }
}
