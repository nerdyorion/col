<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateGalleryTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('gallery', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('category_id')->unsigned()->index();
            $table->foreign('category_id')->references('id')->on('gallery_categories');
            $table->integer('photo_album_id')->unsigned()->index()->nullable();
            $table->foreign('photo_album_id')->references('id')->on('photo_album');
            $table->string('name', 255);
            $table->string('soundcloud_id', 255)->nullable();
            $table->string('youtube_id', 255)->nullable();
            $table->string('image_url', 2000)->nullable();
            $table->string('image_url_thumb', 2006)->nullable();
            $table->text('carousel_urls')->nullable();
            $table->integer('created_by');
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('gallery');
    }
}
