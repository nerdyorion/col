<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSettingsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('settings', function (Blueprint $table) {
            $table->increments('id');
            $table->string('birthday_message', 1000)->nullable();
            $table->time('birthday_message_send_time')->nullable();
            $table->float('sms_price_per_unit');
            $table->string('sms_vendor_name', 255);
            $table->string('sms_vendor_desc', 255);
            $table->string('sms_vendor_email', 255);
            $table->string('sms_vendor_pay_button_text', 255);
            $table->string('sms_vendor_pay_redirect_url', 2000);
            $table->string('sms_vendor_logo_url', 2000);
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('settings');
    }
}
