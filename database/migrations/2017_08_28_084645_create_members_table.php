<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMembersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('members', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('member_category_id')->unsigned()->index();
            $table->foreign('member_category_id')->references('id')->on('member_categories');
            $table->string('full_name', 255);
            $table->string('email', 255)->unique();
            $table->string('phone1', 50)->nullable();
            $table->string('phone2', 50)->nullable();
            $table->date('dob')->nullable();
            $table->string('address', 8000)->nullable();
            $table->integer('created_by');
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('members');
    }
}
