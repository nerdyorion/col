<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDonationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('donations', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('donation_category_id')->unsigned()->index();
            $table->foreign('donation_category_id')->references('id')->on('donations');
            $table->string('full_name', 255);
            $table->string('phone', 255)->nullable();
            $table->string('email', 255)->nullable();
            $table->float('amount')->nullable();
            $table->string('recurrence', 255)->nullable();
            $table->string('comment', 8000)->nullable();
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('donations');
    }
}
