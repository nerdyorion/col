<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDepartmentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('departments', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name', 255);
            $table->string('description', 8000)->nullable();
            $table->integer('created_by');
            $table->timestamps();
            $table->softDeletes();
        });


        Schema::create('member_department', function (Blueprint $table) {
            $table->integer('member_id');
            $table->integer('department_id');
            $table->primary(['member_id', 'department_id']);
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('departments');
        Schema::dropIfExists('member_department');
    }
}
