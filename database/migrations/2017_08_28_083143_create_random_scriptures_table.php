<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRandomScripturesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('random_scriptures', function (Blueprint $table) {
            $table->increments('id');
            $table->string('text', 8000);
            $table->string('bible_book', 255)->nullable();
            $table->integer('bible_chapter')->nullable();
            $table->integer('bible_verse')->nullable();
            $table->string('tags', 2000)->nullable();
            $table->integer('created_by');
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('random_scriptures');
    }
}
