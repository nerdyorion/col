<?php

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;

class DonationCategoriesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
		DB::table('donation_categories')->insert([
			'name' => 'Donation',
			'description' => 'Donation',
			'can_be_deleted' => 0,
			'created_by' => 1,
			'created_at' => date('Y-m-d H:i:s'),
			'updated_at' => date('Y-m-d H:i:s'),
		]);

		DB::table('donation_categories')->insert([
			'name' => 'Tithe',
			'description' => 'Tithe',
			'can_be_deleted' => 0,
			'created_by' => 1,
			'created_at' => date('Y-m-d H:i:s'),
			'updated_at' => date('Y-m-d H:i:s'),
		]);

		DB::table('donation_categories')->insert([
			'name' => 'Offering',
			'description' => 'Offering',
			'can_be_deleted' => 0,
			'created_by' => 1,
			'created_at' => date('Y-m-d H:i:s'),
			'updated_at' => date('Y-m-d H:i:s'),
		]);

		DB::table('donation_categories')->insert([
			'name' => 'First Fruit',
			'description' => 'First Fruit',
			'can_be_deleted' => 0,
			'created_by' => 1,
			'created_at' => date('Y-m-d H:i:s'),
			'updated_at' => date('Y-m-d H:i:s'),
		]);
    }
}
