<?php

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // $users = factory(App\User::class, 3)->make();
		
		// DB::table('users')->insert([
		// 	'name' => str_random(10),
		// 	'email' => str_random(10).'@gmail.com',
		// 	'password' => bcrypt('secret'),
		// ]);

		// factory(App\\Models\User::class, 5)->create()->each(function ($u) {
		// 	$u->posts()->save(factory(App\Post::class)->make());
		// });

    	// LIVE
		// factory(App\Models\User::class, 5)->create();

		DB::table('users')->insert([
			'name' => 'John Doe',
			'email' => 'johndoe@yahoo.com',
			'gender' => 'Male',
			'is_admin' => 1,
			'password' => Hash::make('johndoe'),
			'created_at' => date('Y-m-d H:i:s'),
			'updated_at' => date('Y-m-d H:i:s'),
		]);

    }
}
