<?php

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| Here you may define all of your model factories. Model factories give
| you a convenient way to create models for testing and seeding your
| database. Just tell the factory how a default model should look.
|
*/

/** @var \Illuminate\Database\Eloquent\Factory $factory */
$factory->define(App\Models\User::class, function (Faker\Generator $faker) {
    static $password;
    $gender = $faker->randomElements(['Male', 'Female']);

    return [
        'name' => $faker->name,
        'email' => $faker->unique()->safeEmail,
        'phone' => $faker->phoneNumber,
        'gender' => $gender[0],
        'address' => $faker->address,
        'password' => $password ?: $password = Hash::make('secret'),
        // 'password' => $password ?: $password = bcrypt('secret'),
        'remember_token' => str_random(10),
    ];
});
