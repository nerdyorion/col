<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Contracts\Auth\Factory as Auth;

class MyMiddleware
{
	/**
	* Authentication factory instance.
	*
	* @var \Illuminate\Contracts\Auth\Factory
	*/

	protected $auth;

	/**
	* Inject our middleware dependencies.
	*
	* @param \Illuminate\Contracts\Auth\Factory $auth
	*/

	public function __construct(Auth $auth)
	{
		$this->auth = $auth;
	}

	/**
	* Handle an incoming request.
	*
	* @param \Illuminate\Http\Request $request
	* @param \Closure	$next
	* @return mixed
	*/

	public function handle($request, Closure $next)
	{
		// Code to be executed *before* the response
		// has been rendered, should go here.

		if ($auth->guest()) {
			return redirect()->route('login');
		}

		$response = $next($request);

		// Code to be executed *after* the response
		// has been rendered, should go here.

		$response->header('My-Header', 'present');

		return $response;
	}

}


?>