<?php

namespace App\Http\Controllers\Admin;

// use App\Panda;
use Illuminate\Contracts\View\Factory as View;
use App\Http\Controllers\Controller;

/**
 * Class HomeController.
 */
class HomeController extends Controller
{
    /**
    * Our view factory instance.
    *
    * @var \Illuminate\Contracts\View\Factory
    */

    protected $view;
    
    /**
    * Inject our controller dependencies.
    *
    * @param \Illuminate\Contracts\View\Factory $view
    */

    public function __construct(View $view)
    {
        // $this->middleware('auth');
        $this->view = $view;
    }

    public function index()
    {
        $data['page_title'] = "Dashboard";
        return $this->view->make('admin.index', compact('data', $data));
    }

}
