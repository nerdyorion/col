<?php

namespace App\Http\Controllers\Admin;

use Validator;
use Illuminate\Contracts\View\Factory as View;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Hash;
use Illuminate\Http\Request;
use App\Models\User;

/**
 * Class UsersController.
 */
class UsersController extends Controller
{
    /**
    * Our view factory instance.
    *
    * @var \Illuminate\Contracts\View\Factory
    */
    protected $view;
    
    /**
    * Inject our controller dependencies.
    *
    * @param \Illuminate\Contracts\View\Factory $view
    */
    public function __construct(View $view)
    {
        $this->view = $view;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data['page_title'] = "Users";
        $data['rows'] = User::all();
        return $this->view->make('admin.users.index', compact('data', $data));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    // public function create()
    // {
    //     $data['page_title'] = "Users";
    //     $data['rows'] = User::all();
    //     return $this->view->make('admin.users.index', compact('data', $data));
    // }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $rules = [
            // 'role_id' => 'required',
            // 'full_name' => 'required|max:255|regex:[A-Za-z1-9 ]',
            'full_name' => 'required|max:255',
            'email' => 'required|email|unique:users|max:255',
            'phone' => 'max:255|nullable',
            'gender' => 'required|max:10',
            'address' => 'max:2000|nullable',
            'password' => 'min:5|confirmed'
            ];

        // $this->validate($request, $rules);

        $validator = Validator::make($request->all(), $rules);

        if ($validator->fails()) {
            return redirect('manage/users')
                ->withErrors($validator)
                ->withInput($request->except('password'));
        }

        $user = new User;
        // $user->role_id => $request->input('role_id'),
        $user->name = ucwords($request->input('full_name'));
        $user->email = strtolower($request->input('email'));
        $user->phone = $request->phone;
        $user->gender = $request->gender;
        $user->address = $request->address;
        $user->password = Hash::make($request->password);
        $user->is_admin = 1;

        // dd($user);

        $user->save();

        // return redirect()->route('admin.users.index')->with('success', "The user <strong>$user->first_name</strong> has successfully been created.");

        return redirect('manage/users')->with('success', "Record added successfully.");
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $id = (int) $id;
        $data['page_title'] = "User Details";
        $data['id'] = $id;
        $data['row'] = User::find($id); // dd($id);

        if (is_null($data['row'])) {
            // no record exists with requested id
            return redirect('manage/users');
        }

        return $this->view->make('admin.users.details', compact('data', $data));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $id = (int) $id;
        $data['page_title'] = "Update User";
        $data['id'] = $id;
        $data['row'] = User::find($id); // dd($id);

        if (is_null($data['row'])) {
            // no record exists with requested id
            return redirect('manage/users');
        }

        return $this->view->make('admin.users.edit', compact('data', $data));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $rules = [
            // 'role_id' => 'required',
            // 'full_name' => 'required|max:255|regex:[A-Za-z1-9 ]',
            'full_name' => 'required|max:255',
            'email' => 'required|email|unique:users|max:255',
            'phone' => 'max:255|nullable',
            'gender' => 'required|max:10',
            'address' => 'max:2000|nullable'
            ];

        $validator = Validator::make($request->all(), $rules);

        if ($validator->fails()) {
            // return redirect("manage/users/$id/edit")
            return redirect()->route('users.edit', ['id' => $id])
                ->withErrors($validator)
                ->withInput();
        }

        $user = User::find($id);

        if (is_null($user)) {
            // no record exists with requested id
            return redirect('manage/users');
        }
        
        // $user->role_id => $request->input('role_id'),
        $user->name = ucwords($request->input('full_name'));
        $user->email = strtolower($request->input('email'));
        $user->phone = $request->phone;
        $user->gender = $request->gender;
        $user->address = $request->address;

        $user->save();

        return redirect('manage/users')->with('success', "Record updated successfully.");
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        return redirect('manage/users')->with('success', "Record deleted successfully.");
    }

}
