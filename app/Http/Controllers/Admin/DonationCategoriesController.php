<?php

namespace App\Http\Controllers\Admin;

use Validator;
use Illuminate\Contracts\View\Factory as View;
use App\Http\Controllers\Controller;
use App\Models\DonationCategory;
use Illuminate\Http\Request;

/**
 * Class DonationCategories.
 */
class DonationCategoriesController extends Controller
{
    /**
    * Our view factory instance.
    *
    * @var \Illuminate\Contracts\View\Factory
    */
    protected $view;
    
    /**
    * Inject our controller dependencies.
    *
    * @param \Illuminate\Contracts\View\Factory $view
    */
    public function __construct(View $view)
    {
        $this->view = $view;
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data['page_title'] = "Donation Categories";
        $data['rows'] = DonationCategory::all(); //dd($data['rows']);
        return $this->view->make('admin.donation-categories.index', compact('data', $data));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $rules = [
            'name' => 'required|max:255',
            'description' => 'max:8000|nullable'
            ];

        $validator = Validator::make($request->all(), $rules);

        if ($validator->fails()) {
            return redirect('manage/donation-categories')
                ->withErrors($validator)
                ->withInput();
        }

        $donation_category = new DonationCategory;
        $donation_category->name = $request->input('name');
        $donation_category->description = $request->description;
        $donation_category->can_be_deleted = 1;
        $donation_category->created_by = 1;

        $donation_category->save();

        return redirect('manage/donation-categories')->with('success', "Record added successfully.");
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $id = (int) $id;
        $data['page_title'] = "Update Donation Category";
        $data['id'] = $id;
        $data['row'] = DonationCategory::find($id);

        if (is_null($data['row'])) {
            // no record exists with requested id
            return redirect('manage/donation-categories');
        }

        return $this->view->make('admin.donation-categories.edit', compact('data', $data));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $rules = [
            'name' => 'required|max:255',
            'description' => 'max:8000|nullable'
            ];

        $validator = Validator::make($request->all(), $rules);

        if ($validator->fails()) {
            return redirect()->route('donation-categories.edit', ['id' => $id])
                ->withErrors($validator)
                ->withInput();
        }

        $donation_category = DonationCategory::find($id);

        if (is_null($donation_category)) {
            // no record exists with requested id
            return redirect('manage/donation-categories');
        }
        
        $donation_category->name = $request->name;
        $donation_category->description = $request->description;

        $donation_category->save();

        return redirect('manage/donation-categories')->with('success', "Record updated successfully.");
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        DonationCategory::where('id', '=', (int) $id)
        ->where('can_be_deleted', '=', 1)
        ->delete();

        return redirect('manage/donation-categories')->with('success', "Record deleted successfully.");
    }
}
