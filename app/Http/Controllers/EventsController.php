<?php

namespace App\Http\Controllers;

// use App\Panda;
use Illuminate\Contracts\View\Factory as View;

/**
 * Class EventsController.
 */
class EventsController extends Controller
{
    /**
    * Our view factory instance.
    *
    * @var \Illuminate\Contracts\View\Factory
    */

    protected $view;
    
    /**
    * Inject our controller dependencies.
    *
    * @param \Illuminate\Contracts\View\Factory $view
    */

    public function __construct(View $view)
    {
        $this->view = $view;
    }

    public function index()
    {
        // $pandas = Panda::all();
        // return $this->view->make('index', compact('pandas'));
        return $this->view->make('events', compact('pandas'));
    }

    public function view()
    {
        // $pandas = Panda::all();
        // return $this->view->make('index', compact('pandas'));
        return $this->view->make('event-details', compact('pandas'));
    }

}
