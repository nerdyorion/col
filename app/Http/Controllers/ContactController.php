<?php

namespace App\Http\Controllers;

use Validator;
use App\Models\Feedback;
use Illuminate\Contracts\View\Factory as View;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

/**
 * Class ContactController.
 */
class ContactController extends Controller
{
    /**
    * Our view factory instance.
    *
    * @var \Illuminate\Contracts\View\Factory
    */

    protected $view;
    
    /**
    * Inject our controller dependencies.
    *
    * @param \Illuminate\Contracts\View\Factory $view
    */

    public function __construct(View $view)
    {
        $this->view = $view;
    }

    public function index()
    {
        // $pandas = Panda::all();
        // return $this->view->make('index', compact('pandas'));
        return $this->view->make('contact', compact('pandas'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        Validator::extend('captcha', function ($field, $value, $params) {
            return $value == '27';
        });

        $rules = [
            'full_name' => 'required|max:255',
            'email' => 'required|max:255',
            'phone' => 'max:255|nullable',
            'subject' => 'required|max:2000',
            'message' => 'required',
            'verify' => 'required|captcha'
            ];

        $validator = Validator::make($request->all(), $rules);

        if ($validator->fails()) {
            // return redirect('contact')
            //     ->withErrors($validator)
            //     ->withInput();
            $errors = $validator->messages();
            // return $errors;
            echo '<div id="error-msg" style="display:block;" class="myadmin-alert myadmin-alert-icon myadmin-alert-click alert6 myadmin-alert-top">
                <ul style="list-style-type: none;">
                <li>' . $errors . '</li>
                </ul>
                <div class="pull-right" style="position: absolute; top:15px; display: inline;"><i class="ti-close"></i></div>
            </div>';
            exit;
        }

        $feedback = new Feedback;
        $feedback->type = 'contact-us';
        $feedback->full_name = $request->input('full_name');
        $feedback->email = $request->input('email');
        $feedback->phone = $request->input('phone');
        $feedback->subject = $request->input('subject');
        $feedback->message = $request->input('message');

        $feedback->save();

        // return redirect('contact')->with('success', "Thanks for your message. We will get back to you shortly.");
        echo '<div id="success-msg" style="display:block;" class="myadmin-alert myadmin-alert-icon myadmin-alert-click alert3 myadmin-alert-top">
        <ul style="list-style-type: none;">
        <li>Thanks for your message. We will get back to you shortly.</li>
        </ul>
        <div class="pull-right" style="position: absolute; top:15px; display: inline;"><i class="ti-close"></i></div>
        </div>';
    }

}
