<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Setting extends Model
{
    use SoftDeletes;
    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
    */
    protected $dates = ['deleted_at'];

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'birthday_message',
        'birthday_message_send_time',
        'sms_price_per_unit',
        'sms_vendor_name',
        'sms_vendor_desc',
        'sms_vendor_email',
        'sms_vendor_pay_button_text',
        'sms_vendor_pay_redirect_url',
        'sms_vendor_logo_url'
    ];
    // (Add default settings so it's not empty)
    // (Load all settings in session)

}
