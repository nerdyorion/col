<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Gallery extends Model
{
    use SoftDeletes;
    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
    */
    protected $dates = ['deleted_at'];

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'category_id',
        'photo_album_id',
        'name',
        'soundcloud_id',
        'youtube_id',
        'image_url',
        'image_url_thumb',
        'carousel_urls',
        'created_by'
    ];

	// NB:: If category_id sermon has youtube_url, then it's a video sermon, use youtube embed
	// for music, video, photo gallery :: music--soundcloud_id, video--youtube_id, photo gallery: photo_album_id
	// Max 30 carousel_urls

    /**
     * Get the category that owns the gallery.
     */
    public function category()
    {
        return $this->belongsTo(GalleryCategory::class);
    }

    /**
     * Get the photo_album that owns the gallery.
     */
    public function photo_album()
    {
        return $this->belongsTo(PhotoAlbum::class);
    }
}
