<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class DonationCategory extends Model
{
    use SoftDeletes;
    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
    */
    protected $dates = ['deleted_at'];

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name',
        'description',
        'created_by'
    ];

    // (Donation, Tithe, First-Fruit, Offering, Teens Church Building, Church Planting, e.t.c) donation,tithe,offering,f-f -> undeletable


    /**
     * Get the donations for the donation category.
     */
    public function donations()
    {
        return $this->hasMany(Donation::class);
    }

    /**
     * Get the transactions for the donation category.
     */
    public function transactions()
    {
        return $this->hasManyThrough(Transaction::class, Donation::class);
    }
}
