<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class SentSms extends Model
{
    use SoftDeletes;
    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
    */
    protected $dates = ['deleted_at'];

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'member_id',
        'sender_id',
        'phone',
        'message',
        'no_of_units',
        'error_reason',
        'successful',
        'is_birthday',
        'created_by'
    ];

    /**
     * Get the member that owns the sms.
     */
    public function member()
    {
        return $this->belongsTo(Member::class);
    }

}
