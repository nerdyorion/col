<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class LivestreamSetting extends Model
{
	use SoftDeletes;
	
    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
    */
    protected $dates = ['deleted_at'];

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'event_title',
        'embed_id',
        'accountId',
        'eventId',
        'width',
        'height',
        'event_month',
        'event_year',
        'homepage',
        'created_by'
    ];

    // ONLY ONE ROW where homepage = 1 -- NOT DELETABLE
}
