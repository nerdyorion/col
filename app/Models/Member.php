<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Member extends Model
{
    use SoftDeletes;
    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
    */
    protected $dates = ['deleted_at'];

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'member_category_id',
        'full_name',
        'email',
        'phone1',
        'phone2',
        'dob',
        'address',
        'created_by'
    ];

    /**
     * Get the category that owns the member.
     */
    public function category()
    {
        return $this->belongsTo(MemberCategory::class);
    }

    /**
     * Get the departments the member belongs to.
     */
    public function departments()
    {
    	return $this->belongsToMany(Department::class);
    }

    /**
     * Get the sent sms the member has.
     */
    public function sent_sms()
    {
    	return $this->hasMany(SentSms::class);
    }


}
