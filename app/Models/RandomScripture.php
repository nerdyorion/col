<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class RandomScripture extends Model
{
	use SoftDeletes;
	
    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
    */
    protected $dates = ['deleted_at'];

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'text',
        'bible_book',
        'bible_chapter',
        'bible_verse',
        'tags',
        'created_by'
    ];

    // display random_scriptures in "quote" section of event-details
}
