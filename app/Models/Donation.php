<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Donation extends Model
{
    use SoftDeletes;
    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
    */
    protected $dates = ['deleted_at'];

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'donation_category_id',
        'full_name',
        'phone',
        'email',
        'amount',
        'recurrence',
        'comment'
    ];

    /**
     * Get the category that owns the donation.
     */
    public function category()
    {
        return $this->belongsTo(DonationCategory::class);
    }

    /**
     * Get the transaction that owns the donation.
     */
    public function transaction()
    {
        return $this->belongsTo(Transaction::class);
    }
    
}
