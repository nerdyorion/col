<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
| WE MUST USE RCCGCHAPELOFLIFE.ORG
*/


Route::get('/', 'HomeController@index')->name('index');

Route::get('/announcements', 'AnnouncementsController@index')->name('announcements');

Route::get('/events', 'EventsController@index')->name('events');

Route::get('/event-details', 'EventsController@view')->name('event-details');

Route::get('/church-projects', 'ChurchProjectsController@index')->name('church-projects'); // teens church | church planting

Route::get('/service-times', 'ServiceTimesController@index')->name('service-times');

Route::get('/contact', 'ContactController@index')->name('contact');
Route::post('/contact', 'ContactController@store');

Route::get('/blog', 'BlogController@index')->name('blog');

Route::get('/blog-details', 'BlogController@view')->name('blog-details');

Route::get('/about', 'AboutController@index')->name('about');

Route::get('/watch-holy-ghost-service', 'HolyGhostServiceController@index')->name('watch-holy-ghost-service');

Route::get('/watch-holy-ghost-service-details', 'HolyGhostServiceController@view')->name('watch-holy-ghost-service-details');



// ---------  Start Ministries

Route::get('/sanitation', 'SanitationController@index')->name('sanitation');

Route::get('/choir', 'ChoirController@index')->name('choir');

Route::get('/good-women', 'GoodWomenController@index')->name('good-women');

Route::get('/excellent-men', 'ExcellentMenController@index')->name('excellent-men');

Route::get('/children', 'ChildrenController@index')->name('children');

Route::get('/prayer', 'PrayerController@index')->name('prayer');

Route::get('/house-fellowship', 'HouseFellowshipController@index')->name('house-fellowship');

// ---------  End Ministries

Route::get('/gallery', 'GalleryController@index')->name('gallery');

Route::get('/music', 'MusicController@index')->name('music');

Route::get('/videos', 'VideosController@index')->name('videos');

Route::get('/sermons', 'SermonsController@index')->name('sermons');

Route::get('/testimonies', 'TestimoniesController@index')->name('testimonies');

Route::get('/tithes-offering-first-fruit', 'TithesController@index')->name('tithes-offering-first-fruit');

Route::get('/pastor-in-charge-of-province-32', 'PICPController@index')->name('pastor-in-charge-of-province-32');

Route::get('/the-pastor', 'ThePastorController@index')->name('the-pastor');

Route::get('/deacons-and-deaconesses', 'DeaconsAndDeaconessesController@index')->name('deacons-and-deaconesses');

Route::get('/heads-of-departments', 'HODsController@index')->name('heads-of-departments');

Route::get('/ask-the-pastor', 'AskThePastorController@index')->name('ask-the-pastor');

Route::get('/join-the-workforce', 'JoinTheWorkforceController@index')->name('join-the-workforce');

Route::get('/prayer-request', 'PrayerRequestController@index')->name('prayer-request');


// Route::get('/manage/donation-categories', 'Admin\DonationCategoriesController@index')->name('donation-categories');


// Route::group(['prefix' => 'manage', 'namespace' => 'Admin'], function () {
Route::group(['prefix' => 'manage', 'namespace' => 'Admin', 'middleware' => ['auth', 'admin']], function () {
    Route::resource('/', 'HomeController', ['only' => [
    	'index'
	]]);
    Route::resource('donation-categories', 'DonationCategoriesController', ['except' => [
        'create'
    ]]);
    Route::resource('users', 'UsersController', ['except' => [
    	'create'
	]]);

    Route::get('orders', [
        'uses' => 'OrdersController@index',
        'as' => 'orders.index',
    ]);
});

// Auth::routes();

// Route::group(['prefix' => 'manage', 'middleware' => ['auth', 'admin']],function(){
Route::group(['prefix' => 'manage','namespace' => 'Auth'],function(){
    // Authentication Routes...
    Route::get('login', 'LoginController@showLoginForm')->name('login');
    Route::post('login', 'LoginController@login');
    Route::get('logout', 'LoginController@logout')->name('logout');

    // Password Reset Routes...
    Route::get('password/reset', 'ForgotPasswordController@showLinkRequestForm')->name('password.reset');
    Route::post('password/email', 'ForgotPasswordController@sendResetLinkEmail')->name('password.email');
    Route::get('password/reset/{token}', 'ResetPasswordController@showResetForm')->name('password.reset.token');
    Route::post('password/reset', 'ResetPasswordController@reset');

});