function SetCheckAll(inputName)
  {

    var elementInput = document.getElementsByTagName('input')
    var unchecked = 0
		
    for (var h = 0; h < elementInput.length; h++)
    {

      if (elementInput[h].getAttribute('type') == 'checkbox' && elementInput[h].getAttribute('name') == inputName)
      {

	  if (elementInput[h].checked == false)
	  {

	    unchecked = 1
	  }
      }
    }
    for (var i = 0; i < elementInput.length; i++)
    {

      if (elementInput[i].getAttribute('type') == 'checkbox' && elementInput[i].getAttribute('name') == inputName)
      {

        if (unchecked == 1)
        {

          elementInput[i].checked = true
        }
        else
        {

          elementInput[i].checked = false
        }
      }
    }
  }